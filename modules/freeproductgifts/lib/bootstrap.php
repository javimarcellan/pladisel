<?php
/**
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * You must not modify, adapt or create derivative works of this source code
 *
 * @author    Musaffar Patel
 * @copyright 2016-2017 Musaffar Patel
 * @license   LICENSE.txt
 */

$module_dir = 'freeproductgifts';
/* types */
include_once(_PS_MODULE_DIR_."/$module_dir/lib/types/module.php");

/* lib */
include_once(_PS_MODULE_DIR_."/$module_dir/lib/classes/FPGControllerCore.php");
include_once(_PS_MODULE_DIR_."/$module_dir/lib/classes/FPGInstallCore.php");
include_once(_PS_MODULE_DIR_."/$module_dir/lib/widgets/MPProductSearchWidgetController.php");

/* models */
include_once(_PS_MODULE_DIR_."/$module_dir/models/FPGInstall.php");
include_once(_PS_MODULE_DIR_."/$module_dir/models/FPGModel.php");
include_once(_PS_MODULE_DIR_."/$module_dir/models/FPGProductOptionModel.php");
include_once(_PS_MODULE_DIR_."/$module_dir/models/FPGCartProductModel.php");
include_once(_PS_MODULE_DIR_."/$module_dir/models/FPGCartProductExtraModel.php");

/* controllers */
include_once(_PS_MODULE_DIR_."/$module_dir/controllers/admin/producttab/FPGAdminProductTabController.php");
include_once(_PS_MODULE_DIR_."/$module_dir/controllers/admin/producttab/FPGAdminProductTabEditGiftController.php");

include_once(_PS_MODULE_DIR_."/$module_dir/controllers/admin/FPGAdminConfigGeneralController.php");
include_once(_PS_MODULE_DIR_."/$module_dir/controllers/front/FPGFrontController.php");

/* helpers */
include_once(_PS_MODULE_DIR_."/$module_dir/helpers/productsearchwidget.php");
include_once(_PS_MODULE_DIR_."/$module_dir/helpers/FPGCartHelper.php");
