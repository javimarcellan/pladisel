<?php
/**
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * You must not modify, adapt or create derivative works of this source code
 *
 * @author    Musaffar Patel
 * @copyright 2016-2017 Musaffar Patel
 * @license   LICENSE.txt
 */

class MPProductSearchWidgetController
{

	public function getSearchResults($product_name, $id_lang)
	{
		$sql = new DbQuery();
		$sql->select('DISTINCT(id_product), name');
		$sql->from('product_lang');
		$sql->where('name LIKE "%'.pSQL($product_name).'%"');
		$sql->where('id_lang = '.(int)$id_lang);
		$results = Db::getInstance()->executeS($sql);
		return $results;
	}

	public function processSearch()
	{
		return $this->getSearchResults(Tools::getValue('search_string'), Context::getContext()->language->id);
	}

	public function route()
	{
		switch (Tools::getValue('action'))
		{
			case 'processsearch' :
				return $this->processSearch();
		}
	}

}