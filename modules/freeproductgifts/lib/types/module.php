<?php
/**
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * You must not modify, adapt or create derivative works of this source code
 *
 * @author    Musaffar Patel
 * @copyright 2016-2017 Musaffar Patel
 * @license   LICENSE.txt
 */

class TFPGProductGift
{
	public
		$id_product = -1,
		$id_shop = -1,
		$gift_ids = array();
}

class TFPGProductGiftEntry
{
	public
		$id_product = -1,
		$id_shop = -1,
		$id_product_attribute = -1,
		$id_gift = -1,
		$qty = -1,
		$min_qty = -1,
		$position = -1,
		$enabled = -1;
}