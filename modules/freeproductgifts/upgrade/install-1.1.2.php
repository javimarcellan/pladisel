<?php
/**
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * You must not modify, adapt or create derivative works of this source code
 *
 * @author    Musaffar Patel
 * @copyright 2016-2017 Musaffar Patel
 * @license   LICENSE.txt
 */

if (!defined('_PS_VERSION_'))
	exit;

function upgrade_module_1_1_2($object)
{
	$return = true;
	$return &= Db::getInstance()->execute('
			CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'fpg_cart_product` (
				`id_fpg_cart_product` int(10) unsigned NOT NULL AUTO_INCREMENT,
				`id_cart` int(10) unsigned NOT NULL,
				`id_product_parent` int(10) unsigned NOT NULL,
				`id_product_attribute_parent` int(10) unsigned NOT NULL,
				`id_product` int(10) unsigned NOT NULL,
				`id_product_attribute` int(10) unsigned NOT NULL,
			PRIMARY KEY (`id_fpg_cart_product`)
			) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8 ;');
	return true;
}