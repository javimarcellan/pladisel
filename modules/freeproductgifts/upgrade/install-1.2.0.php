<?php
/**
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * You must not modify, adapt or create derivative works of this source code
 *
 * @author    Musaffar Patel
 * @copyright 2016-2017 Musaffar Patel
 * @license   LICENSE.txt
 */

if (!defined('_PS_VERSION_'))
	exit;

function upgrade_module_1_2_0($object)
{
	$return = true;
	$return &= Db::getInstance()->execute('
			CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'fpg_product_option` (
				`id_product_option` int(10) unsigned NOT NULL AUTO_INCREMENT,
				`id_product` int(10) unsigned NOT NULL,
				`total_qty_per_product` mediumint(8) unsigned NOT NULL DEFAULT \'0\',			
				PRIMARY KEY (`id_product_option`)	
			) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8 ;');
	return true;
}