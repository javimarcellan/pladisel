<?php
/**
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * You must not modify, adapt or create derivative works of this source code
 *
 * @author    Musaffar Patel
 * @copyright 2016-2017 Musaffar Patel
 * @license   LICENSE.txt
 */

if (!defined('_PS_VERSION_'))
	exit;

class FPGCartProductExtraModel extends ObjectModel
{

	/** @var integer Unique ID */
	public $id_fpg_cart_product_extra;

	/** @var integer Cart ID */
	public $id_cart;

	/** @var integer product ID */
	public $id_product;

	/** @var integer product Attribute ID */
	public $id_product_attribute;

	/** @var boolean Is Free Gift */
	public $fpg_freegift;

	/** @var integer Parent product ID */
	public $fpg_id_product_parent;

	/** @var integer Parent product attribute ID */
	public $fpg_id_product_attribute_parent;


	/**
	 * @see ObjectModel::$definition
	 */

	public static $definition = array(
		'table' => 'fpg_cart_product_extra',
		'primary' => 'id_fpg_cart_product_extra',
		'multilang' => false,
		'fields' => array(
			'id_cart' => array('type' => self::TYPE_INT),
			'id_product' => array('type' => self::TYPE_INT),
			'id_product_attribute' => array('type' => self::TYPE_INT),
			'fpg_freegift' => array('type' => self::TYPE_INT),
			'fpg_id_product_parent' => array('type' => self::TYPE_INT),
			'fpg_id_product_attribute_parent' => array('type' => self::TYPE_INT)
		)
	);

	public function getByParentProduct($id_product, $ipa, $id_cart)
	{
		$sql = new DbQuery();
		$sql->select('*');
		$sql->from('fpg_cart_product_extra');
		$sql->where('fpg_id_product_parent=' . (int)$id_product);
		$sql->where('fpg_id_product_attribute_parent=' . (int)$ipa);
		$sql->where('id_cart=' . (int)$id_cart);
		$result = DB::getInstance()->executeS($sql);
		if (!empty($result)) {
			return $this->hydrateCollection('FPGCartProductExtraModel', $result);
		} else {
			return array();
		}
	}

	public static function deleteExtra($id_product, $id_product_attribute)
	{
		DB::getInstance()->delete(self::$definition['table'], 
			'id_product='.(int)$id_product.'
			AND id_product_attribute='.(int)$id_product_attribute);
	}
}