<?php
/**
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * You must not modify, adapt or create derivative works of this source code
 *
 * @author    Musaffar Patel
 * @copyright 2016-2017 Musaffar Patel
 * @license   LICENSE.txt
 */

if (!defined('_PS_VERSION_'))
	exit;

class FPGModel {

	/**
	 * @var TFPGProductGift
	 */
	public static function getProductGifts($id_product, $enabled, $id_shop, $raw = false)
	{
		$sql = new DbQuery();
		$sql->select('id_product, id_gift, qty, min_qty, position, enabled');
		$sql->from('fpg_product_gift');
		$sql->where('id_product = '.(int)$id_product);

		if (!is_null($enabled) && $enabled)
			$sql->where('enabled = 1');

		if (!is_null($enabled) && $enabled == false)
			$sql->where('enabled = 0');

		if ($id_shop > 0)
			$sql->where('id_shop = '.(int)$id_shop.' OR id_shop = 0');
		$sql->orderBy('position');
		$result = DB::getInstance()->executeS($sql);

		if ($raw)
			return $result;

		if (!empty($result) && count($result) > 0)
		{
			$fpg_product_gift = new TFPGProductGift();
			$fpg_product_gift->id_product = $id_product;
			$fpg_product_gift->id_shop = $id_shop;
			foreach ($result as $row)
				$fpg_product_gift->gift_ids[] = $row['id_gift'];
			return $fpg_product_gift;
		}
		else return false;
	}

	public static function getProductGift($id_product, $id_gift, $id_shop)
	{
		$sql = new DbQuery();
		$sql->select('id_product, id_gift, qty, min_qty, position, enabled');
		$sql->from('fpg_product_gift');
		$sql->where('id_product = '.(int)$id_product);
		$sql->where('id_gift = '.(int)$id_gift);
		if ($id_shop > 0)
			$sql->where('id_shop = '.(int)$id_shop.' OR id_shop = 0');
		$row = DB::getInstance()->getRow($sql);
		return $row;
	}

	public static function getAllFreeGiftsAvailableForCart($id_cart, $id_shop, $return_raw = false)
	{
		$gift_ids = array();
		$sql = 'SELECT
					fpg.id_product,
		            fpg.id_gift,
		            fpg.qty,
		            fpg.min_qty
		        FROM '._DB_PREFIX_.'fpg_product_gift fpg
		        INNER JOIN '._DB_PREFIX_.'cart_product cp ON fpg.id_product = cp.id_product AND (cp.id_cart = '.(int)$id_cart.' AND cp.id_shop='.(int)$id_shop.')
				AND fpg.id_gift IN (
					SELECT id_product
  					FROM '._DB_PREFIX_.'cart_product
  					WHERE cp.id_cart = '.(int)$id_cart.' AND cp.id_shop = '.(int)$id_shop.'
				)';
		$result = DB::getInstance()->executeS($sql);
		if ($return_raw) return $result;
		if ($result)
			foreach ($result as $row)
			{
				$gift_ids[$row['id_gift']]['id_product'] = $row['id_product'];
				$gift_ids[$row['id_gift']]['qty'] = $row['qty'];
				$gift_ids[$row['id_gift']]['min_qty'] = $row['min_qty'];
			}
		return $gift_ids;
	}

	public static function getAllFreeGiftsAvailableForCartSummary($id_gift, $id_cart, $id_shop)
	{
		$sql = 'SELECT
		            fpg.id_gift,
		            SUM(cp.quantity) AS product_qty,
		            fpg.qty AS fpg_qty,
		            SUM(fpg.min_qty) AS min_qty
		        FROM '._DB_PREFIX_.'fpg_product_gift fpg
		        INNER JOIN '._DB_PREFIX_.'cart_product cp ON fpg.id_product = cp.id_product AND (cp.id_cart = '.(int)$id_cart.' AND cp.id_shop='.(int)$id_shop.')
				AND fpg.id_gift IN (
					SELECT id_product
  					FROM '._DB_PREFIX_.'cart_product
  					WHERE cp.id_cart = '.(int)$id_cart.' AND cp.id_shop = '.(int)$id_shop.'
				)
				AND fpg.id_gift = '.(int)$id_gift;
		$result = DB::getInstance()->getRow($sql);
		return $result;
	}


	public static function saveFPGProductToCart($id_cart, $fpg_id_product_parent, $fpg_id_product_attribute_parent, $id_product, $id_address_delivery, $id_shop, $id_product_attribute, $quantity)
	{
		$sql = 'SELECT COUNT(id_cart) AS total_count FROM '._DB_PREFIX_.'cart_product
				WHERE id_cart = '.(int)$id_cart.'
				AND id_product = '.(int)$id_product.'
				AND id_product_attribute = '.(int)$id_product_attribute;
		$row = DB::getInstance()->getRow($sql);

		if ($row['total_count'] == 0)
		{
			DB::getInstance()->insert('cart_product', array(
				'id_cart' => (int)$id_cart,
				'id_product' => (int)$id_product,
				'id_address_delivery' => (int)$id_address_delivery,
				'id_shop' => (int)$id_shop,
				'id_product_attribute' => (int)$id_product_attribute,
				'quantity' => (int)$quantity,
				'date_add' => date('Y-m-d H:i:s')
				/*'fpg_freegift' => '1',
				'fpg_id_product_attribute_parent' => (int)$fpg_id_product_attribute_parent,
				'fpg_id_product_parent' => (int)$fpg_id_product_parent*/
			));
		}
		else {
			$sql = 'UPDATE '._DB_PREFIX_.'cart_product
			        SET quantity = quantity + '.(int)$quantity.'
			        WHERE
						id_cart = '.(int)$id_cart.'
						AND id_product = '.(int)$id_product.'
						AND id_product_attribute='.(int)$id_product_attribute;
			DB::getInstance()->execute($sql);
		}

		// Add Cart Product Extra
		$sql = 'DELETE FROM '._DB_PREFIX_.'fpg_cart_product_extra
		        WHERE
					id_cart = '.(int)$id_cart.'
					AND id_product = '.(int)$id_product.'
					AND id_product_attribute='.(int)$id_product_attribute.'
					AND fpg_id_product_parent = '.(int)$fpg_id_product_parent.'
					AND fpg_id_product_attribute_parent = '.(int)$fpg_id_product_attribute_parent;
		DB::getInstance()->execute($sql);

		DB::getInstance()->insert('fpg_cart_product_extra', array(
			'id_cart' => (int)$id_cart,
			'id_product' => (int)$id_product,
			'id_product_attribute' => (int)$id_product_attribute,
			'fpg_freegift' => '1',
			'fpg_id_product_attribute_parent' => (int)$fpg_id_product_attribute_parent,
			'fpg_id_product_parent' => (int)$fpg_id_product_parent
		));
	}

	public static function getCartProductsLight($id_cart, $raw = false)
	{
		$sql = 'SELECT id_product, quantity, id_product_attribute FROM '._DB_PREFIX_.'cart_product WHERE id_cart='.(int)$id_cart;
		$result = DB::getInstance()->executeS($sql);
		$products = array();

		if ($raw) return $result;

		if ($result)
		{
			foreach ($result as $product)
			{
				$products[$product['id_product']]['id_product'] = $product['id_product'];
				$products[$product['id_product']]['quantity'] = $product['quantity'];
			}
			return $products;
		}
		else
			return false;
	}


	/**
	 * @var TFPGProductGiftEntry
	 */
	public static function getFPGEntry($id_gift, $id_product, $id_shop, $raw = false)
	{
		$sql = 'SELECT
		 			id_product,
		 			id_gift,
		 			id_shop,
		 			qty,
		 			min_qty,
		 			id_product_attribute,
		 			`position`,
		 			enabled
				FROM '._DB_PREFIX_.'fpg_product_gift
				WHERE id_product='.(int)$id_product.'
				AND id_shop='.(int)$id_shop.'
				AND id_gift='.(int)$id_gift;
		$row = DB::getInstance()->getRow($sql);

		if (!empty($row))
		{
			$fpg_entry = new TFPGProductGiftEntry();
			$fpg_entry->enabled = $row['enabled'];
			$fpg_entry->id_gift = $row['id_gift'];
			$fpg_entry->id_product = $row['id_product'];
			$fpg_entry->id_shop = $row['id_shop'];
			$fpg_entry->min_qty = $row['min_qty'];
			$fpg_entry->position = $row['position'];
			$fpg_entry->qty = $row['qty'];
			$fpg_entry->id_product_attribute = $row['id_product_attribute'];
			return $fpg_entry;
		}
		else
			return false;
	}

	public static function saveFPGEntry(TFPGProductGiftEntry $fpg_entry)
	{
		DB::getInstance()->delete('fpg_product_gift', 'id_product='.(int)$fpg_entry->id_product.' AND id_shop='.(int)$fpg_entry->id_shop.' AND id_gift='.(int)$fpg_entry->id_gift);
		DB::getInstance()->insert('fpg_product_gift', array(
			'id_product' => (int)$fpg_entry->id_product,
			'id_gift' => (int)$fpg_entry->id_gift,
			'id_shop' => (int)$fpg_entry->id_shop,
			'qty' => (int)$fpg_entry->qty,
			'min_qty' => (int)$fpg_entry->min_qty,
			'position' => (int)$fpg_entry->position,
			'enabled' => (int)$fpg_entry->enabled
		));
	}

	public static function deleteFPGEntry($id_product, $id_gift, $id_shop)
	{
		DB::getInstance()->delete('fpg_product_gift', 'id_product='.(int)$id_product.' AND id_gift='.(int)$id_gift.' AND id_shop='.(int)$id_shop);
	}

	public static function getProductName($id_product, $id_lang, $id_shop)
	{
		$sql = 'SELECT pl.name FROM '._DB_PREFIX_.'product_lang pl
				WHERE id_product='.(int)$id_product.'
				AND id_lang='.(int)$id_lang.'
				AND id_shop='.(int)$id_shop;
		$row = DB::getInstance()->getRow($sql);
		return $row['name'];
	}

	public static function getCartProductFPGInfo($id_cart, $id_product, $id_product_attribute)
	{
		$sql = new DbQuery();
		$sql->select('fpg_freegift, fpg_id_product_parent, fpg_id_product_attribute_parent');
		$sql->from('fpg_cart_product_extra', 'c');
		$sql->where('c.id_cart = '.(int)$id_cart);
		$sql->where('c.id_product = '.(int)$id_product);
		$sql->where('c.id_product_attribute = '.(int)$id_product_attribute);

		return Db::getInstance()->getRow($sql);
	}

}