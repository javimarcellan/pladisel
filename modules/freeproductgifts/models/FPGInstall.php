<?php
/**
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * You must not modify, adapt or create derivative works of this source code
 *
 * @author    Musaffar Patel
 * @copyright 2016-2017 Musaffar Patel
 * @license   LICENSE.txt
 */

class FPGInstall extends FPGInstallCore
{
	public static function installDB()
	{
		$return = true;
		$return &= Db::getInstance()->execute('
		CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'fpg_product_gift` (
			`id_product` int(10) unsigned NOT NULL,
			`id_product_attribute` int(10) unsigned NOT NULL DEFAULT "0",
			`id_gift` int(10) unsigned NOT NULL,
			`id_shop` int(10) unsigned NOT NULL,
			`qty` mediumint(8) unsigned NOT NULL DEFAULT "1",
			`min_qty` mediumint(8) unsigned NOT NULL DEFAULT "1",
			`position` mediumint(8) unsigned NOT NULL DEFAULT "0",
			`enabled` tinyint(3) unsigned NOT NULL DEFAULT "0",
		PRIMARY KEY (`id_product`,`id_product_attribute`,`id_gift`,`id_shop`)
		) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8 ;');

		$return &= Db::getInstance()->execute('
			CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'fpg_cart_product` (
				`id_fpg_cart_product` int(10) unsigned NOT NULL AUTO_INCREMENT,
				`id_cart` int(10) unsigned NOT NULL,
				`id_product_parent` int(10) unsigned NOT NULL,
				`id_product_attribute_parent` int(10) unsigned NOT NULL,
				`id_product` int(10) unsigned NOT NULL,
				`id_product_attribute` int(10) unsigned NOT NULL,
			PRIMARY KEY (`id_fpg_cart_product`)
			) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8 ;');

		$return &= Db::getInstance()->execute('
			CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'fpg_cart_product_extra` (
				`id_fpg_cart_product_extra` int(10) unsigned NOT NULL AUTO_INCREMENT,
				`id_cart` int(10) unsigned NOT NULL DEFAULT \'0\',
				`id_product` int(10) unsigned NOT NULL DEFAULT \'0\',
				`id_product_attribute` int(10) unsigned NOT NULL DEFAULT \'0\',
				`fpg_freegift` tinyint(3) unsigned NOT NULL DEFAULT \'0\',
				`fpg_id_product_parent` int(10) unsigned NOT NULL DEFAULT \'0\',
				`fpg_id_product_attribute_parent` int(10) unsigned NOT NULL DEFAULT \'0\',
				PRIMARY KEY (`id_fpg_cart_product_extra`)			
			) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8 ;');

		$return &= Db::getInstance()->execute('
			CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'fpg_product_option` (
				`id_product_option` int(10) unsigned NOT NULL AUTO_INCREMENT,
				`id_product` int(10) unsigned NOT NULL,
				`total_qty_per_product` mediumint(8) unsigned NOT NULL DEFAULT \'0\',			
				PRIMARY KEY (`id_product_option`)			
			) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8 ;');

		Configuration::updateValue('additional_gifts_method', 'limit');
		return $return;
	}

	private static function addColumn($table, $name, $type)
	{
		try {
			$return = Db::getInstance()->execute('ALTER TABLE  `'._DB_PREFIX_.''.pSQL($table).'` ADD `'.pSQL($name).'` '.pSQL($type));
		} catch (Exception $e) {
			return true;
		}
		return true;
	}

	private static function dropColumn($table, $name)
	{
		Db::getInstance()->execute('ALTER TABLE  `'._DB_PREFIX_.''.pSQL($table).'` DROP `'.pSQL($name).'`');
	}

	public static function uninstallDB()
	{
		//self::dropTable('av_settings');
	}


}
