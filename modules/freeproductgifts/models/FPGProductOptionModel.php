<?php
/**
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * You must not modify, adapt or create derivative works of this source code
 *
 * @author    Musaffar Patel
 * @copyright 2016-2017 Musaffar Patel
 * @license   LICENSE.txt
 */

if (!defined('_PS_VERSION_'))
	exit;

class FPGProductOptionModel extends ObjectModel
{

	/** @var integer Unique ID */
	public $id_product_option;

	/** @var integer Product ID */
	public $id_product;

	/** @var integer Parent product ID */
	public $total_qty_per_product;

	/**
	 * @see ObjectModel::$definition
	 */

	public static $definition = array(
		'table' => 'fpg_product_option',
		'primary' => 'id_product_option',
		'multilang' => false,
		'fields' => array(
			'id_product' => array('type' => self::TYPE_INT),
			'total_qty_per_product' => array('type' => self::TYPE_INT)
		)
	);

	public function loadByProduct($id_product)
	{
		$sql = new DbQuery();
		$sql->select('*');
		$sql->from('fpg_product_option');
		$sql->where('id_product='.(int)$id_product);
		$result = DB::getInstance()->getRow($sql);
		if ($result)
			$this->hydrate($result);
	}

}