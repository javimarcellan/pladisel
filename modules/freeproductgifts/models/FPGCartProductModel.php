<?php
/**
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * You must not modify, adapt or create derivative works of this source code
 *
 * @author    Musaffar Patel
 * @copyright 2016-2017 Musaffar Patel
 * @license   LICENSE.txt
 */

if (!defined('_PS_VERSION_'))
	exit;

class FPGCartProductModel extends ObjectModel
{

	/** @var integer Unique ID */
	public $id_fpg_cart_product;

	/** @var integer Cart ID */
	public $id_cart;

	/** @var integer Parent product ID */
	public $id_product_parent;

	/** @var integer Parent product attribute ID */
	public $id_product_attribute_parent;

	/** @var integer product ID */
	public $id_product;

	/** @var integer product ID */
	public $id_product_attribute;

	/**
	 * @see ObjectModel::$definition
	 */

	public static $definition = array(
		'table' => 'fpg_cart_product',
		'primary' => 'id_fpg_cart_product',
		'multilang' => false,
		'fields' => array(
			'id_cart' => array('type' => self::TYPE_INT),
			'id_product_parent' => array('type' => self::TYPE_INT),
			'id_product_attribute_parent' => array('type' => self::TYPE_INT),
			'id_product' => array('type' => self::TYPE_INT),
			'id_product_attribute' => array('type' => self::TYPE_INT)
		)
	);

	/**
	 * Get based on parent product ID and IPA
	 * @param $id_product
	 * @param $id_product_attribute
	 * @param $id_cat
	 */
	public function loadByFreeGiftProduct($id_product, $id_product_attribute, $id_cart)
	{
		$sql = new DbQuery();
		$sql->select('*');
		$sql->from('fpg_cart_product');
		$sql->where('id_cart=' . (int)$id_cart);
		$sql->where('id_product=' . (int)$id_product);
		$sql->where('id_product_attribute=' . (int)$id_product_attribute);
		$row = DB::getInstance()->getRow($sql);
		if (!empty($row)) {
			$this->hydrate($row);
		}
	}

	/**
	 * Get based on parent product ID and IPA
	 * @param $id_product
	 * @param $id_product_attribute
	 * @param $id_cat
	 */
	public function loadByParentProduct($id_product, $id_product_attribute, $id_cart)
	{
		$sql = new DbQuery();
		$sql->select('*');
		$sql->from('fpg_cart_product');
		$sql->where('id_cart='.(int)$id_cart);
		$sql->where('id_product_parent='.(int)$id_product);
		$sql->where('id_product_attribute_parent='.(int)$id_product_attribute);

		$row = DB::getInstance()->getRow($sql);
		if (!empty($row)) {
			$this->hydrate($row);
		}
	}

	public function getByProduct($id_cart, $id_product, $id_product_attribute)
	{
		$sql = new DbQuery();
		$sql->select('*');
		$sql->from('fpg_cart_product');
		$sql->where('id_cart='.(int)$id_cart);
		$sql->where('id_product='.(int)$id_product);
		$sql->where('id_product_attribute='.(int)$id_product_attribute);
		$result = DB::getInstance()->executeS($sql);
		return $this->hydrateCollection('FPGCartProductModel', $result);
	}

	public function getByParentProduct($id_product_parent, $id_product_attribute_parent)
	{
		$sql = new DbQuery();
		$sql->select('*');
		$sql->from('fpg_cart_product');
		$sql->where('id_cart='.(int)$this->id_cart);
		$sql->where('id_product_parent='.(int)$id_product_parent);
		$sql->where('id_product_attribute_parent='.(int)$id_product_attribute_parent);
		$result = DB::getInstance()->executeS($sql);
		return $this->hydrateCollection('FPGCartProductModel', $result);
	}

	public static function getFreeProductCount($id_cart, $id_product_parent, $id_fpg_product)
	{
		$sql = new DbQuery();
		$sql->select('SUM(quantity) AS total_count');
		$sql->from('cart_product');
		$sql->where('id_cart='.(int)$id_cart);
		$sql->where('fpg_id_product_parent='.(int)$id_product_parent);
		$sql->where('id_product='.(int)$id_fpg_product);
		$row = DB::getInstance()->getRow($sql);

		if (!empty($row['total_count']))
			return $row['total_count'];
		else
			return 0;
	}

	public static function getCartFreeGiftCount($id_cart)
	{
		$sql = new DbQuery();
		$sql->select('SUM(quantity) AS total_count');
		$sql->from('cart_product');
		$sql->where('id_cart='.(int)$id_cart);
		$sql->where('fpg_freegift=1');
		$row = DB::getInstance()->getRow($sql);

		if (!empty($row['total_count']))
			return $row['total_count'];
		else
			return 0;
	}

	public static function isUnique($id_cart, $id_product_parent, $id_product_attribute_parent, $id_product, $id_product_attribute)
	{
		$sql = new DbQuery();
		$sql->select('COUNT(*) AS total_count');
		$sql->from(self::$definition['table']);
		$sql->where('id_cart='.(int)$id_cart);
		$sql->where('id_product_parent='.(int)$id_product_parent);
		$sql->where('id_product_attribute_parent='.(int)$id_product_attribute_parent);
		$sql->where('id_product='.(int)$id_product);
		$sql->where('id_product_attribute='.(int)$id_product_attribute);

		$row = DB::getInstance()->getRow($sql);

		if ($row['total_count'] > 0)
			return false;
		else
			return true;
	}

	/*
	 * @depreciated
	 */
	public static function getNonUnique($id_cart, $id_product, $id_product_attribute)
	{
		/*$sql = 'SELECT COUNT(*) as tc
				FROM '._DB_PREFIX_.'cart_product
				WHERE id_product='.(int)$id_product.'
				AND id_cart='.(int)$id_cart.'
				AND id_product_attribute < '.(int)$id_product_attribute.'
				AND fpg_freegift=1';*/

		$sql = '
			SELECT COUNT(*) as tc 
			FROM '._DB_PREFIX_.'cart_product cp
			INNER JOIN '._DB_PREFIX_.'fpg_cart_product_extra cpe ON cp.id_product = cpe.id_product 
				-- AND cp.id_product_attribute = cpe.id_product_attribute 
				AND cp.id_cart = cpe.id_cart
				AND cpe.fpg_freegift = 1
			WHERE cp.id_product='.(int)$id_product.'
			AND cp.id_cart='.(int)$id_cart.' 
			AND cp.id_product_attribute < '.(int)$id_product_attribute.' 	
		';
		$row = DB::getInstance()->getRow($sql);

		if (!empty($row['tc']))
			return $row['tc'];
		else return 0;
	}

	public static function getNonUniqueQty($id_cart, $id_product, $id_product_attribute)
	{
		$sql = '
			SELECT SUM(cp.quantity) as tc 
			FROM '._DB_PREFIX_.'cart_product cp
			INNER JOIN '._DB_PREFIX_.'fpg_cart_product_extra cpe ON cp.id_product = cpe.id_product 
				AND cp.id_product_attribute = cpe.id_product_attribute 
				AND cp.id_cart = cpe.id_cart
				AND cpe.fpg_freegift = 1
			WHERE cp.id_product='.(int)$id_product.'
			AND cp.id_cart='.(int)$id_cart.' 
			AND cp.id_product_attribute <= '.(int)$id_product_attribute.' 	
		';
		$row = DB::getInstance()->getRow($sql);

		if (!empty($row['tc']))
			return $row['tc'];
		else return 0;
	}

	public static function getPSCartProduct($id_cart, $id_product, $id_product_attribute, $id_shop)
	{
		$sql = new DbQuery();
		$sql->select('*');
		$sql->from('cart_product');
		$sql->where('id_cart='.(int)$id_cart);
		$sql->where('id_product='.(int)$id_product);
		$sql->where('id_product_attribute='.(int)$id_product_attribute);
		$sql->where('id_shop='.(int)$id_shop);

		$row = DB::getInstance()->getRow($sql);

		if (!empty($row))
			return $row;
		else
			return false;
	}


}