<?php

class FPGCartHelper {

    /**
     * Determineif the id_prouct provided is a free giftbased on products in the customer cart
     * @param $id_product
     */
    public static function isFreeGiftInCart($id_product) {
        $free_gifts_in_cart = FPGModel::getAllFreeGiftsAvailableForCart(Context::getContext()->cart->id, Context::getContext()->shop->id);

        if (!empty($free_gifts_in_cart[$id_product])) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Deermine how many free gift quantities afree gift item in the cart is eligible for
     * @param $id_gift_product
     * @param $id_gift_product_ipa
     * @param $cart_products_array
     * @return int
     * @throws PrestaShopDatabaseException
     */
    public static function getFreeGiftEligibileQty($id_gift_product, $id_gift_product_ipa) {
        $id_cart = Context::getContext()->cart->id;
        $id_shop = Context::getContext()->shop->id;
        $qty_eligible = 0;

        $sql = new DbQuery();
        $sql->select('*');
        $sql->from('fpg_cart_product', 'fcp');
        $sql->innerJoin('cart_product', 'cp', 'fcp.id_product_parent = cp.id_product and fcp.id_product_attribute_parent = cp.id_product_attribute AND cp.id_cart = '.$id_cart);
        $sql->where('fcp.id_cart='.(int)Context::getContext()->cart->id);
        $sql->where('fcp.id_product='.(int)$id_gift_product);
        $sql->where('fcp.id_product_attribute='.(int)$id_gift_product_ipa);
        $results = DB::getInstance()->executeS($sql);

        // when determining how many free gifts are eligible, we must treat all variations of a product as a single product
        $product_grouped_array = array();
        foreach ($results as $row) {
            if (empty($product_grouped_array[$row['id_product_parent']])) {
                $product_grouped_array[$row['id_product_parent']] = $row['quantity'];
            } else {
                $product_grouped_array[$row['id_product_parent']] += $row['quantity'];
            }
        }

        if (empty($product_grouped_array)) {
            return 0;
        }

        foreach ($product_grouped_array as $key => $product_group) {
            $fpg_product_gift = FPGModel::getProductGift($key, $id_gift_product, Context::getContext()->shop->id);
            if (!empty($fpg_product_gift)) {
                $qty_eligible += $fpg_product_gift['qty'] * floor($product_grouped_array[$key] / $fpg_product_gift['min_qty']);
            }
        }
        return $qty_eligible;
    }

    /**
     * @param $id_product
     * @param $id_product_attribute
     * @param $id_cart
     */
    public static function deleteAssociatedGifts($id_product, $id_product_attribute, $id_cart)
    {
        $sql = new DbQuery();
        $sql->select('*');
        $sql->from('fpg_cart_product_extra');
        $sql->where('fpg_id_product_parent='.(int)$id_product);
        $sql->where('fpg_id_product_attribute_parent='.(int)$id_product_attribute);
        $sql->where('id_cart='.(int)$id_cart);

        $results = DB::getInstance()->executeS($sql);

        if (!empty($results)) {
            foreach ($results as $row) {
                DB::getInstance()->delete('cart_product', 'id_cart=' . (int)$id_cart . ' AND id_product=' . (int)$row['id_product'] . ' AND id_product_attribute=' . (int)$row['id_product_attribute']);
            }
        }
    }

    public static function updateQty($id_product, $id_product_attribute, $id_cart, $qty)
    {
        if ($qty == 0) {
            if (Configuration::get('additional_gifts_method') == 'limit') {
                DB::getInstance()->delete('cart_product', 'id_cart=' . (int)$id_cart . ' AND id_product=' . (int)$id_product . ' AND id_product_attribute=' . (int)$id_product_attribute);
            }
        } else {
            DB::getInstance()->update('cart_product', array(
    		    'quantity' => $qty
            ), 'id_cart='.(int)$id_cart.' AND id_product='.(int)$id_product.' AND id_product_attribute='.(int)$id_product_attribute);
        }
    }

    /**
     * Ensures the modules cart tables has no free gifts which are no longer present in the cart
     * @param $id_cart
     */
    public static function syncFPGCartTables($id_cart)
    {
        $sql = new DbQuery();
        $sql->select('*');
        $sql->from('fpg_cart_product_extra');
        $sql->where('id_cart='.(int)$id_cart);

        $results = DB::getInstance()->executeS($sql);

        if (!empty($results)) {
            foreach ($results as $row) {
                $sql2 = new DbQuery();
                $sql2->select('count(*) AS total_count');
                $sql2->from('cart_product');
                $sql2->where('id_cart=' . (int)$id_cart);
                $sql2->where('id_product=' . (int)$row['id_product']);
                $sql2->where('id_product_attribute=' . (int)$row['id_product_attribute']);

                $row_count = DB::getInstance()->getRow($sql2);

                if (empty($row_count['total_count'])) {
                    DB::getInstance()->delete('fpg_cart_product_extra', 'id_cart=' . (int)$id_cart . ' AND id_product=' . (int)$row['id_product'] . ' AND id_product_attribute=' . (int)$row['id_product_attribute']);
                    DB::getInstance()->delete('fpg_cart_product', 'id_cart=' . (int)$id_cart . ' AND id_product=' . (int)$row['id_product'] . ' AND id_product_attribute=' . (int)$row['id_product_attribute']);
                }
            }
        }
    }

    public static function getCartProduct($id_product, $ipa, $id_cart)
    {
        $sql = new DbQuery();
        $sql->select('*');
        $sql->from('cart_product');
        $sql->where('id_cart=' . (int)$id_cart);
        $sql->where('id_product=' . (int)$id_product);
        $sql->where('id_product_attribute='.(int)$ipa);
        $row = DB::getInstance()->getRow($sql);
        if (!empty($row)) {
            return $row;
        } else {
            return false;
        }
    }

    /**
     * Determine how many free gifts have been rewarded to a product
     * @param $id_product
     * @param $ipa
     */
    public static function calculateFreeGiftQty($id_product, $ipa, $id_cart)
    {
        $fpg_cart_product_extra = new FPGCartProductExtraModel();
        $result = $fpg_cart_product_extra->getByParentProduct($id_product, $ipa, $id_cart);
        $qty = 0;

        if (!empty($result)) {
            foreach ($result as $row) {
                $cart_row = self::getCartProduct($row->id_product, $row->id_product_attribute, $id_cart);
                if ($cart_row) {
                    $qty += $cart_row['quantity'];
                }
            }
        }
        return $qty;
    }

}