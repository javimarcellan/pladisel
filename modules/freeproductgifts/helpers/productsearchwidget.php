<?php
/**
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * You must not modify, adapt or create derivative works of this source code
 *
 * @author    Musaffar Patel
 * @copyright 2016-2017 Musaffar Patel
 * @license   LICENSE.txt
 */

class ProductSearchWidget
{

	public function searchProducts($search_text)
	{
		$products = array();
		$sql = 'SELECT
					p.id_product,
					pl.name
				FROM '._DB_PREFIX_.'product p
				INNER JOIN '._DB_PREFIX_.'product_lang pl ON p.id_product = pl.id_product
				WHERE name LIKE "%'.pSQL($search_text).'%"
				AND pl.id_lang = '.(int)Context::getContext()->language->id.'
				AND pl.id_shop='.(int)Context::getContext()->shop->id;
		$results = DB::getInstance()->executeS($sql);
		return $results;
	}

}