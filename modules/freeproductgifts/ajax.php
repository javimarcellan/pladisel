<?php
/**
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * You must not modify, adapt or create derivative works of this source code
 *
 * @author    Musaffar Patel
 * @copyright 2016-2017 Musaffar Patel
 * @license   LICENSE.txt
 */

include(dirname(__FILE__).'/../../config/config.inc.php');
include(dirname(__FILE__).'/../../init.php');
include_once(_PS_MODULE_DIR_.'/freeproductgifts/lib/bootstrap.php');

$module = Module::getInstanceByName('freeproductgifts');

if (Tools::getValue('section') != '')
{
	switch (Tools::getValue('section'))
	{
		case 'adminproducttab' :
			die($module->hookDisplayAdminProductsExtra(array()));

		case 'mpproductsearchwidgetcontroller' :
			$mp_product_search_widget = new MPProductSearchWidgetController();
			die(Tools::jsonEncode($mp_product_search_widget->route()));
			
	}
}
else
{
	switch (Tools::getValue('route'))
	{
		case 'fpgfrontcontroller' :
			$fpg_front_controller = new FPGFrontController($module);
			die($fpg_front_controller->route());

		case 'mpproductsearchwidgetcontroller' :
			$mp_product_search_widget = new MPProductSearchWidgetController();
			die(Tools::jsonEncode($mp_product_search_widget->route()));
	}
}
