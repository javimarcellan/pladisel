<?php
/**
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * You must not modify, adapt or create derivative works of this source code
 *
 * @author    Musaffar Patel
 * @copyright 2016-2017 Musaffar Patel
 * @license   LICENSE.txt
 */

include_once(_PS_MODULE_DIR_.'/freeproductgifts/lib/bootstrap.php');

class Product extends ProductCore
{
	public static function priceCalculation($id_shop, $id_product, $id_product_attribute, $id_country, $id_state, $zipcode, $id_currency, $id_group, $quantity, $use_tax, $decimals, $only_reduc, $use_reduc, $with_ecotax, &$specific_price, $use_group_reduction, $id_customer = 0, $use_customer_price = true, $id_cart = 0, $real_quantity = 0, $id_customization = 0)
	{
		$price = parent::priceCalculation(
			$id_shop, $id_product, $id_product_attribute, $id_country, $id_state, $zipcode, $id_currency,
			$id_group, $quantity, $use_tax, $decimals, $only_reduc, $use_reduc, $with_ecotax, $specific_price, $use_group_reduction,
			$id_customer, $use_customer_price, $id_cart, $real_quantity, $id_customization
		);

		if ($id_cart && Module::isEnabled('FreeProductGifts'))
		{
			$price = Hook::exec('fpgPriceCalculation', array(
				'price' => $price,
				'id_product' => $id_product,
				'id_product_attribute' => $id_product_attribute,
				'quantity' => $quantity,
				'id_cart' => $id_cart,
				'id_shop' => $id_shop
			));
			return (float)$price;
		}
		else return $price;
	}
	
	public static function getProductsProperties($id_lang, $query_result)
	{
		$results_array = parent::getProductsProperties($id_lang, $query_result);

		if (!Module::isEnabled('FreeProductGifts')) return $results_array;

		include_once(_PS_MODULE_DIR_.'/freeproductgifts/lib/bootstrap.php');

		foreach ($results_array as &$product)
		{
			$fpg_product = FPGModel::getProductGifts($product['id_product'], null, Context::getContext()->shop->id);
			if (!empty($fpg_product) && count($fpg_product->gift_ids) > 0)
				$product['fpg_count'] = count($fpg_product->gift_ids);
			else
				$product['fpg_count'] = 0;
		}
		return $results_array;
	}	
}