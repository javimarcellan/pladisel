{*
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author Musaffar Patel <musaffar.patel@gmail.com>
*  @copyright  2007-2017 Musaffar Patel
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Property of Musaffar Patel
*}

<table class="table">
	<thead>
	<tr>
		<th>Gift Product</th>
		<th>Qty</th>
		<th>Min Qty</th>
		<th>Enabled</th>
		<th>Action</th>
	</tr>
	</thead>
	<tbody>
		{foreach from=$free_products item=gift}
		<tr data-id="{$gift.id_gift|escape:'html':'UTF-8'}">
			<td>{$gift.name|escape:'html':'UTF-8'}</td>
			<td>{$gift.qty|escape:'html':'UTF-8'}</td>
			<td>{$gift.min_qty|escape:'html':'UTF-8'}</td>
			<td>{$gift.enabled|escape:'html':'UTF-8'}</td>
			<td>
				<i class="fpg-gift-edit material-icons" style="cursor: pointer">mode edit</i>
				<i class="fpg-gift-delete material-icons" style="cursor: pointer;">delete forever</i>
			</td>
		</tr>
		{/foreach}
	</tbody>
</table>
<button id="btn-add-free-gift" type="button" class="btn btn-primary">Add Free Gift</button>

<div id="fpg-options" class="form-wrapper" style="margin-top:40px;">
	<h3>Options</h3>

	<div class="form-group">
		<label class="control-label col-lg-3">
			{l s='Total free gifts allowed for this product' mod='freeproductgifts'}
		</label>

		<div class="col-lg-9">
			<input name="total_qty_per_product" id="total_qty_per_product" value="{$fpg_product_option_model|escape:'html':'UTF-8'}" type="text" />
		</div>
	</div>

	<button id="btn-fpg-save-options" type="button" class="btn btn-primary">Save</button>

</div>
