{*
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author Musaffar Patel <musaffar.patel@gmail.com>
*  @copyright  2007-2017 Musaffar Patel
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Property of Musaffar Patel
*}

<div id="form-fpg-edit" class="form-wrapper fpg-form-wrapper">

	<div class="alert alert-danger mp-errors" style="display: none"></div>

	<div class="form-group row">
		<label class="control-label col-lg-2">
			Enabled?
		</label>

		<div class="col-lg-10">
			<input data-toggle="switch" class="" id="enabled" data-inverse="true" type="checkbox" name="enabled" value="1" {if !empty($fpg_entry->enabled)}checked{/if}>
		</div>
	</div>

	<div class="form-group row">
		<label class="control-label col-lg-2">
			Free Gift Product?
		</label>

		<div class="col-lg-10">

			<i class="icon-search"></i>
			<input id="product_search" type="text" name="product_search" value="{if !empty($product_name)}{$product_name|escape:'html':'UTF-8'}{/if}" class="form-control search" placeholder="Search and add a product as free gift" autocomplete="off" />


			{* Product search results *}
			<div id="product-search-results" class="autocomplete-search" style="display: none">
				<input name="id_product" id="id_product" type="hidden" data-required="required" data-validation-message="Please select a product" value="{if !empty($fpg_entry->id_gift)}{$fpg_entry->id_gift|escape:'html':'UTF-8'}{/if}">

				<ul class="search-results-table">
					<li class="cloneable hidden card" style="cursor:pointer">
						(<span class="id_product label" data-bind="id_product"></span>)
						<span class="name label" data-bind="name"></span>
					</li>
				</ul>
			</div>
			{* / Product Search Results *}

		</div>
	</div>

	<div class="form-group row">
		<label class="control-label col-lg-2">
			Free Qty
		</label>

		<div class="col-lg-10">
			<input class="form-control" id="qty" type="text" name="qty" value="{if !empty($fpg_entry->qty)}{$fpg_entry->qty|escape:'html':'UTF-8'}{else}1{/if}" />
		</div>
	</div>

	<div class="form-group row">
		<label class="control-label col-lg-2">
			Min Qty
		</label>

		<div class="col-lg-10">
			<input class="form-control" id="min_qty" type="text" name="min_qty" data-required="required" value="{if !empty($fpg_entry->min_qty)}{$fpg_entry->min_qty|escape:'html':'UTF-8'}{else}1{/if}" />
		</div>
	</div>

	<div class="form-group row">
		<label class="control-label col-lg-2">
			Position
		</label>

		<div class="col-lg-10">
			<input class="form-control" id="position" type="text" name="position" value="{if !empty($fpg_entry->position)}{$fpg_entry->position|escape:'html':'UTF-8'}{else}1{/if}" />
		</div>
	</div>

</div>



<button type="button" id="fpg-btn-edit-save" class="btn btn-primary">Save</button>
<button type="button" id="btn-fpg-cancel-edit" class="btn btn-primary-outline">Cancel</button>

<script>
	$(document).ready(function () {
		prestaShopUiKit.init();
	});
</script>