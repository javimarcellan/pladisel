{*
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author Musaffar Patel <musaffar.patel@gmail.com>
*  @copyright  2007-2017 Musaffar Patel
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Property of Musaffar Patel
*}

{if isset($fpg_products) && $fpg_products|@count gt 0}
	<div id="fpg-widget-wrapper">	
		<input id="id_gift" name="id_gift" type="hidden" value=""/>
		<input id="id_gift_ipa" name="id_gift_ipa" type="hidden" value=""/>
		<span class="fpg-widget-title">{l s='Choose your free product' mod='freeproductgifts'}</span>
		<ul>
			{foreach from=$fpg_products item=fpg_product name=fpg_product}
				<li class="fpg_product anim-all-100" id="{$fpg_product->id|escape:'htmlall':'UTF-8'}">
					<div class="content">
						<img src="{$fpg_product->image|escape:'html':'UTF-8'}" class="fpg_thumb" />
						<span class="fpg-name">{$fpg_product->name|truncate:55:'...'|escape:'html':'UTF-8'}</span>

						{if isset($fpg_product->attribute_groups)}
							{if count($fpg_product->attribute_groups) > 0}
								<select name="fpg_ipa">
									{foreach from=$fpg_product->attribute_groups item=attribute_group name=attribute_group}
										<option value="{$attribute_group.id_product_attribute|escape:'htmlall':'UTF-8'}">{$attribute_group.label|escape:'htmlall':'UTF-8'}</option>
									{/foreach}
								</select>
							{/if}
						{/if}
						<a class="fpg_select anim-all-100">
							<span class="add anim-all-100">+</span>
							<i class="added icon icon-check"></i>
						</a>
						<a href="{$fpg_product->link|escape:'html':'UTF-8'}" class="fpg_info anim-all-100" title="{$fpg_product->description_short|strip_tags}">
							<span class="">...</span>
						</a>
					</div>
				</li>
			{/foreach}
		</ul>	
	</div>
{/if}	
{*
<script>

	{if isset($smarty.get.content_only)}
		content_only = '{$smarty.get.content_only|escape:'html':'UTF-8'}';
	{else}
		content_only = '';
	{/if}

	$(document).ready(function() {
		allow_multi_select = true;
		fpg_product_options = {$fpg_product_options|@json_encode};
		console.log(fpg_product_options);
	    var fpg_product_controller = new FPGProductController();
	});
</script>
*}