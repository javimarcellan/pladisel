/*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author Musaffar Patel <musaffar.patel@gmail.com>
*  @copyright  2015-2017 Musaffar
*  @version  Release: $Revision$
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Property of Musaffar Patel
*/

ProductSearchWidget = function(id) {
	var self = this;
	self.id = id;
	self.$textbox = $("input#"+self.id);
	self.$select = $("select[name='"+self.id+"']");
	self.$hidden = $("input."+self.id);
	self.initialised = false;

	self.refreshProductList = function(array_products) {
		if (array_products.length > 0) {
			self.$select.empty();
			for (var i=0; i<array_products.length; i++) {
				self.$select
					.append($("<option></option>")
					.attr("value", array_products[i]['id_product'])
					.text(array_products[i]['name']));
			}
			if (self.$hidden.val() != '') self.$select.val(self.$hidden.val());
			if (!self.initialised) self.$select.val(self.$select.find("option:first").val());
		}
	}

	/* events */
	self.doSearch = function() {
		$.ajax({
			type: 'POST',
			headers: {"cache-control": "no-cache"},
			//url: module_dir + 'freeproductgifts/ajax.php?rand=' + new Date().getTime(),
			url: module_tab_url + '&rand=' + new Date().getTime() + '&route=fpgproducttabeditcontroller&do=admin_productsearch&search_text=' + self.$textbox.val(),
			async: true,
			cache: false,
			//data: '&do=admin_productsearch&search_text=' + self.$textbox.val(),
			dataType: 'json',
			complete: function (jsonData, textStatus, jqXHR) {
			},
			success: function (result) {
				self.refreshProductList(result);
			},
			error: function (XMLHttpRequest, textStatus, errorThrown) {
			}
		});
	}

	/* constructor */
	self.init = function () {
		self.doSearch();
	}
	self.init();

	$("body").on("change", "select[name=" + self.id + ']', function () {
		self.$textbox.val($("select[name='"+self.id+"'] option:selected").text());
	});

	$("body").on("keydown", "input#"+self.id, function() {
		self.doSearch();
	});

}

ProductTabEditController = function() {
	self = this;
	self.searchWidget;

	self.init = function() {
		self.searchWidget = new ProductSearchWidget('id_gift');
	}
	self.init();
}

$(document).ready(function() {
	var productTabEditController = new ProductTabEditController();
});
