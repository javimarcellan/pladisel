/*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author Musaffar Patel <musaffar.patel@gmail.com>
*  @copyright  2015-2017 Musaffar
*  @version  Release: $Revision$
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Property of Musaffar Patel
*/

MPProductSearchWidget = function(wrapper, module_ajax_url) {
	var self = this;
	self.wrapper = wrapper;
	self.module_ajax_url = module_ajax_url;

	self.pk = 'id_product'; //unique idebntifier for each row,. data binded to database table

	self.init = function() {
	};
	self.init();

	self.showResultsList = function () {
		$(self.wrapper).find("#product-search-results").slideDown();
	};

	self.hideResultsList = function () {
		$(self.wrapper).find("#product-search-results").slideUp();
	};

	self.clearResultsList = function () {
		$(self.wrapper).find(".search-results-table li.result-item").remove();
	};

	self.addToResultList = function (jsonRow) {
		$results_table = $(self.wrapper).find(".search-results-table");

		var $cloned = $results_table.find("li.cloneable").clone();
		$cloned.removeClass("cloneable");
		$cloned.removeClass("hidden");
		$cloned.addClass("result-item");

		$.each(jsonRow, function (key, value) {
			$cloned.find("span."+key).html(value);
			$cloned.attr("data-"+key, value);
		});
		$cloned.appendTo($results_table);
	};

	self.onResultSelect = function (id, selected_text) {
		$(self.wrapper).find('input[name="'+self.pk+'"]').val(id);
		$(self.wrapper + " input#product_search").val(selected_text);
	};

	self.popupProcessSearch = function (search_string, scope) {
		var url = self.module_ajax_url + '?route=mpproductsearchwidgetcontroller&action=processsearch';

		$.ajax({
			type: 'POST',
			url: url,
			async: true,
			cache: false,
			dataType: "json",
			data: {
				'search_string': search_string,
				'scope': scope
			},
			success: function (jsonData) {
				self.clearResultsList();
				self.showResultsList();
				for (var x = 0; x <= jsonData.length - 1; x++) {
					self.addToResultList(jsonData[x])
				}
				return false;
			}
		});
	};

	$("body").on("keyup", self.wrapper + " #product_search", function () {
		self.popupProcessSearch($(this).val(), $('#' + self.popupFormId + " #scope").val())
	});

	$("body").on("click", self.wrapper + " .result-item", function () {
		self.onResultSelect($(this).attr("data-"+self.pk), $(this).attr("data-name"));
		self.hideResultsList();
	});
}