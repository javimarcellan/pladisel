/*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author Musaffar Patel <musaffar.patel@gmail.com>
*  @copyright  2015-2017 Musaffar
*  @version  Release: $Revision$
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Property of Musaffar Patel
*/

FPGAdminProductTabEditGiftController = function(canvas) {

	var self = this;
	self.canvas = canvas;
	self.$canvas = $(canvas);

	self.renderForm = function(id_gift) {
		var url = module_ajax_url_fpg + '?section=adminproducttab&route=fpgadminproducttabeditgiftcontroller&action=renderform';
		breadcrumb.add('Add / Edit Gift', url);

		if (id_gift == null) id_gift = 0;

		MPTools.waitStart();
		$.ajax({
			type: 'POST',
			url: url,
			async: true,
			cache: false,
			data: {
				'id_product' : id_product,
				'id_gift' : id_gift
			},
			success: function (html_result) {
				self.$canvas.html(html_result);
				MPTools.waitEnd();
			}
		});
	};

	/**
	 * Save the form
	 */
	self.processForm = function() {
		var url = module_ajax_url_fpg + '?section=adminproducttab&route=fpgadminproducttabeditgiftcontroller&action=processform';

		var form_data = self.$canvas.find("#form-fpg-edit :input").serialize();
		var form_data = form_data + '&id_parent_product=' + id_product;

		MPTools.waitStart();

		$.ajax({
			type: 'POST',
			url: url,
			async: true,
			cache: false,
			//dataType: "json",
			data: form_data,
			success: function (result) {
				breadcrumb.cancel();
				MPTools.waitEnd();
			}
		});
	};

	self.init = function() {
		productsearch = new MPProductSearchWidget(self.canvas, module_ajax_url_fpg);
	};
	self.init();

	$(self.canvas).on("click", '#btn-fpg-cancel-edit', function () {
		breadcrumb.cancel();
	});

	$(self.canvas).on("click", '#fpg-btn-edit-save', function() {
		if (MPTools.validateForm("#form-fpg-edit"))
			self.processForm();
	});

};

