/*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author Musaffar Patel <musaffar.patel@gmail.com>
*  @copyright  2015-2017 Musaffar
*  @version  Release: $Revision$
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Property of Musaffar Patel
*/

FPGAdminProductTabController = function(canvas) {

	var self = this;
	self.canvas = canvas;
	self.$canvas = $(canvas);
	
	/* sub controllers */
	self.edit_gift_controller = {}; //FPGAdminProductTabEditGiftController();


	self.renderList = function() {
		MPTools.waitStart();

		var url = module_ajax_url_fpg + '?section=adminproducttab&route=fpgadminproducttabcontroller&action=render';

		var post_data = {
			'id_product': id_product
		};

		breadcrumb.add('Free Gifts', url, post_data);

		$.ajax({
			type: 'POST',
			url: url,
			async: true,
			cache: false,
			data: post_data,
			success: function (html_result) {
				self.$canvas.html(html_result);
				MPTools.waitEnd();
			}
		});
	};

	/**
	 * save the options
	 * @returns {boolean}
	 */
	self.saveOptions = function() {

		var url = module_ajax_url_fpg + '?section=adminproducttab&route=fpgadminproducttabcontroller&action=processform';

		var form_data = self.$canvas.find("#fpg-options :input").serialize();
		var form_data = form_data + '&id_product=' + id_product;

		MPTools.waitStart();
		$.ajax({
			type: 'POST',
			url: url,
			async: true,
			cache: false,
			//dataType: "json",
			data: form_data,
			success: function (result) {
				MPTools.waitEnd();
			}
		});
		return false;
	};

	/** delete a gift associated with a producrt */
	self.delete = function(id_gift) {
		var url = module_ajax_url_fpg + '?section=adminproducttab&route=fpgadminproducttabcontroller&action=processdelete';

		var post_data = {
			'id_product': id_product,
			'id_gift': id_gift
		};

		MPTools.waitStart();

		$.ajax({
			type: 'POST',
			url: url,
			async: true,
			cache: false,
			data: post_data,
			success: function (result) {
				MPTools.waitEnd();
				breadcrumb.pop();
				self.renderList();
			}
		});
	};

	self.init = function() {
		self.edit_gift_controller = new FPGAdminProductTabEditGiftController(self.canvas);
		self.renderList();
	};
	self.init();

	$(self.canvas).on("click", '#btn-add-free-gift', function () {
		self.edit_gift_controller.renderForm();
	});

	$(self.canvas).on("click", '.fpg-gift-edit', function () {
		var id_gift = $(this).parents("tr").attr("data-id");
		self.edit_gift_controller.renderForm(id_gift);
	});

	$(self.canvas).on("click", '.fpg-gift-delete', function () {
		var id_gift = $(this).parents("tr").attr("data-id");
		self.delete(id_gift);
	});

	$(self.canvas).on("click", '#btn-fpg-save-options', function () {
		self.saveOptions();
	});

};

