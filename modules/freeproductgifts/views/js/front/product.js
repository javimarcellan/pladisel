/*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author Musaffar Patel <musaffar.patel@gmail.com>
*  @copyright  2015-2017 Musaffar
*  @version  Release: $Revision$
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Property of Musaffar Patel
*/

FPGProductController = function() {
	var self = this;

	self.wrapper = "#fpg-widget-wrapper";
	self.$wrapper = $(self.wrapper);

	self.renderWidget = function() {
        $.ajax({
            type: 'POST',
            headers: { "cache-control": "no-cache" },
            url: module_ajax_url + '?route=fpgfrontcontroller&action=renderwidget&rand=' + new Date().getTime(),
            async: true,
            cache: false,
            //dataType : "json",
            data: {
				id_product: id_product
			},
            success: function(html) {
				$(html).insertBefore("div.product-add-to-cart");
				self.$wrapper = $("#fpg-widget-wrapper");
            }
        });
	};

    self.init = function() {
		/* select first free gift as default */
		self.renderWidget();
		self.$wrapper.find(".fpg_product").first().addClass('selected');
    };
    self.init();

    /* Events */

	$("body").on("click", self.wrapper + ' .fpg_select', function() {
		var selected_count = self.$wrapper.find(".selected").length;

		if (selected_count >= fpg_product_options.total_qty_per_product) {
			var extra_selected = (selected_count - fpg_product_options.total_qty_per_product) + 1;
			self.$wrapper.find(".selected").slice(0, extra_selected).removeClass("selected");
		}

        $fpg_product = $(this).parents(".fpg_product");
        $fpg_product.toggleClass("selected");

		// add data to product buty form

		// get id's of all gifts
		var gift_ids = [];
		if (fpg_product_options.total_qty_per_product > 1) {
			self.$wrapper.find(".fpg_product.selected").each(function () {
				gift_ids.push($(this).attr("id") + ':' + $(this).find("select[name='fpg_ipa']").val());
			});
			id_gift = gift_ids.join(',');
		} else {
			$fpg_selected = self.$wrapper.find(".fpg_product.selected");
	        if ($fpg_selected.length == 0) return false;

    	    id_gift = $fpg_selected.attr("id");
			id_gift_ipa = $fpg_selected.find("select[name='fpg_ipa']").val();
		}

		//id_gift = $fpg_product.attr("id");
		//id_gift_ipa = $fpg_product.find("select[name='fpg_ipa']").val();

		self.$wrapper.find("input#id_gift").val(id_gift);
		self.$wrapper.find("input#id_gift_ipa").val(id_gift_ipa);
		return false;
    });

	/**
	 * On Gift combination change
 	 */
	$("body").on("change", self.wrapper + ' select[name="fpg_ipa"]', function() {
		$fpg_product = $(this).parents(".fpg_product");
		$fpg_product.find(".fpg_select").trigger("click");
	});

};

var fpg_product_controller = new FPGProductController();