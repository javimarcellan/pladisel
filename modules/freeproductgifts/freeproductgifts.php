<?php
/**
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * You must not modify, adapt or create derivative works of this source code
 *
 * @author    Musaffar Patel
 * @copyright 2016-2017 Musaffar Patel
 * @license   LICENSE.txt
 */

if (!defined('_PS_VERSION_'))
	exit;

include_once(_PS_MODULE_DIR_.'/freeproductgifts/lib/bootstrap.php');

class FreeProductGifts extends Module
{
	/** @var FPGProductTabController */
	protected $controller_product_tab;

	/** @var AVFrontController */
	protected $controller_front;

	/** @var AVConfigController */
	protected $controller_config;

	public $module_file;

	public function __construct()
	{
		$this->name = 'freeproductgifts';
		$this->tab = 'others';
		$this->version = '2.0.6';
		$this->author = 'Musaffar Patel';
		$this->need_instance = 0;
		$this->module_key = '557507429b348929356fcdbad54491ac';
		$this->confirmUninstall = $this->l('Are you sure you want to uninstall?');

		parent::__construct();
		$this->displayName = $this->getTranslator()->trans('Free Product Gifts', array(), 'Modules.FreeProductGifts');
		$this->description = $this->getTranslator()->trans('Allow customers to receive free gift with selected products purchased from your store', array(), 'Modules.FreeProductGifts');

		$this->bootstrap = true;
		$this->module_file = __FILE__;

		$this->ps_versions_compliancy = array('min' => '1.7.0.0', 'max' => _PS_VERSION_);
	}

	public function install()
	{
		if (parent::install() == false
			|| !$this->registerHook('fpgPriceCalculation')
			|| !$this->registerHook('displayShoppingCart')
			|| !$this->registerHook('renderProductPageWidget')
			|| !$this->registerHook('displayRightColumnProduct')
			|| !$this->registerHook('displayLeftColumnProduct')
			|| !$this->registerHook('displayAdminProductsExtra')
			|| !$this->registerHook('actionCartSave')
			|| !$this->registerHook('displayHeader')
			|| !$this->registerHook('displayFPGIcon')
			|| !$this->registerHook('fpgDeleteCartProduct')
			|| !$this->registerHook('backOfficeHeader')
			|| !$this->registerHook('displayFooterProduct')
			|| !$this->registerHook('actionDeleteProductInCartAfter')
			|| !$this->registerHook('filterProductSearch')
			|| !$this->installModule()) return false;
		return true;
	}

	public function uninstall()
	{
		parent::uninstall();
		//FPGInstall::uninstallDB();
		return true;
	}

	public function installModule()
	{
		return FPGInstall::installDB();
	}

	public function setMedia()
	{
		(new FPGAdminProductTabController($this))->setMedia();
		(new FPGFrontController($this))->setMedia();
	}

	public function getContent()
	{
		$render = '';
		$fpg_general = new FPGAdminConfigGeneralController($this);
		$render .= $fpg_general->route();
		return $render;
	}


	/* hooks */
	public function hookFpgPriceCalculation($params)
	{
		$front_controller = new FPGFrontController($this);
		return $front_controller->hookFpgPriceCalculation($params);
	}

	public function hookDisplayShoppingCart($params)
	{
		$front_controller = new FPGFrontController($this);
		$front_controller->hookDisplayShoppingCart($params);
	}

	public function hookRenderProductPageWidget($params)
	{
		$front_controller = new FPGFrontController($this);
		return $front_controller->hookRenderProductPageWidget($params);
	}

	public function hookDisplayAdminProductsExtra($params)
	{
		$this->controller_product_tab = new FPGAdminProductTabController($this, $params);
		return $this->controller_product_tab->route();
	}

	public function hookActionCartSave($params)
	{
		//if (Tools::getValue('delete') == 'true') return false;
		$front_controller = new FPGFrontController($this);
		if (Tools::getValue('add') == '1') {
			$front_controller->addFPGToCart($params);
			$front_controller->updateCartFreeGiftQuantity(Tools::getValue('id_product'), Tools::getValue('id_product_attribute'));
		}

		if (Tools::getIsset('op')) {
			error_log(Tools::getValue('op'));
			$front_controller->updateCartFreeGiftQuantity(Tools::getValue('id_product'), Tools::getValue('id_product_attribute'));
		}

		// is qty being updated in cart?
		/*if ((Tools::getIsset('op') || (Tools::getIsset('add')) && Tools::getIsset('summary')))
			$front_controller->updateCartFreeGiftQuantities();*/
		return true;
	}

	public function hookDisplayHeader()
	{
		$this->setMedia();
	}


	/* Ajax Handlers (routed via ajax.php) */
	public function addFPGToCart()
	{
		//$front_controller = new FPGFrontController($this);
		//$front_controller->addFPGToCart();
		//return $front_controller->updateCartFreeGiftQuantities();
	}

	public function hookDisplayFPGIcon($params)
	{
		//$front_controller = new FPGFrontController($this);
		//return $front_controller->hookRenderFPGIcon($params);
	}

	/*public function hookFpgDeleteCartProduct($params)
	{
		$front_controller = new FPGFrontController($this);
		return $front_controller->deleteFPGCartProduct($params);
	}*/

	public function hookBackOfficeHeader($params)
	{
		$this->setMedia();
	}

	public function hookActionDeleteProductInCartAfter($params)
	{
	}

	/**
	 * Add the free gift ribbon to the product listing
	 * @param $params
	 */
	public function hookFilterProductSearch($params)
	{
		(new FPGFrontController($this))->hookFilterProductSearch($params);
	}


}