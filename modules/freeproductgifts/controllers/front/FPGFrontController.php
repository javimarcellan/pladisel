<?php
/**
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * You must not modify, adapt or create derivative works of this source code
 *
 * @author    Musaffar Patel
 * @copyright 2016-2017 Musaffar Patel
 * @license   LICENSE.txt
 */

class FPGFrontController extends FPGControllerCore
{

	protected $sibling;

	public function __construct(&$sibling = null)
	{
		parent::__construct($sibling);
		if ($sibling !== null)
			$this->sibling = &$sibling;

		if (Tools::getValue('id_product') != '' && Context::getContext()->controller->php_self == 'product')
		{
			$this->fpg_product_option_model = new FPGProductOptionModel();
			$this->fpg_product_option_model->loadByProduct(Tools::getValue('id_product'));
		}
	}

	public function setMedia()
	{
		if ($this->sibling !== null && isset(Context::getContext()->controller->php_self))
		{
			if (Context::getContext()->controller->php_self == 'cart')
				$this->sibling->context->controller->addJS($this->sibling->_path.'views/js/front/cart.js');

			if (Context::getContext()->controller->php_self == 'product')
			{
				$this->sibling->context->controller->addCSS($this->sibling->_path.'views/css/front/freeproductgifts.css');
				$this->sibling->context->controller->addJS($this->sibling->_path.'views/js/front/product.js');
				Media::addJsDef(array(
					'id_product' => Tools::getValue('id_product'),
					'module_ajax_url' => $this->module_ajax_url,
					'fpg_product_options' => $this->fpg_product_option_model
				));
			}
		}
	}

	/* Determine how many products with a free gift are in the cart, irrespective of attribute id */
	private function _getParentCartProductCount($id_product, $id_product_attribute = 0, $id_cart = 0)
	{
		if ($id_cart == 0)
			$id_cart = Context::getContext()->cart->id;

		$parent_cart_product_count = 0;
		$cart_products_raw = FPGModel::getCartProductsLight($id_cart, true);

		if (!empty($cart_products_raw))
		{
			foreach ($cart_products_raw as $c_product)
			{
				if ($c_product['id_product'] == $id_product)
					$parent_cart_product_count = $parent_cart_product_count + $c_product['quantity'];
			}
		}
		return $parent_cart_product_count;
	}


	private function _getFreeQtyAvailable($fpg_cart_info, $fpg_info)
	{
		$parent_cart_product_count = $this->_getParentCartProductCount($fpg_cart_info);
		$free_qty_avail = ($parent_cart_product_count / $fpg_info->min_qty) * $fpg_info->qty;
		return $free_qty_avail;
	}

	public function hookFpgPriceCalculation($params)
	{
        FPGCartHelper::syncFPGCartTables(Context::getContext()->cart->id);

		// is this product a free gift ?
		$fpg_cart_info = FPGModel::getCartProductFPGInfo($params['id_cart'], $params['id_product'], $params['id_product_attribute']);
		$id_cart = Context::getContext()->cart->id;
		$id_shop = Context::getContext()->shop->id;

		if (!$fpg_cart_info['fpg_freegift'])
			return $params['price'];
		else {
            //this is a free gift product
			$fpg_cart_product_model = new FPGCartProductModel();
			$fpg_cart_product_model->loadByFreeGiftProduct($params['id_product'], $params['id_product_attribute'], Context::getContext()->cart->id);

			$qty_eligible = FPGCartHelper::getFreeGiftEligibileQty($fpg_cart_product_model->id_product, $fpg_cart_product_model->id_product_attribute);
			$freegift_qty_rewarded = FPGCartHelper::calculateFreeGiftQty($fpg_cart_product_model->id_product_parent, $fpg_cart_product_model->id_product_attribute_parent, $id_cart);  // how many free gifts does this product already have?

            $fpg_cart_product_extra_model = new FPGCartProductExtraModel();
            $free_gifts = $fpg_cart_product_extra_model->getByParentProduct($fpg_cart_product_model->id_product_parent, $fpg_cart_product_model->id_product_attribute_parent, $id_cart); // all free gifts associated with parent product

            // determine how many can be given away free based on quantities of previous gifts associated with this parent product in the cart
            $qty_running = 0;
            if (!empty($free_gifts)) {
                foreach ($free_gifts as $gift) {
                    $cart_product = FPGCartHelper::getCartProduct($gift->id_product, $gift->id_product_attribute, $id_cart);
                    $qty_running += $cart_product['quantity'];
                    if ($gift->id_product == $params['id_product'] && $gift->id_product_attribute == $params['id_product_attribute']) {
                        break;
                    }
                }
            }

            if ($qty_running > $qty_eligible) {
                $qty_eligible = $params['quantity'] - ($qty_running - $qty_eligible);
            }

			if (Configuration::get('additional_gifts_method', null, 0, 0) == 'charge')
			{
				$qty_not_eligible = $params['quantity'] - $qty_eligible;
				if ($params['quantity'] > $qty_eligible) {
					return ($params['price'] * $qty_not_eligible) / $params['quantity'];
				} else {
					return 0.00;
				}
			}
			else {
                if ($qty_running > $qty_eligible) {
                    $qty_not_eligible = $params['quantity'] - $qty_eligible;
                    $qty_eligible = $params['quantity'] - $qty_not_eligible;
                    FPGCartHelper::updateQty($params['id_product'], $params['id_product_attribute'], $id_cart, $qty_eligible);
                }
				return 0.00;
			}
		}
	}

	public function hookDisplayShoppingCart($params)
	{
		return '';
		if ($params['products']) 
		{
			foreach ($params['products'] as &$product) 
			{
				$fpg_cart_info = FPGModel::getCartProductFPGInfo(Context::getContext()->cart->id, $product['id_product'], $product['id_product_attribute']);
				if ($fpg_cart_info['fpg_freegift'] == 1) 
				{
					$product['gift'] = 0;
					if (!empty($product['price_without_quantity_discount']))
						$product['price_wt'] = $product['price_without_quantity_discount'];
					elseif (!empty($product['price_without_specific_price']))
						$product['price_wt'] = $product['price_without_specific_price'];

					if ($product['total_wt'] == 0)
						$product['gift'] = 1;
				}
			}
		}
		Context::getContext()->smarty->assign($params);
	}

	public function hookRenderProductPageWidget()
	{
		$free_products = array();

		$this->sibling->context->controller->addJquery();
		$this->sibling->context->controller->addCSS($this->sibling->_path.'views/css/front.css');
		$this->sibling->context->controller->addJS($this->sibling->_path.'views/js/front.js');

		//if (FPGCartProductModel::getCartFreeGiftCount(Context::getContext()->cart->id) >= 1) return false; //@todo: add this is a feature

		$freeproducts = FPGModel::getProductGifts(Tools::getValue('id_product'), true, Context::getContext()->shop->id);

		if (isset($freeproducts->gift_ids))
		{
			foreach ($freeproducts->gift_ids as $key => $id_gift) 
			{
				$freeproduct = new Product($id_gift, true, $this->context->language->id, $this->context->shop->id);

				if ($freeproduct->active == false)
					continue;

				$freeproduct->link = Context::getContext()->link->getProductLink($freeproduct, null, null, null, Context::getContext()->language->id, Context::getContext()->shop->id);


				$cover_image = $freeproduct->getCover($id_gift);
				$freeproduct->id_image = $cover_image['id_image'];
				$freeproduct->image = Context::getContext()->link->getImageLink($freeproduct->link_rewrite, $freeproduct->id_image, ImageType::getFormattedName('home'));

				$freeproduct->id_product_attribute = Product::getDefaultAttribute($id_gift);

				if ($freeproduct->quantity == 0 && !Product::isAvailableWhenOutOfStock($freeproduct->out_of_stock))
					continue;

				$attributes_groups = $freeproduct->getAttributesGroups($this->context->language->id);

				$groups = array();
				foreach ($attributes_groups as $attribute_group) 
				{
					if ($attribute_group['quantity'] <= 0) {
						continue;
					}
					
					$ipa = $attribute_group['id_product_attribute'];
					$groups[$ipa]['id_product_attribute'] = $ipa;
					if (!isset($groups[$ipa]['label']))
						$groups[$ipa]['label'] = $attribute_group['group_name'].':'.$attribute_group['attribute_name'].', ';
					else
						$groups[$ipa]['label'] .= $attribute_group['group_name'].':'.$attribute_group['attribute_name'].', ';
				}
				$freeproduct->attribute_groups = $groups;
				$free_products[] = $freeproduct;
			}
		}

		$fpg_product_option_model = new FPGProductOptionModel();
		$fpg_product_option_model->loadByProduct(Tools::getValue('id_product'));

		$this->sibling->smarty->assign(array(
			'fpg_products' => $free_products,
			'fpg_product_options' => $fpg_product_option_model
		));
		return $this->sibling->display($this->sibling->module_file, 'views/templates/front/widget.tpl');
	}

	/**
	 * Depreciated
	 * @param $params
	 * @return bool
	 */
	public function hookRenderFPGIcon($params)
	{
		if ((int)Configuration::get('productlist_display_icon') == 0 || !Module::isEnabled('FreeProductGifts')) return false;

		$fpg_product = FPGModel::getProductGifts($params['product']['id_product'], true, Context::getContext()->shop->id);
		if (!empty($fpg_product) && count($fpg_product->gift_ids) > 0) 
		{
			$this->sibling->smarty->assign(array(
				'product' => $params['product'],
			));
			return $this->sibling->display($this->sibling->module_file, 'views/templates/front/productlist_icon.tpl');
		}
	}

	/**
	 * Add the free gift ribbon to the product listing
	 * @param $params
	 */	
	public function hookFilterProductSearch($params)
	{
		if ((int)Configuration::get('productlist_display_icon') == 0 || !Module::isEnabled('FreeProductGifts')) return false;
		foreach ($params['searchVariables']['products'] as &$product) {
			$fpg_product = FPGModel::getProductGifts($product['id_product'], true, Context::getContext()->shop->id);
			if (!empty($fpg_product) && count($fpg_product->gift_ids) > 0) {
				$product['flags'][] = array(
					'type' => 'new',
					'label' => $this->l('+gift')
				);
			}
		}
	}

	public function hookDisplayFooterProduct($params)
	{
		return $this->sibling->display($this->sibling->module_file, 'views/templates/front/hook_product_footer.tpl');
	}

	private function getFreeGiftQtyToAdd($id_gift, $id_product, $quantity_add)
	{
		$fpg_entry = FPGModel::getFPGEntry($id_gift, $id_product, Context::getContext()->shop->id);
		$qty_free = floor($quantity_add / $fpg_entry->min_qty) * $fpg_entry->qty;
		return $qty_free;
	}

	/**
	 * Add a free gift to the basket along when customer adds normal product to basket
	 * @param $params
	 * @return bool
	 */
	public function addFPGToCart($params)
	{
		$id_product_attribute_parent = (int)Product::getIdProductAttributesByIdAttributes(Tools::getValue('id_product'), Tools::getValue('group'));
		$id_gift = (int)Tools::getValue('id_gift');
		$id_gift_ipa = (int)Tools::getValue('id_gift_ipa');

		if ((int)Tools::getValue('id_gift') == 0 || Tools::getValue('id_product') == 0) return false;

		$cart = Context::getContext()->cart;

        // work around for Prestashop trigerring hookCartActionSave for new carts
		$sql = 'SELECT date_add FROM '._DB_PREFIX_.'cart_product
		        WHERE id_cart = '.(int)$cart->id.'
				AND id_product = '.(int)$id_gift.'
				AND id_product_attribute = '.(int)$id_gift_ipa;
        $fpg_last_add = strtotime(DB::getInstance()->getValue($sql));
        $diff = time() - $fpg_last_add;
        if ($diff <= 1)
            return false;
        // end

		// multiple gifts to be added
		if (strpos(Tools::getValue('id_gift'), ',') > 0) {
			$arr_id_gifts = explode(',', Tools::getValue('id_gift'));
			foreach ($arr_id_gifts as $gift)
			{
				$tmp_arr = explode(':', $gift);
				$id_gift = $tmp_arr[0];
				$id_gift_ipa = $tmp_arr[1];

				$qty_free = $this->getFreeGiftQtyToAdd($id_gift, Tools::getValue('id_product'), Tools::getValue('qty'));
				FPGModel::saveFPGProductToCart($cart->id, Tools::getValue('id_product'), $id_product_attribute_parent, $id_gift, $cart->id_address_delivery, Context::getContext()->shop->id, $id_gift_ipa, $qty_free);
				$fpg_cart_product = new FPGCartProductModel();
				$fpg_cart_product->id_cart = $cart->id;
				$fpg_cart_product->id_product = $id_gift;
				$fpg_cart_product->id_product_attribute = $id_gift_ipa;
				$fpg_cart_product->id_product_parent = Tools::getValue('id_product');
				$fpg_cart_product->id_product_attribute_parent = $id_product_attribute_parent;
				$fpg_cart_product->save();
			}
			return true;
		}

        $qty_free = $this->getFreeGiftQtyToAdd($id_gift, Tools::getValue('id_product'), Tools::getValue('qty'));

		$qty_freegifts_in_stock = Product::getQuantity($id_gift, $id_gift_ipa);
		
		if (Configuration::get('PS_STOCK_MANAGEMENT') == 1) {
			$qty_freegifts_in_stock = Product::getQuantity($id_gift, $id_gift_ipa);
			if ($qty_free > $qty_freegifts_in_stock) {
				$qty_free = $qty_freegifts_in_stock;
			}
		}

		if ($qty_free == 0) {
			FPGCartHelper::updateQty($id_gift, $id_gift_ipa, $cart->id, $qty_free);
		}

		if ($qty_free > 0) {
            FPGModel::saveFPGProductToCart($cart->id, Tools::getValue('id_product'), $id_product_attribute_parent, $id_gift, $cart->id_address_delivery, Context::getContext()->shop->id, $id_gift_ipa, $qty_free);
            $fpg_cart_product = new FPGCartProductModel();

            if (FPGCartProductModel::isUnique($cart->id, Tools::getValue('id_product'), Tools::getValue('fpg_id_product_attribute_parent'), Tools::getValue('id_gift'), Tools::getValue('ipa'))) {
                $fpg_cart_product->id_cart = $cart->id;
                $fpg_cart_product->id_product = $id_gift;
                $fpg_cart_product->id_product_attribute = $id_gift_ipa;
                $fpg_cart_product->id_product_parent = Tools::getValue('id_product');
                $fpg_cart_product->id_product_attribute_parent = $id_product_attribute_parent;
                $fpg_cart_product->save();
            }
        }
	}


	public function deleteFPGCartProduct($id_product, $id_product_attribute)
	{
		DB::getInstance()->delete('fpg_cart_product',
			'id_product_parent='.(int)$id_product.' 
			AND id_product_attribute_parent='.(int)$id_product_attribute);

		/*$fpg_cart_product = new FPGCartProductModel();
		$fpg_cart_product->id_cart = Context::getContext()->cart->id;
		$fpg_product_collection = $fpg_cart_product->getByParentProduct($params['id_product'], $params['id_product_attribute']);

		if (!empty($fpg_product_collection))
		{
			foreach ($fpg_product_collection as $fpg_cart_item) 
			{
				$fpg_cart_product_delete = new FPGCartProductModel($fpg_cart_item->id_fpg_cart_product);
				$fpg_cart_product_delete->delete();
				FPGCartProductExtraModel::deleteExtra($fpg_cart_item->id_product, $fpg_cart_item->id_product_attribute);
			}
		}*/
	}

	/*
	 * check each product has max amount of free gifts a product is eligible for in the cart
	 */
	public function updateCartFreeGiftQuantities()
	{
		$cart_products_raw = FPGModel::getCartProductsLight(Context::getContext()->cart->id, true);
		$id_cart = Context::getContext()->cart->id;
		$id_shop = Context::getContext()->shop->id;

		$total_free_qty_for_prod = 0;

		if (!empty($cart_products_raw))
		{
			foreach ($cart_products_raw as $cart_product)
			{
				$fpg_cart_product_info = FPGModel::getCartProductFPGInfo(Context::getContext()->cart->id, $cart_product['id_product'], $cart_product['id_product_attribute']);

				$fpg_cart_product_option_model = new FPGProductOptionModel();
				$fpg_cart_product_option_model->loadByProduct($cart_product['id_product']);

				if (empty($fpg_cart_product_option_model->id))
					$fpg_cart_product_option_model->total_qty_per_product = 1;

				// if it's not a free gift in the cart, get all child free gifts
				if (empty($fpg_cart_product_info['fpg_freegift']))
				{
					$fpg_cart_product_model = new FPGCartProductModel();
					$fpg_cart_product_model->id_cart = $id_cart;
					$result = $fpg_cart_product_model->getByParentProduct($cart_product['id_product'], $cart_product['id_product_attribute']);

					// loop through all child gifts and get total qty
					$free_gift_qty_totals = array();
					if (!empty($result))
					{
						$total_free_qty_for_prod = 0;
						foreach ($result as $free_gift)
						{
							$free_gift_cart_product = FPGCartProductModel::getPSCartProduct($id_cart, $free_gift->id_product, $free_gift->id_product_attribute, $id_shop);

							if (empty($free_gift_qty_totals[$free_gift->id_product]))
								$free_gift_qty_totals[$free_gift->id_product] = $free_gift_cart_product['quantity'];
							else
								$free_gift_qty_totals[$free_gift->id_product] += $free_gift_cart_product['quantity'];

							$total_free_qty_for_prod += $free_gift_cart_product['quantity'];
						}

						foreach ($free_gift_qty_totals as $id_gift => $qty_gift)
						{
							$fpg_product_gift = FPGModel::getProductGift($cart_product['id_product'], $id_gift, $id_shop);

							$qty_available = $cart_product['quantity'] * $fpg_product_gift['qty']; //total number of gifts this product is eligible for

							$qty_shortfall = $qty_available - $total_free_qty_for_prod; // how many extra free gifts is customer eligible for, for this product?

							if ($qty_shortfall > 0)
							{
								$qty_supplement = $qty_available - $total_free_qty_for_prod; // determine if customer has less free gifts for this products than he/she is eligible for
								$total_free_qty_for_prod = $total_free_qty_for_prod + $qty_supplement;
								//if they are, add supplement, qty to first free gift associated with this product
								$sql = 'UPDATE '._DB_PREFIX_.'cart_product SET quantity = quantity + '.(int)$qty_supplement.' 
										WHERE id_product = '.(int)$id_gift.'
										AND id_cart='.(int)$id_cart.'
										AND id_shop='.(int)$id_shop.'  
										LIMIT 1';
								DB::getInstance()->execute($sql);
							}

							// if parent product quantity is less than the eligible free gifts, reduce quantity of free gift
							if ((int)$fpg_cart_product_option_model->total_qty_per_product == 0)
								$fpg_cart_product_option_model->total_qty_per_product = 1;

							if ($fpg_cart_product_option_model->total_qty_per_product == 1 && $total_free_qty_for_prod > $cart_product['quantity'])
							{
								if (Configuration::get('additional_gifts_method') == 'limit')
								{
									$sql = 'UPDATE '._DB_PREFIX_.'cart_product SET quantity = '.(int)$cart_product['quantity'].' 
											WHERE id_product = '.(int)$id_gift.'
											AND id_cart='.(int)$id_cart.'
											AND id_shop='.(int)$id_shop.'  
											LIMIT 1';
									DB::getInstance()->execute($sql);
								}
							}
						}
					}

					// if multiple free gifts can be rewarded to a single product and free gift quantities exceed what customer is eligible for, then adjust parent product qty accordingly
					if ($fpg_cart_product_option_model->total_qty_per_product > 1 && ($total_free_qty_for_prod > $fpg_cart_product_option_model->total_qty_per_product))
					{
						$product_qty = ceil($total_free_qty_for_prod / $fpg_cart_product_option_model->total_qty_per_product);

						//if (Tools::getValue('ajax') != 'true' && Tools::getValue('add') == 'true')
						if (Tools::getValue('ajax') != 'true')
						{
							if ($cart_product['quantity'] < $product_qty)
								DB::getInstance()->update('cart_product', array(
									'quantity' => $product_qty
								), 'id_cart='.(int)$id_cart.' AND id_product='.(int)$cart_product['id_product'].' AND id_product_attribute='.(int)$cart_product['id_product_attribute']);
						}
					}

					// if not enough parent products in cart to account for all free gifts, up the quantity of the parent product tocompensate
					/*if ($fpg_cart_product_option_model->total_qty_per_product == 1 && ($total_free_qty_for_prod > $cart_product['quantity']))
						DB::getInstance()->update('cart_product', array(
							'quantity' => $total_free_qty_for_prod
						), 'id_cart='.(int)$id_cart.' AND id_product='.(int)$cart_product['id_product'].' AND id_product_attribute='.(int)$cart_product['id_product_attribute']);*/
				}
			}
		}
	}

	/**
	 * Set the correct quantity for a free gift in the cart, based on a eligible product
	 * @param $id_product
	 * @param $ipa
	 * @param $cart_op
	 */
	public function updateCartFreeGiftQuantity($id_product, $ipa)
	{
		if (empty(Context::getContext()->cart->id)) {
			return false;
		}
		
        FPGCartHelper::syncFPGCartTables(Context::getContext()->cart->id);

		$id_cart = Context::getContext()->cart->id;
		$id_shop = Context::getContext()->shop->id;

		$fpg_cart_product_model = new FPGCartProductModel();
		$fpg_cart_product_model->loadByParentProduct($id_product, $ipa, Context::getContext()->cart->id);

        // it is possible the quantity of the free gift product itself has been updated
		if (empty($fpg_cart_product_model->id_fpg_cart_product_extra)) {
			$fpg_cart_product_model->loadByFreeGiftProduct($id_product, $ipa, Context::getContext()->cart->id);
		}

		$qty_eligible = FPGCartHelper::getFreeGiftEligibileQty($fpg_cart_product_model->id_product, $fpg_cart_product_model->id_product_attribute);
		$cart_product = FPGCartProductModel::getPSCartProduct($id_cart, $fpg_cart_product_model->id_product, $fpg_cart_product_model->id_product_attribute, $id_shop);
        $cart_product_parent = FPGCartProductModel::getPSCartProduct($id_cart, $fpg_cart_product_model->id_product_parent, $fpg_cart_product_model->id_product_attribute_parent, $id_shop);

		$fpg_cart_product_option_model = new FPGProductOptionModel();
		$fpg_cart_product_option_model->loadByProduct($fpg_cart_product_model->id_product_parent);

        $qty_eligible_max = ($fpg_cart_product_option_model->total_qty_per_product) * $cart_product_parent['quantity'];

		if ($cart_product['quantity'] < $qty_eligible) {
			FPGCartHelper::updateQty($cart_product['id_product'], $cart_product['id_product_attribute'], $id_cart, $qty_eligible);
		}

		if ($cart_product['quantity'] > $qty_eligible) {
			if (Configuration::get('additional_gifts_method') == 'limit') {
				if ($qty_eligible > 0) {
					FPGCartHelper::updateQty($cart_product['id_product'], $cart_product['id_product_attribute'], $id_cart, $qty_eligible_max);
				}  else {
					FPGCartHelper::deleteAssociatedGifts($id_product, $ipa, $id_cart);
				}
			}
		}

		if (Tools::getValue('delete') == '1') {
			$front_controller = new FPGFrontController($this->sibling);
			$front_controller->deleteFPGCartProduct($id_product, $ipa);
		}
	}

	public function route()
	{
		switch (Tools::getValue('action'))
		{
			case 'renderwidget' :
				return $this->hookRenderProductPageWidget();
		}
	}
}