<?php
/**
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * You must not modify, adapt or create derivative works of this source code
 *
 * @author    Musaffar Patel
 * @copyright 2016-2017 Musaffar Patel
 * @license   LICENSE.txt
 */

class FPGAdminConfigGeneralController extends FPGControllerCore
{
	public $sibling;

	public function route()
	{
		$return = '';

		switch (Tools::getValue('do'))
		{
			case 'processgeneralform' :
				$this->processGeneralForm();
				$this->redirect('');
				break;

			default:
				$return .= $this->renderGeneralForm();
				return $return;
		}
	}

	public function renderGeneralForm()
	{
		$fields = array(
			'form' => array(
				'legend' => array(
					'title' => $this->l('General Options'),
					'icon' => 'icon-cogs'
				),
				'input' => array(
					array(
						'name' => 'productlist_display_icon',
						'type' => 'select',
						'label' => 'Display Free Gift Icon in product list?',
						'class' => 'fixed-width-xs',
						'options' => array(
							'query' => array(
								array(
									'id_option' => '1',
									'name' => 'Yes'
								),
								array(
									'id_option' => '0',
									'name' => 'No'
								)
							),
							'id' => 'id_option',
							'name' => 'name'
						),
					),
					array(
						'name' => 'additional_gifts_method',
						'type' => 'select',
						'label' => 'Additional Free Gifts Behaviour',
						'class' => 'fixed-width-xs',
						'options' => array(
							'query' => array(
								array(
									'id_option' => 'limit',
									'name' => 'Limit Free Gift Qty'
								),
								array(
									'id_option' => 'charge',
									'name' => 'Charge for extra free gift quantities'
								)
							),
							'id' => 'id_option',
							'name' => 'name'
						)
					),
				),
				'submit' => array(
					'title' => $this->l('Save'),
					'class' => 'btn btn-default pull-right',
					'name' => 'submit_config_general',
				)
			),
		);

		$helper = new HelperForm();
		$lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
		$helper->default_form_language = $lang->id;
		$helper->module = $this->sibling;
		$helper->token = Tools::getAdminTokenLite('AdminModules');
		$helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->sibling->name.'&do=processgeneralform';

		$helper->tpl_vars['fields_value']['productlist_display_icon'] = (int)Configuration::get('productlist_display_icon', null, 0, 0);
		$helper->tpl_vars['fields_value']['additional_gifts_method'] = Configuration::get('additional_gifts_method', null, 0, 0);
		return $helper->generateForm(array($fields));
	}

	public function processGeneralForm()
	{
		Configuration::updateValue('productlist_display_icon', (int)Tools::getValue('productlist_display_icon'), false, 0, 0);
		Configuration::updateValue('additional_gifts_method', Tools::getValue('additional_gifts_method'), false, 0, 0);
	}

}