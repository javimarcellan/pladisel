<?php
/**
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * You must not modify, adapt or create derivative works of this source code
 *
 * @author    Musaffar Patel
 * @copyright 2016-2017 Musaffar Patel
 * @license   LICENSE.txt
 */

class FPGAdminProductTabController extends FPGControllerCore
{

	public function __construct($sibling, $params = array())
	{
		parent::__construct($sibling, $params);
		$this->sibling = $sibling;
		$this->set_module_base_admin_url();
		$this->base_url = Tools::getShopProtocol().Tools::getShopDomain().__PS_BASE_URI__;
	}

	public function setMedia()
	{
		if (Tools::getValue('controller') == 'AdminProducts')
		{
			Context::getContext()->controller->addCSS($this->sibling->_path.'views/css/lib/tools.css');
			Context::getContext()->controller->addCSS($this->sibling->_path.'views/css/admin/admin.css');

			Context::getContext()->controller->addJS($this->sibling->_path.'views/js/lib/Tools.js');
			Context::getContext()->controller->addJS($this->sibling->_path.'views/js/lib/MpProductSearchWidget.js');
			Context::getContext()->controller->addJS($this->sibling->_path.'views/js/lib/Breadcrumb.js');
			Context::getContext()->controller->addJS($this->sibling->_path.'views/js/admin/producttab/FPGAdminProductTabController.js');
			Context::getContext()->controller->addJS($this->sibling->_path.'views/js/admin/producttab/FPGAdminProductTabEditGiftController.js');
		}
	}

	protected function set_module_base_admin_url()
	{
		if (!defined('_PS_ADMIN_DIR_'))
		{
			$this->admin_mode = false;
			return false;
		}
		else
		{
			$this->admin_mode = true;
			$arr_temp = explode('/', _PS_ADMIN_DIR_);
			if (count($arr_temp) <= 1) $arr_temp = explode('\\', _PS_ADMIN_DIR_);
			$dir_name = end($arr_temp);
			$this->module_base_url = $dir_name.'/index.php?controller=AdminProducts&id_product='.Tools::getValue('id_product').'&updateproduct&token='.Tools::getAdminTokenLite('AdminProducts');
		}
	}

	/**
	 * Render list of free gifts already assigned to this product
	 */
	public function renderMain()
	{
		Context::getContext()->smarty->assign(array(
			'module_ajax_url' => $this->module_ajax_url,
			'id_product' => $this->params['id_product']
		));
		return $this->sibling->display($this->sibling->module_file, 'views/templates/admin/producttab/main.tpl');
	}

	public function renderList()
	{
		$fpg_product_option_model = new FPGProductOptionModel();
		$fpg_product_option_model->loadByProduct(Tools::getValue('id_product'));

		$free_products = FPGModel::getProductGifts(Tools::getValue('id_product'), null, Context::getContext()->shop->id, true);

		if (!empty($free_products))
			foreach ($free_products as &$gift)
				$gift['name'] = FPGModel::getProductName($gift['id_gift'], Context::getContext()->language->id, Context::getContext()->shop->id);

		Context::getContext()->smarty->assign(array(
			'free_products' => $free_products,
			'fpg_product_option_model' => $fpg_product_option_model->total_qty_per_product
		));
		return $this->sibling->display($this->sibling->module_file, 'views/templates/admin/producttab/list.tpl');
	}


	/**
	 * Delete a free gift associated with a product
	 */
	public function processDelete()
	{
		FPGModel::deleteFPGEntry(Tools::getValue('id_product'), Tools::getValue('id_gift'), Context::getContext()->shop->id);
	}

	/**
	 * Save options
	 */
	public function processForm()
	{
		$fpg_product_optiion_model = new FPGProductOptionModel();
		$fpg_product_optiion_model->loadByProduct(Tools::getValue('id_product'));

		$fpg_product_optiion_model->id_product = (int)Tools::getValue('id_product');
		$fpg_product_optiion_model->total_qty_per_product = (int)Tools::getValue('total_qty_per_product');

		$fpg_product_optiion_model->save();
	}

	public function route()
	{
		$return = '';

		switch (Tools::getValue('route'))
		{
			case 'fpgadminproducttabeditgiftcontroller' :
				die((new FPGAdminProductTabEditGiftController($this->sibling, $this->params))->route());

			case 'fpgadminproducttabcontroller' :

				switch (Tools::getValue('action'))
				{
					case 'processdelete' :
						die($this->processDelete());

					case 'processform' :
						die($this->processForm());

					default:
						die($this->renderList());
				}
			default:
				return $this->renderMain();
		}

	}

}