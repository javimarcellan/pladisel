<?php
/**
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * You must not modify, adapt or create derivative works of this source code
 *
 * @author    Musaffar Patel
 * @copyright 2016-2017 Musaffar Patel
 * @license   LICENSE.txt
 */

class FPGAdminProductTabEditGiftController extends FPGControllerCore
{

	public function renderForm()
	{
		if ((int)Tools::getValue('id_gift') > 0)
		{
			$fpg_entry = FPGModel::getFPGEntry(Tools::getValue('id_gift'), Tools::getValue('id_product'), Context::getContext()->shop->id);
			$product_name = FPGModel::getProductName($fpg_entry->id_gift, Context::getContext()->language->id, Context::getContext()->shop->id);
			Context::getContext()->smarty->assign(array(
				'fpg_entry' => $fpg_entry,
				'product_name' => $product_name
			));
		}
		return $this->sibling->display($this->sibling->module_file, 'views/templates/admin/producttab/edit.tpl');
	}

	public function processForm()
	{
		$fpg_entry = FPGModel::getFPGEntry(Tools::getValue('id_gift'), Tools::getValue('id_product'), Context::getContext()->shop->id);

		if (!$fpg_entry)
			$fpg_entry = new TFPGProductGiftEntry();

		$fpg_entry->id_product = Tools::getValue('id_parent_product');
		$fpg_entry->qty = Tools::getValue('qty');
		$fpg_entry->min_qty = Tools::getValue('min_qty');
		$fpg_entry->position = Tools::getValue('position');
		$fpg_entry->id_gift = Tools::getValue('id_product');
		$fpg_entry->enabled = Tools::getValue('enabled');
		$fpg_entry->id_shop = Context::getContext()->shop->id;
		FPGModel::saveFPGEntry($fpg_entry);
	}

	public function route()
	{
		switch (Tools::getValue('action'))
		{
			case 'renderform' :
				die($this->renderForm());

			case 'processform' :
				die($this->processForm());
		}
	}

}