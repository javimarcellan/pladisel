<?php
/**
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * You must not modify, adapt or create derivative works of this source code
 *
 * @author    Musaffar Patel
 * @copyright 2016-2017 Musaffar Patel
 * @license   LICENSE.txt
 */

class DDWTimeslot extends ObjectModel
{
	public $id_carrier;
	public $id_weekday;
	public $id_ddw_specificdate;
	public $id_shop;
	public $time_start;
	public $time_end;
	public $order_limit = 0;
	public $position = 0;
	public $enabled = 1;

	public static $definition = array(
		'table' => 'ddw_timeslots',
		'primary' => 'id_timeslot',
		'multilang' => false,
		'fields' => array(
			'id_carrier' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt', 'required' => true),
			'id_shop' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt', 'required' => true),
			'id_weekday' => array('type' => self::TYPE_INT, 'validate' => 'isInt', 'required' => true),
			'id_ddw_specificdate' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt', 'required' => true),
			'time_start' => array('type' => self::TYPE_STRING, 'required' => false),
			'time_end' => array('type' => self::TYPE_STRING, 'required' => false),
			'order_limit' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt', 'required' => false),
			'enabled' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt', 'required' => false),
			'position' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt', 'required' => false)
		)
	);

    /**
     * Load timeslots based opn specific date ID
     * @param $id_ddw_specificdate
     * @param $id_carrier
     * @param $id_shop
     * @return array
     * @throws PrestaShopDatabaseException
     * @throws PrestaShopException
     */
    public function getTimeSlotsForSpecificDate($id_ddw_specificdate, $id_carrier, $id_shop)
    {
		$sql = new DbQuery();
		$sql->select('*');
		$sql->from('ddw_timeslots');
		$sql->where('id_carrier = '.(int)$id_carrier);
		$sql->where('id_ddw_specificdate = '.(int)$id_ddw_specificdate);
		$sql->where('id_shop = '.(int)$id_shop);
		$sql->orderBy('position');

		$collection = Db::getInstance()->executeS($sql);
		if ($collection)
			return $this->hydrateCollection('DDWTimeslot', $collection);
		else
			return array();
    }

	public function getTimeSlotsForWeekday($id_carrier, $id_weekday, $id_shop)
	{
		$sql = new DbQuery();
		$sql->select('*');
		$sql->from('ddw_timeslots');
		$sql->where('id_carrier = '.(int)$id_carrier);
		$sql->where('id_weekday = '.(int)$id_weekday);
		$sql->where('id_shop = '.(int)$id_shop);
		$sql->orderBy('position');

		$collection = Db::getInstance()->executeS($sql);
		if ($collection)
			return $this->hydrateCollection('DDWTimeslot', $collection);
		else
			return array();
	}


    /**
     * Delete all by specific date ID
     * @param $id_ddw_specificdate
     */
    public static function deleteAllBySpecificDate($id_ddw_specificdate)
    {
        DB::getInstance()->delete(_DB_PREFIX_.self::$definition['table'], 'id_ddw_specificdate = '.(int)$id_ddw_specificdate);
    }

	public static function deleteAllByCarrier($id_carrier, $id_shop, $id_weekday = '', $id_ddw_specific_date = 0)
	{
        $sql = '';
		if ($id_weekday != '') {
            $sql = ' AND id_weekday=' . (int)$id_weekday;
        }

        if ((int)$id_ddw_specific_date > 0) {
            $sql .= ' AND id_ddw_specificdate='.(int)$id_ddw_specific_date;
        }

		DB::getInstance()->delete(
			'ddw_timeslots',
			'id_carrier='.(int)$id_carrier.' AND id_shop='.(int)$id_shop.$sql
		);
	}

	public static function formatHumanTime($string)
	{
		// $input is valid HH:MM format.
		if (preg_match('/^(?:[01][0-9]|2[0-3]):[0-5][0-9]$/', $string))
			return $string;
		else return '00:00';
	}
}