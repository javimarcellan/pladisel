<?php
/**
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * You must not modify, adapt or create derivative works of this source code
 *
 * @author    Musaffar Patel
 * @copyright 2016-2017 Musaffar Patel
 * @license   LICENSE.txt
 */

class DDWWeekday extends ObjectModel
{
	public $id_carrier;
	public $id_weekday;
	public $id_shop;
	public $enabled;
	public $minmax_enabled = 0;
	public $min_days = 0;
	public $max_days = 0;
	public $cutofftime_enabled = 0;
	public $cutofftime_hours = 0;
	public $cutofftime_minutes = 0;
	public $min_days_postcutoff = 0;

	public static $definition = array(
		'table' => 'ddw_weekdays',
		'primary' => 'id_ddw_weekday',
		'multilang' => false,
		'fields' => array(
			'id_carrier' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt', 'required' => true),
			'id_shop' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt', 'required' => true),
			'id_weekday' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt', 'required' => true),
			'enabled' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt', 'required' => true),
			'minmax_enabled' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt', 'required' => true),
			'min_days' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt', 'required' => false),
			'max_days' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt', 'required' => false),
			'cutofftime_enabled' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
			'cutofftime_hours' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
			'cutofftime_minutes' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
			'min_days_postcutoff' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt')
		)
	);

	public function loadWeekday($id_carrier, $id_weekday, $id_shop)
	{
		$sql = 'SELECT
		 			id_ddw_weekday,
		 			id_carrier,
		 			id_weekday,
		 			id_shop,
		 			min_days,
		 			max_days,
		 			enabled,
		 			minmax_enabled,
		 			cutofftime_enabled,
		 			cutofftime_hours,
		 			cutofftime_minutes,
		 			min_days_postcutoff
		 		FROM '._DB_PREFIX_.self::$definition['table'].'
		 		WHERE id_carrier='.(int)$id_carrier.'
		 		AND id_shop = '.(int)$id_shop.'
		 		AND id_weekday = '.(int)$id_weekday;
		$row = DB::getInstance()->getRow($sql);
		if (!$row) return false;
		else
			$this->hydrate($row);
	}

	public function getAvailableWeekdaysCollection($id_carrier, $id_shop)
	{
		$sql = new DbQuery();
		$sql->select('*');
		$sql->from('ddw_weekdays');
		$sql->where('enabled = 1');
		$sql->where('id_carrier = '.(int)$id_carrier);
		$sql->where('id_shop = '.(int)$id_shop);
		$result = DB::getInstance()->executeS($sql);
		if (!$result) return array();
		else
			return $this->hydrateCollection('DDWWeekday', $result);
	}

}