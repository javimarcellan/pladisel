<?php
/**
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * You must not modify, adapt or create derivative works of this source code
 *
 * @author    Musaffar Patel
 * @copyright 2016-2017 Musaffar Patel
 * @license   LICENSE.txt
 */

class DDWBlockedDate extends ObjectModel
{
	public $id_carrier;
	public $id_shop;
	public $start_date;
	public $end_date;
	public $recurring = 0;

	public static $definition = array(
		'table' => 'ddw_blocked_dates',
		'primary' => 'id_blockeddate',
		'multilang' => false,
		'fields' => array(
			'id_carrier' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt', 'required' => true),
			'id_shop' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt', 'required' => true),
			'start_date' => array('type' => self::TYPE_STRING, 'required' => false),
			'end_date' => array('type' => self::TYPE_STRING, 'required' => false),
			'recurring' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt', 'required' => false)
		)
	);

	public function getCollection($id_carrier, $id_shop)
	{
		$sql = new DbQuery();
		$sql->select('*');
		$sql->from('ddw_blocked_dates');
		$sql->where('id_carrier = '.(int)$id_carrier);
		$sql->where('id_shop = '.(int)$id_shop);
		$result = Db::getInstance()->executeS($sql);
		return $this->hydrateCollection('DDWBlockedDate', $result);
	}
}