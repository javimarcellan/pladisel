<?php
/**
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * You must not modify, adapt or create derivative works of this source code
 *
 * @author    Musaffar Patel
 * @copyright 2016-2017 Musaffar Patel
 * @license   LICENSE.txt
 */

class DDWSpecificDateModel extends ObjectModel
{
    public $id_ddw_specificdate;
	public $id_carrier;
	public $id_shop;
	public $enabled;
	public $start_date;
	public $end_date;
	public $minmax_enabled;
	public $min_days;
	public $max_days;
	public $cutofftime_enabled;
	public $cutofftime_hours;
	public $cutofftime_minutes;
	public $min_days_postcutoff;

	public static $definition = array(
		'table' => 'ddw_specificdate',
		'primary' => 'id_ddw_specificdate',
		'multilang' => false,
		'fields' => array(
			'id_carrier' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt', 'required' => true),
			'id_shop' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt', 'required' => true),
			'enabled' => array('type' => self::TYPE_BOOL, 'required' => false),
			'start_date' => array('type' => self::TYPE_STRING, 'required' => false),
			'end_date' => array('type' => self::TYPE_STRING, 'validate' => 'isUnsignedInt', 'required' => false),
			'minmax_enabled' => array('type' => self::TYPE_BOOL, 'validate' => 'isUnsignedInt', 'required' => false),
			'min_days' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt', 'required' => false),
            'max_days' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt', 'required' => false),
            'cutofftime_enabled' => array('type' => self::TYPE_BOOL, 'validate' => 'isUnsignedInt', 'required' => false),
            'cutofftime_hours' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt', 'required' => false),
            'cutofftime_minutes' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt', 'required' => false),
            'min_days_postcutoff' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt', 'required' => false)
		)
	);

	/**
	 * @param $start_date (YYY-MM-DD)
	 */
	public function loadByDate($start_date, $id_carrier, $id_shop)
	{
		$sql = new DbQuery();
		$sql->select('*');
		$sql->from(self::$definition['table']);
		$sql->where('start_date = "'.$start_date.'"');
		$sql->where('id_carrier = ' . (int)$id_carrier);
		$sql->where('id_shop = ' . (int)$id_shop);
		$row = Db::getInstance()->getRow($sql);

		if (!empty($row)) {
			$this->hydrate($row);
		} else {
			return false;
		}
	}

	public function getAll($id_carrier, $id_shop)
	{
		$sql = new DbQuery();
		$sql->select('*');
		$sql->from(self::$definition['table']);
		$sql->where('id_carrier = ' . (int)$id_carrier);
		$sql->where('id_shop = ' . (int)$id_shop);
		$result = Db::getInstance()->executeS($sql);
		return $this->hydrateCollection('DDWSpecificDateModel', $result);
	}

}