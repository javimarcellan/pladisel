<?php
/**
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * You must not modify, adapt or create derivative works of this source code
 *
 * @author    Musaffar Patel
 * @copyright 2016-2017 Musaffar Patel
 * @license   LICENSE.txt
 */

if (!defined('_PS_VERSION_'))
	exit;

class DDW extends ObjectModel
{
	/** @var integer Unique ID */
	public $id_ddw;

	/** @var integer Carrier ID */
	public $id_carrier;

	/** @var boolean required */
	public $required;

	/** @var boolean enabled */
	public $enabled;

	/** @var integer min_days */
	public $min_days;

	/** @var integer max_days */
	public $max_days;

	/** @var integer cutofftime_enabled */
	public $cutofftime_enabled;

	/** @var integer cutofftime_hours */
	public $cutofftime_hours;

	/** @var integer cutofftime_minutes */
	public $cutofftime_minutes;

	/** @var integer timeslots_prep_minutes */
	public $timeslots_prep_minutes;

	/** @var integer timeslots_prep_minutes */
	public $product_eta_display_enabled;

	/**
	 * @see ObjectModel::$definition
	 */

	public static $definition = array(
		'table' => 'ddw',
		'primary' => 'id_ddw',
		'fields' => array(
			'id_carrier' => array(
				'type' => self::TYPE_INT,
				'required' => true
			),
			'required' => array(
				'type' => self::TYPE_INT,
				'required' => true
			),
			'enabled' => array(
				'type' => self::TYPE_INT,
				'required' => true
			),
			'min_days' => array(
				'type' => self::TYPE_INT,
				'required' => true
			),
			'max_days' => array(
				'type' => self::TYPE_INT,
				'required' => true
			),
			'cutofftime_enabled' => array(
				'type' => self::TYPE_INT,
				'required' => true
			),
			'cutofftime_hours' => array(
				'type' => self::TYPE_INT
			),
			'cutofftime_minutes' => array('type' => self::TYPE_INT),
			'timeslots_prep_minutes' => array('type' => self::TYPE_INT),
			'product_eta_display_enabled' => array('type' => self::TYPE_INT)
		)
	);

	public function loadByCarrier($id_carrier)
	{
		$sql = new DbQuery();
		$sql->select('*');
		$sql->from('ddw');
		$sql->where('id_carrier = '.(int)$id_carrier);

		$row = Db::getInstance()->getRow($sql);
		if (is_array($row))
			$this->hydrate($row);		
	}

	public static function getOrderCountInTimeWindow($ddw_order_date, $ddw_order_time, $id_shop)
	{
		$sql = 'SELECT COUNT(id_order) AS total_count
				FROM '._DB_PREFIX_.'orders
				WHERE ddw_order_date = "'.pSQL($ddw_order_date).'"
				AND ddw_order_time = "'.pSQL($ddw_order_time).'"
				AND current_state <> '.Configuration::get('PS_OS_CANCELED');
		$row = DB::getInstance()->getRow($sql);
		return $row['total_count'];
	}

	public static function saveToCart($ddw_order_date, $ddw_order_time, $id_cart)
	{
		Db::getInstance()->update('cart', array(
				'ddw_order_date' => pSQL($ddw_order_date),
				'ddw_order_time' => (int)$ddw_order_time
		),
				'id_cart='.(int)$id_cart
		);
	}

	public static function getCartDDWDateTime($id_cart)
	{
		$cart_ddw = array();
		$cart_ddw['ddw_order_date'] = '';
		$cart_ddw['ddw_order_time'] = '';
		/* get the order date and time we stored in cart session */
		$sql = 'SELECT
		            ddw_order_date,
		            ddw_order_time
				FROM '._DB_PREFIX_.'cart
				WHERE id_cart = '.(int)$id_cart;
		$result = Db::getInstance()->getRow($sql);
		if (isset($result) && is_array($result))
		{
			$cart_ddw['ddw_order_date'] = $result['ddw_order_date'];
			$cart_ddw['ddw_order_time'] = $result['ddw_order_time'];
			if (!empty($cart_ddw['ddw_order_time']))
			{
				$ddw_timeslot = new DDWTimeslot($cart_ddw['ddw_order_time']);
				if (!empty($ddw_timeslot->id))
					$cart_ddw['ddw_order_time'] = $ddw_timeslot->time_start.' - '.$ddw_timeslot->time_end;
			}
		}
		return $cart_ddw;
	}

	public static function getOrderDDWDateTime($id_order)
	{
		$order_ddw = array();
		$order_ddw['ddw_order_date'] = '';
		$order_ddw['ddw_order_time'] = '';
		/* get the order date and time we stored in cart session */
		$sql = 'SELECT
		            ddw_order_date,
		            ddw_order_time
				FROM '._DB_PREFIX_.'orders
				WHERE id_order = '.(int)$id_order;
		$result = Db::getInstance()->getRow($sql);
		if (isset($result) && is_array($result))
		{
			$order_ddw['ddw_order_date'] = $result['ddw_order_date'];
			$order_ddw['ddw_order_time'] = $result['ddw_order_time'];
			/*if (!empty($order_ddw['ddw_order_time']))
			{
				$ddw_timeslot = new DDWTimeslot($order_ddw['ddw_order_time']);
				if (!empty($ddw_timeslot->id))
					$order_ddw['ddw_order_time'] = $ddw_timeslot->time_start.' - '.$ddw_timeslot->time_end;
			}*/
		}
		return $order_ddw;
	}


	public static function saveToOrder($cart)
	{
		$ddw_order_date = '';
		$ddw_order_time = '';
		/* get the order date and time we stored in cart session */
		$sql = 'SELECT
		            ddw_order_date,
		            ddw_order_time
				FROM '._DB_PREFIX_.'cart
				WHERE secure_key = "'.$cart->secure_key.'"
				AND id_cart = '.(int)$cart->id;
		$result = Db::getInstance()->getRow($sql);

		if (isset($result) && is_array($result))
		{
			$ddw_order_date = $result['ddw_order_date'];
			$ddw_order_time = $result['ddw_order_time'];

			if (!empty($ddw_order_time))
			{
				$ddw_timeslot = new DDWTimeslot($ddw_order_time);
				if (!empty($ddw_timeslot->id))
					$ddw_order_time = $ddw_timeslot->time_start.' - '.$ddw_timeslot->time_end;
			}
		}
		else
			return false;

		$sql = 'SELECT * FROM '._DB_PREFIX_.'orders
				WHERE secure_key = "'.$cart->secure_key.'"
				AND id_cart = '.(int)$cart->id;

		Db::getInstance()->execute($sql);
		$result = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);
		if (count($result) > 0)
		{
			$id_order = $result[0]['id_order'];
			Db::getInstance()->update('orders', array(
					'ddw_order_date' => pSQL($ddw_order_date),
					'ddw_order_time' => pSQL($ddw_order_time)
			),
					'id_order='.(int)$id_order.' AND id_cart='.(int)$cart->id
			);
		}
	}

	public static function saveToOrderDirect($id_order, $ddw_order_date, $ddw_order_time)
	{
		DB::getInstance()->update(
				'orders',
				array(
						'ddw_order_date' => pSQL($ddw_order_date),
						'ddw_order_time' => pSQL($ddw_order_time)
				),
				'id_order='.(int)$id_order
		);
		return true;
	}

	public static function updateCarrierID($old_id_carrier, $new_id_carrier)
	{
		DB::getInstance()->update('ddw', array('id_carrier' => (int)$new_id_carrier), 'id_carrier='.(int)$old_id_carrier);
		DB::getInstance()->update('ddw_blocked_dates', array('id_carrier' => (int)$new_id_carrier), 'id_carrier='.(int)$old_id_carrier);
		DB::getInstance()->update('ddw_timeslots', array('id_carrier' => (int)$new_id_carrier), 'id_carrier='.(int)$old_id_carrier);
		DB::getInstance()->update('ddw_weekdays', array('id_carrier' => (int)$new_id_carrier), 'id_carrier='.(int)$old_id_carrier);
	}

}
