<?php
/**
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * You must not modify, adapt or create derivative works of this source code
 *
 * @author    Musaffar Patel
 * @copyright 2016-2017 Musaffar Patel
 * @license   LICENSE.txt
 */

class DDWInstall
{
	public static function installDB()
	{
		$return = true;
		$return &= Db::getInstance()->execute('
			CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'ddw` (
			  `id_ddw` int(10) unsigned NOT NULL AUTO_INCREMENT,
			  `id_carrier` int(10) unsigned NOT NULL,
			  `required` char(1) NOT NULL,
			  `enabled` char(1) DEFAULT NULL,
			  `min_days` mediumint(8) unsigned DEFAULT NULL,
			  `max_days` mediumint(8) unsigned DEFAULT NULL,
			  `cutofftime_enabled` char(1) DEFAULT NULL,
			  `cutofftime_hours` smallint(5) unsigned DEFAULT NULL,
			  `cutofftime_minutes` smallint(5) unsigned DEFAULT NULL,
			  `timeslots_prep_minutes` mediumint(8) unsigned NOT NULL DEFAULT "0",
			  `product_eta_display_enabled` tinyint(3) unsigned NOT NULL DEFAULT "0",
			  PRIMARY KEY (`id_ddw`)
			) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8 ;');

		$return &= Db::getInstance()->execute('
			CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'ddw_blocked_dates` (
			`id_blockeddate` int(10) unsigned NOT NULL AUTO_INCREMENT,
            `id_carrier` int(10) unsigned DEFAULT NULL,
            `id_shop` int(10) unsigned DEFAULT NULL,
            `recurring` smallint(5) unsigned DEFAULT NULL,
            `start_date` date DEFAULT NULL,
            `end_date` date DEFAULT NULL,
            PRIMARY KEY (`id_blockeddate`)
			) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8;');

		$return &= Db::getInstance()->execute('
			CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'ddw_timeslots` (
				`id_timeslot` int(10) unsigned NOT NULL AUTO_INCREMENT,
				`id_weekday` int(10) NOT NULL DEFAULT "0",
				`id_carrier` int(10) unsigned DEFAULT NULL,
				`id_shop` int(10) unsigned DEFAULT NULL,
				`id_ddw_specificdate` int(10) unsigned DEFAULT NULL,				
				`time_start` varchar(12) NOT NULL,
				`time_end` varchar(12) NOT NULL,
				`order_limit` int(10) unsigned NOT NULL DEFAULT "0",
				`enabled` tinyint(3) unsigned DEFAULT "1",
				`position` int(10) unsigned DEFAULT NULL,
				PRIMARY KEY (`id_timeslot`)
			) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8;');

		$return &= Db::getInstance()->execute('
			CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'ddw_timeslots_lang` (
			  `id_timeslot` int(10) unsigned NOT NULL DEFAULT "0",
			  `id_lang` int(10) unsigned NOT NULL DEFAULT "0",
			  `time_slot` varchar(128) DEFAULT NULL,
			  PRIMARY KEY (`id_timeslot`,`id_lang`)
			) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8;');

		$return &= Db::getInstance()->execute('
			CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'ddw_translations` (
			  `id_translation` int(10) unsigned NOT NULL AUTO_INCREMENT,
			  `id_shop` int(10) unsigned DEFAULT NULL,
			  `name` varchar(128) DEFAULT NULL,
			  `type` varchar(32) NOT NULL,
			  PRIMARY KEY (`id_translation`)
			) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8;');

		$return &= Db::getInstance()->execute('
			CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'ddw_translations_lang` (
			  `id_translation` int(10) unsigned DEFAULT NULL,
			  `id_lang` int(10) unsigned DEFAULT NULL,
			  `text` text
			) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8;');

		$return &= Db::getInstance()->execute('
			CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'ddw_weekdays` (
				`id_ddw_weekday` int(10) unsigned NOT NULL AUTO_INCREMENT,
				`id_carrier` int(10) unsigned NOT NULL DEFAULT "0",
				`id_weekday` smallint(5) unsigned NOT NULL,
				`id_shop` int(10) NOT NULL,
				`enabled` tinyint(3) unsigned NOT NULL DEFAULT "0",
				`minmax_enabled` tinyint(3) unsigned NOT NULL DEFAULT "0",
				`min_days` int(10) unsigned NOT NULL DEFAULT "0",
				`max_days` int(10) unsigned NOT NULL DEFAULT "0",
				`cutofftime_enabled` tinyint(3) unsigned NOT NULL DEFAULT "0",
				`cutofftime_hours` smallint(5) unsigned NOT NULL DEFAULT "0",
				`cutofftime_minutes` smallint(5) unsigned NOT NULL DEFAULT "0",
				`min_days_postcutoff` mediumint(8) unsigned NOT NULL DEFAULT "0",
				PRIMARY KEY (`id_ddw_weekday`)
			) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8;');

		$return &= Db::getInstance()->execute('
			CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'ddw_scopes` (
				`id_scope` int(11) unsigned NOT NULL AUTO_INCREMENT,
				`id_carrier` mediumint(8) unsigned NOT NULL,
				`id_associated` int(10) unsigned NOT NULL,
				`scope` varchar(255) NOT NULL,
				`cutofftime_enabled` tinyint(3) unsigned NOT NULL DEFAULT "0",
				`cutofftime_hours` tinyint(3) unsigned NOT NULL DEFAULT "0",
				`cutofftime_minutes` tinyint(4) NOT NULL DEFAULT "0",
				`min_days` mediumint(8) unsigned NOT NULL DEFAULT "0",
				`max_days` mediumint(8) unsigned NOT NULL DEFAULT "0",
				PRIMARY KEY (`id_scope`)
			) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8;');

        $return &= self::createTableSpecificDates();

		/*$return &= Db::getInstance()->execute('
				ALTER TABLE  `'._DB_PREFIX_.'cart` ADD  `ddw_order_date` DATETIME NOT NULL
			');*/
		self::addColumn('cart', 'ddw_order_date', "DATETIME NULL DEFAULT '1970-01-01 00:00:00'");
		self::addColumn('cart', 'ddw_order_time', 'VARCHAR(64)');
		self::addColumn('orders', 'ddw_order_date', "DATETIME NULL DEFAULT '1970-01-01 00:00:00'");
		self::addColumn('orders', 'ddw_order_time', 'VARCHAR(64)');
		return $return;
	}

    public static function createTableSpecificDates()
    {
		return Db::getInstance()->execute('
			CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'ddw_specificdate` (
				`id_ddw_specificdate` int(10) unsigned NOT NULL AUTO_INCREMENT,
				`id_carrier` int(10) unsigned NOT NULL DEFAULT "0",
				`id_shop` int(10) NOT NULL,
				`enabled` tinyint(3) unsigned NOT NULL DEFAULT "0",
				`start_date` date DEFAULT NULL,
				`end_date` date DEFAULT NULL,
				`minmax_enabled` tinyint(3) unsigned NOT NULL DEFAULT "0",
				`min_days` int(10) unsigned NOT NULL DEFAULT "0",
				`max_days` int(10) unsigned NOT NULL DEFAULT "0",
				`cutofftime_enabled` tinyint(3) unsigned NOT NULL DEFAULT "0",
				`cutofftime_hours` smallint(5) unsigned NOT NULL DEFAULT "0",
				`cutofftime_minutes` smallint(5) unsigned NOT NULL DEFAULT "0",
				`min_days_postcutoff` mediumint(8) unsigned NOT NULL DEFAULT "0",
				PRIMARY KEY (`id_ddw_specificdate`)			
			) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8;');

    }

	public static function addColumn($table, $name, $type)
	{
		try
		{
			$return = Db::getInstance()->execute('ALTER TABLE  `'._DB_PREFIX_.''.pSQL($table).'` ADD `'.pSQL($name).'` '.$type);
		} catch(Exception $e)
		{
			return true;
		}
		return true;
	}

	private static function dropColumn($table, $name)
	{
		Db::getInstance()->execute('ALTER TABLE  `'._DB_PREFIX_.''.pSQL($table).'` DROP `'.pSQL($name).'`');
	}

	protected static function dropTable($table_name)
	{
		$sql = 'DROP TABLE IF EXISTS `'._DB_PREFIX_.$table_name.'`';
		DB::getInstance()->execute($sql);
	}

	private static function createTranslation($name, $type, $text)
	{
		$languages = Context::getContext()->language->getLanguages();

		$ddw_translation = new TDDWTranslation();
		$ddw_translation->name = $name;
		$ddw_translation->type = $type;
		foreach ($languages as $lang)
			$ddw_translation->text_collection[$lang['id_lang']] = $text;
		DDWTranslationsModel::createTranslation($ddw_translation, Context::getContext()->shop->id);
	}

	private static function installTranslations()
	{
		self::createTranslation('date_invoice_label', 'text', 'Delivery Date');
		self::createTranslation('time_invoice_label', 'text', 'Delivery Time');
		self::createTranslation('text_checkout', 'html', '<strong>Select delivery date and time below</strong>');
		self::createTranslation('required_error', 'text', 'You must select a delivery date to continue');
	}

	public static function installData()
	{
		self::installTranslations();
	}

	public static function uninstall()
	{
		/*self::dropColumn('cart', 'ddw_order_date');
		self::dropColumn('cart', 'ddw_order_time');
		self::dropColumn('orders', 'ddw_order_date');
		self::dropColumn('orders', 'ddw_order_time');*/
		self::dropTable('ddw');
		self::dropTable('ddw_blocked_dates');
		self::dropTable('ddw_timeslots');
		self::dropTable('ddw_timeslots_lang');
		self::dropTable('ddw_translations');
		self::dropTable('ddw_translations_lang');
		self::dropTable('ddw_weekdays');
        self::dropTable('ddw_scopes');
        self::dropTable('ddw_specificdate');
		return true;
	}

}