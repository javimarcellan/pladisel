<?php
/**
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * You must not modify, adapt or create derivative works of this source code
 *
 * @author    Musaffar Patel
 * @copyright 2016-2017 Musaffar Patel
 * @license   LICENSE.txt
 */

class DDWScope extends ObjectModel
{
	public $id_carrier;
	public $id_associated;
	public $scope;
	public $cutofftime_enabled = 0;
	public $cutofftime_hours = 0;
	public $cutofftime_minutes = 0;
	public $min_days = 0;
	public $max_days = 0;

	public $name;

	public static $definition = array(
		'table' => 'ddw_scopes',
		'primary' => 'id_scope',
		'multilang' => false,
		'fields' => array(
			'id_carrier' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt', 'required' => true),
			'id_associated' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt', 'required' => true),
			'cutofftime_enabled' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt', 'required' => true),
			'cutofftime_hours' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt', 'required' => true),
			'cutofftime_minutes' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt', 'required' => true),
			'min_days' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt', 'required' => true),
			'max_days' => array('type' => self::TYPE_INT, 'required' => false),
			'scope' => array('type' => self::TYPE_STRING, 'required' => false)
		)
	);

	public function getCollection($id_carrier, $id_lang, $id_shop)
	{
		$sql = new DbQuery();
		$sql->select('*');
		$sql->from('ddw_scopes');
		$sql->where('id_carrier = '.(int)$id_carrier);
		$sql->orderBy('scope');
		$result = Db::getInstance()->executeS($sql);

		if (!empty($result))
		{
			foreach ($result as &$row)
			{
				$ddw_scope = DDWScopeHelper::getInstance($row['scope'], $id_lang, $id_shop);
				$row['name'] = $ddw_scope->getName($row['id_associated']);
			}
		}
		return $this->hydrateCollection('DDWScope', $result);
	}

	public function load($id_associated, $id_carrier, $scope)
	{
		$sql = new DbQuery();
		$sql->select('*');
		$sql->from('ddw_scopes');
		$sql->where('id_carrier = '.(int)$id_carrier);
		$sql->where('id_associated = '.(int)$id_associated);
		$sql->where('scope LIKE "'.pSQL($scope).'"');
		$row = Db::getInstance()->getRow($sql);

		if (!empty($row)) {
			$this->hydrate($row);
		} else {
			return false;
		}
	}

}