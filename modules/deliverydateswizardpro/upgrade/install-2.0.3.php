<?php
/**
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * You must not modify, adapt or create derivative works of this source code
 *
 * @author    Musaffar Patel
 * @copyright 2016-2017 Musaffar Patel
 * @license   LICENSE.txt
 */

if (!defined('_PS_VERSION_'))
	exit;

include_once(_PS_MODULE_DIR_.'/deliverydateswizardpro/lib/bootstrap.php');

function upgrade_module_2_0_3($object)
{
	DDWInstall::addColumn('ddw_timeslots', 'enabled', 'tinyint(3) unsigned NOT NULL DEFAULT "1"');
	return true;
}