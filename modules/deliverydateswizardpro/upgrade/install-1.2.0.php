<?php
/**
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * You must not modify, adapt or create derivative works of this source code
 *
 * @author    Musaffar Patel
 * @copyright 2016-2017 Musaffar Patel
 * @license   LICENSE.txt
 */

if (!defined('_PS_VERSION_'))
	exit;

include_once(_PS_MODULE_DIR_.'/deliverydateswizardpro/lib/bootstrap.php');

function upgrade_module_1_2_0($object)
{
	$return = '';
	$return &= Db::getInstance()->execute('
			CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'ddw_scopes` (
				`id_scope` int(11) unsigned NOT NULL AUTO_INCREMENT,
				`id_carrier` mediumint(8) unsigned NOT NULL,
				`id_associated` int(10) unsigned NOT NULL,
				`scope` varchar(255) NOT NULL,
				`cutofftime_enabled` tinyint(3) unsigned NOT NULL DEFAULT "0",
				`cutofftime_hours` tinyint(3) unsigned NOT NULL DEFAULT "0",
				`cutofftime_minutes` tinyint(4) NOT NULL DEFAULT "0",
				`min_days` mediumint(8) unsigned NOT NULL DEFAULT "0",
				`max_days` mediumint(8) unsigned NOT NULL DEFAULT "0",
				PRIMARY KEY (`id_scope`)
			) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8;');

	DDWInstall::addColumn('ddw', 'product_eta_display_enabled', 'tinyint(3) unsigned NOT NULL DEFAULT "0"');
	return true;
}