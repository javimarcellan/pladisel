<?php
/**
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * You must not modify, adapt or create derivative works of this source code
 *
 * @author    Musaffar Patel
 * @copyright 2016-2017 Musaffar Patel
 * @license   LICENSE.txt
 */

include(dirname(__FILE__).'/../../config/config.inc.php');
include(dirname(__FILE__).'/../../init.php');
include_once(_PS_MODULE_DIR_.'/deliverydateswizardpro/lib/bootstrap.php');

$module = Module::getInstanceByName('deliverydateswizardpro');
$controller_front = new DDWControllerFront($module);

if (Tools::getValue('route') != '')
{
	$ddw_producttab_controller = new DDWProductTabController($module);
	die($ddw_producttab_controller->route());
}

switch (Tools::getValue('route'))
{
	default :
		switch (Tools::getValue('action'))
		{
			case 'get_blocked_dates' :
				die(Tools::jsonEncode($controller_front->getBlockedDates()));
				break;
			case 'get_last_ddw_cart' :
				die(Tools::jsonEncode($controller_front->getLastDDWCart()));
				break;
			case 'update_ddw_cart' :
				die($controller_front->update_ddw_cart());
				break;
			case 'get_timeslots' :
				die($controller_front->renderTimeSlots());
				break;
			case 'update_ddw_order_detail':
				die(DDW::saveToOrderDirect(Tools::getValue('id_order'), Tools::getValue('ddw_order_date'), Tools::getValue('ddw_order_time')));
				break;
		}
		break;
}



?>