{*
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author Musaffar Patel <musaffar.patel@gmail.com>
*  @copyright  2007-2016 Musaffar Patel
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Property of Musaffar Patel
*}

<div id="delivery-date-panel">

	<div class="ddw-error alert alert-danger" style="display: none">{$required_error|escape:'html':'UTF-8'}</div>

	<div class="ddw_text_checkout">{$text_checkout nofilter}</div>
	<div class="row">
		<div class="calendar-wrapper col-xs-12 col-sm-6">
			<div id="ddw_calendar" style="width:100%"></div>
			<span id="ddw_text" style="display:none"></span>
		</div>
		<div id="ddw_timeslots" class="col-xs-6 col-sm-6">
		</div>
	</div>

	<div class="row">
		<div id="ddw-summary" class="col-xs-12 col-sm-6">
			<span class="text">{l s='Your order will be delivered on' mod='deliverydateswizardpro'}</span>
			<span class="delivery-date"></span>
			<span class="delivery-time"></span>
		</div>
	</div>
</div>

<input id="ddw_order_date" name="ddw_order_date" type="hidden" value="">
<input id="ddw_order_time" name="ddw_order_time" type="hidden" value="">

<script>

	function initDDW() {
		baseDir = "{$baseDir|escape:'html':'UTF-8'}";

		ddw_months = {
			1 : "{l s='January' mod='deliverydateswizardpro'}",
			2 : "{l s='February' mod='deliverydateswizardpro'}",
			3 : "{l s='March' mod='deliverydateswizardpro'}",
			4: "{l s='April' mod='deliverydateswizardpro'}",
			5: "{l s='May' mod='deliverydateswizardpro'}",
			6: "{l s='June' mod='deliverydateswizardpro'}",
			7: "{l s='July' mod='deliverydateswizardpro'}",
			8: "{l s='August' mod='deliverydateswizardpro'}",
			9: "{l s='September' mod='deliverydateswizardpro'}",
			10: "{l s='October' mod='deliverydateswizardpro'}",
			11: "{l s='November' mod='deliverydateswizardpro'}",
			12: "{l s='December' mod='deliverydateswizardpro'}"
		};

		ddw_texts = {
			summary : {
				select_date : "{l s='please select a date' mod='deliverydateswizardpro'}",
				select_time: "{l s='please select a time' mod='deliverydateswizardpro'}"
			},
			generic : {
				required_error : "{$required_error|escape:'html':'UTF-8'}"
			}
		};

		id_lang = "{$id_lang|escape:'html':'UTF-8'}";
		controller_name = "{$controller_name|escape:'html':'UTF-8'}";
		controller = "{$controller|escape:'html':'UTF-8'}";

		ddw = new DDWFrontEnd(controller);

		if (typeof (window.ddw_order_date_cache) !== 'undefined') {
			$("#ddw_order_date").val(window.ddw_order_date_cache);
			$("span#ddw_text").html(window.ddw_order_date_cache);
		}
		if (typeof (window.ddw_order_time_cache) !== 'undefined') $("#ddw_order_time").val(window.ddw_order_time_cache);
		/* if elements such as the gift option cause the widget to reload, use the window as caches for the date and time, and resubmit for saving to cart as it's being cleared each time the state gift option checkboxes changes */
		if (typeof (window.ddw_order_date_cache) !== 'undefined' || typeof (window.ddw_order_time_cache) !== 'undefined')
			ddw.updateDDWCart();
	};


	if (typeof $ === 'undefined')
		document.addEventListener("DOMContentLoaded", function (event) {
			initDDW();
		});
	else
		$(function() {
			initDDW();
		});

</script>

