{*
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author Musaffar Patel <musaffar.patel@gmail.com>
*  @copyright  2007-2016 Musaffar Patel
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Property of Musaffar Patel
*}

{if isset($timeslots)}
	<div id="ddw-timeslots">
		{foreach from=$timeslots item=timeslot}
			{if $timeslot->enabled}
				<div class="timeslot {if $timeslot->available eq 1}available{else}unavailable{/if}">
					<span class="radio" style="display:none"><input type="radio" name="timeslot" value="{$timeslot->id|escape:'htmlall':'UTF-8'}" class="ddw_time_slot chk_timeslot"></span>
					<i class="material-icons">schedule</i>
					<span class="text">
						<span class="time">
							{$timeslot->time_start|escape:'htmlall':'UTF-8'} - {$timeslot->time_end|escape:'htmlall':'UTF-8'}
						</span>
					</span>
				</div>
			{/if}
		{/foreach}
	</div>
{/if}