{*
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author Musaffar Patel <musaffar.patel@gmail.com>
*  @copyright  2007-2016 Musaffar Patel
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Property of Musaffar Patel
*}

{if $order_ddw.ddw_order_date neq ""}
	{if isset($ps_version) && ($ps_version < '1.6')}
		<br/>
		<fieldset>
		<legend><img src="../img/admin/delivery.gif">{l s='Delivery Dates Wizard' mod='deliverydateswizardpro'}</legend>
	{else}
		<div class="col-lg-12">
		<div class="panel">
		<h3>
			<i class="icon-truck"></i>
			{l s='Delivery Dates Wizard' mod='deliverydateswizardpro'}
		</h3>
	{/if}
	<table style="width: 100%">
		<tr>
			<td width="10%">
				{l s='Delivery Date:' mod='deliverydateswizardpro'}
			</td>
			<td>
				<div class="input-group fixed-width-xl">
					<input id="ddw_order_date" type="text" data-hex="true" class="datepicker" name="ddw_order_date" value="{$order_ddw.ddw_order_date|escape:'htmlall':'UTF-8'}">
					<div class="input-group-addon">
						<i class="icon-calendar-o"></i>
					</div>
				</div>
			</td>
		</tr>
		<tr><td height="5"></td></tr>
		<tr>
			<td>
				{l s='Delivery Time:' mod='deliverydateswizardpro'}
			</td>
			<td>
				<div class="input-group fixed-width-xl">
					<input type="text" name="ddw_order_time" id="ddw_order_time" value="{$order_ddw.ddw_order_time|escape:'htmlall':'UTF-8'}" class="">
				</div>
			</td>
		</tr>
		<tr><td height="10"></td></tr>
		<tr>
			<td></td>
			<td align="left">
				<button type="submit" id="submitDDW" class="btn btn-primary pull-left" name="submitDDW">
					{l s='Update' mod='deliverydateswizardpro'}
				</button>
			</td>
		</tr>
	</table>
	{if !(isset($ps_version) && ($ps_version < '1.6'))}
		</div>
		</div>
	{else}
		</fieldset>
	{/if}
{/if}

<script>
	$(document).ready(function() {

		ddw_baseDir = '{$base_url|escape:'htmlall':'UTF-8'}';

		$("#submitDDW").click(function() {
			$.ajax({
				type: 'POST',
				url: ddw_baseDir + 'modules/deliverydateswizardpro/ajax.php?action=update_ddw_order_detail&rand='+new Date().getTime(),
				cache: false,
				data: {
					ddw_order_date: $("input#ddw_order_date").val(),
					ddw_order_time: $("input#ddw_order_time").val(),
					id_order: {$smarty.get.id_order|intval},
				},
				success: function (jsonData) {
					if (typeof($.growl) !== 'undefined')
						$.growl({
							title: "Delivery Dates Wizard Pro", message: "Delivery date and time have been saved"
						});
					else
						alert('Delivery date and time have been saved');
				}
			});
		});
	});
</script>

