{*
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author Musaffar Patel <musaffar.patel@gmail.com>
*  @copyright  2007-2016 Musaffar Patel
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Property of Musaffar Patel
*}


<div id="ddw-product-general" class="row">
    <div id="ddw-options" class="col-md-12">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <h2>{l s='Override main options' mod='deliverydateswizardpro'}</h2>
                    <div id="ddw-carriers" class="row">
                        <div class="col-md-9">
                            <fieldset class="form-group">
                                <select id="id_carrier" name="id_carrier" class="carrier-list" data-toggle="select2">
                                    <option selected>Select a carrier</option>
                                    {foreach from=$carriers item=carrier}
                                        <option value="{$carrier.id_carrier|escape:'htmlall':'UTF-8'}">{$carrier.name|escape:'htmlall':'UTF-8'}</option>
                                    {/foreach}
                                </select>
                            </fieldset>
                        </div>
                    </div>

                    <div class="row form-group">
                        <div class="col-md-4">
                            <label class="form-control-label">Min Days</label>
                            <input type="text" name="min_days" id="min_days" class="form-control edit" value="{$ddw_scope->min_days|escape:'htmlall':'UTF-8'}" style="width:90px;"></input>
                        </div>
                    </div>

                    <div class="row form-group">
                        <div class="col-md-4">
                            <label class="form-control-label">Max Days</label>
                            <input type="text" name="max_days" id="max_days" class="form-control edit" value="{$ddw_scope->max_days|escape:'htmlall':'UTF-8'}" style="width:90px;"></input>
                        </div>
                    </div>

                    <div class="row form-group">
                        <div class="col-md-4">
                            <label class="form-control-label">Cut off time Enabled?</label>
							<span class="switch prestashop-switch fixed-width-lg">
								<input type="radio" name="product_cutofftime_enabled" id="product_cutofftime_enabled_on" value="1" {if $ddw_scope->cutofftime_enabled eq "1"}checked{/if}>
								<label for="product_cutofftime_enabled_on">Yes</label>
								<input type="radio" name="product_cutofftime_enabled" id="product_cutofftime_enabled_off" value="0" {if $ddw_scope->cutofftime_enabled neq "1"}checked{/if}>
								<label for="product_cutofftime_enabled_off">No</label>
								<a class="slide-button btn"></a>
							</span>
                        </div>
                    </div>

                    <div class="row form-group">
                        <div class="col-md-4">
                            <label class="form-control-label"></label>
                            <div class="cutofftime-panel col-lg-9">
                                <div class="input-group">
                                    <span class="input-group-addon" style="width:120px;">Cut off Hours</span>
                                    <select name="cutofftime_hours" id="cutofftime_hours" style="width:80px" class="c-select">
                                        {foreach from=$hour_values item=hour}
                                            <option value="{$hour|escape:'htmlall':'UTF-8'}" {if $hour eq $ddw_scope->cutofftime_hours}selected="selected"{/if}>{"%02d"|sprintf:$hour|escape:'htmlall':'UTF-8'}</option>
                                        {/foreach}
                                    </select>
                                </div>
                                <div class="input-group">
                                    <span class="input-group-addon" style="width:120px">Cut off Min</span>
                                    <select name="cutofftime_minutes" id="cutofftime_minutes" style="width:80px" class="c-select">
                                        {foreach from=$min_values item=min}
                                            <option value="{$min|escape:'htmlall':'UTF-8'}" {if $min eq $ddw_scope->cutofftime_minutes}selected="selected"{/if}>{"%02d"|sprintf:$min|escape:'htmlall':'UTF-8'}</option>
                                        {/foreach}
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group hide">
                        <input type="hidden" name="id_scope" id="id_scope" value="">
                        <input type="hidden" name="id_product" id="id_product" value="{$id_product|escape:'htmlall':'UTF-8'}">
                    </div>
                </div>
                <div class="col-md-12">
                    <button type="button" value="1" id="ddw-general-save" class="btn btn-tertiary pull-right">Save</button>
                </div>
            </div>
        </div>
    </div>
</div>


<script>
	$(document).ready(function () {
		module_ajax_url_ddw = '{$module_ajax_url|escape:'quotes':'UTF-8'}';
		var ddw_product_general = new DDWProductGeneral('#ddw-product-general');
	});
</script>