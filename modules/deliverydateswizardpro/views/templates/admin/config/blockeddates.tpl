{*
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author Musaffar Patel <musaffar.patel@gmail.com>
*  @copyright  2007-2016 Musaffar Patel
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Property of Musaffar Patel
*}

<div id="ddw-blockeddates" class="ddw-tab-panel">
	<h3>{l s='Blocked Dates & Date Ranges' mod='deliverydateswizardpro'}</h3>

	<div class="row">
		<div class="col-xs-12">
			<table id="timeslotsTable" class="table tableDnD table-striped">
				<thead>
				<tr class="nodrag nodrop">
					<th><span class="title_box">From</span></th>
					<th><span class="title_box">To</span></th>
					<th><span class="title_box">Recurring</span></th>
					<th><span class="title_box">Delete</span></th>
				</tr>
				</thead>
				<tbody>
				{foreach from=$ddw_blockeddates item=ddw_blockeddate}
					<tr>
						<td class="start_date">{$ddw_blockeddate->start_date|escape:'htmlall':'UTF-8'}</td>
						<td class="end_date">{$ddw_blockeddate->end_date|escape:'htmlall':'UTF-8'}</td>
						<td class="recurring">{$ddw_blockeddate->recurring|escape:'htmlall':'UTF-8'}</td>
						<td>
							<a href="#delete" class="ddw-blockeddate-delete" data-id="{$ddw_blockeddate->id|escape:'htmlall':'UTF-8'}"><i class="icon icon-trash"></i></a>
						</td>
					</tr>
				{/foreach}
				</tbody>
			</table>
		</div>
	</div>

	<div class="panel-footer row">
		<button type="button" value="1" id="ddw-blockeddate-add" class="btn btn-default pull-right">
			<i class="process-icon-save"></i> Add
		</button>
	</div>
</div>
