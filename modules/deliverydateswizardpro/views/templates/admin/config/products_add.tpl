{*
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author Musaffar Patel <musaffar.patel@gmail.com>
*  @copyright  2007-2016 Musaffar Patel
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Property of Musaffar Patel
*}

<div id="ddw-product" class="ddw-tab-panel">
	<div class="panel-heading">
		<i class="icon-cogs"></i> Add Product based Override
	</div>

	<div class="form-wrapper">

		<div class="form-group row">
			<label class="control-label col-lg-3">
				Scope
			</label>

			<div class="col-lg-9 ">
				<select name="scope" id="scope">
					<option value="supplier">Supplier</option>
					<option value="category">Category</option>
				</select>
				<script>
					$(document).ready(function() {
						$("#ddw-product select#scope").val('{$ddw_scope->scope|escape:'htmlall':'UTF-8'}');
					});
				</script>
			</div>
		</div>

		<div class="form-group row">
			<label class="control-label col-lg-3">
				Search:
			</label>

			<div class="col-lg-9">

				<div class="input-group col-xs-9">
					<div class="input-group col-xs-9">
						<input id="product_search" type="text" name="product_search" value="{$ddw_scope->name|escape:'htmlall':'UTF-8'}">
						<span class="input-group-addon">
							<i class="icon-search"></i>
						</span>
					</div>

					{* Product search results *}
					<div id="product-search-results">
						<table id="search-results-table" class="table">
							<thead>
							<tr class="nodrag nodrop">
								<th><span class="title_box">Scope</span></th>
								<th><span class="title_box">Name</span></th>
							</tr>
							</thead>

							<tbody>
								<tr class="cloneable hidden">
									<td class="scope"></td>
									<td class="name"></td>
								</tr>
							</tbody>
						</table>
					</div>
					{* / Product Search Results *}

				</div>
			</div>
		</div>
	</div>

	<div class="form-wrapper">
		<div class="form-group">
			<label class="control-label col-lg-3">
				Min Days
			</label>
			<div class="col-lg-2">
				<input type="text" name="min_days" id="min_days" value="{$ddw_scope->min_days|escape:'htmlall':'UTF-8'}">
			</div>
		</div>
	</div>

	<div class="form-wrapper">
		<div class="form-group row">
			<label class="control-label col-lg-3">
				Max Days
			</label>

			<div class="col-lg-2">
				<input type="text" name="max_days" id="max_days" value="{$ddw_scope->max_days|escape:'htmlall':'UTF-8'}">
			</div>
		</div>
	</div>

	<div class="form-wrapper">
		<div class="form-group row">
			<label class="control-label col-lg-3">
				Cut off time Enabled?
			</label>

			<div class="col-lg-10">
				<input data-toggle="switch" class="" id="product_cutofftime_enabled" data-inverse="true" type="checkbox" name="product_cutofftime_enabled" value="1" {if $ddw_scope->cutofftime_enabled eq "1"}checked{/if}>
			</div>
		</div>
	</div>

	<div class="form-group row">
		<label class="control-label col-lg-3">
		</label>

		<div class="cutofftime-panel col-lg-10">
			<div class="input-group">
				<span class="input-group-addon" style="width:100px;">Cut off Hours</span>
				<select name="cutofftime_hours" id="cutofftime_hours" style="width:80px">
					{foreach from=$hour_values item=hour}
						<option value="{"%02d"|sprintf:$hour|escape:'htmlall':'UTF-8'}" {if $hour eq $ddw_scope->cutofftime_hours}selected="selected"{/if}>{"%02d"|sprintf:$hour|escape:'htmlall':'UTF-8'}</option>
					{/foreach}
				</select>
			</div>
			<div class="input-group">
				<span class="input-group-addon" style="width:100px">Cut off Min</span>
				<select name="cutofftime_minutes" id="cutofftime_minutes" style="width:80px">
					{foreach from=$min_values item=min}
						<option value="{"%02d"|sprintf:$min|escape:'htmlall':'UTF-8'}" {if $min eq $ddw_scope->cutofftime_minutes}selected="selected"{/if}>{"%02d"|sprintf:$min|escape:'htmlall':'UTF-8'}</option>
					{/foreach}
				</select>
			</div>
		</div>
	</div>

	<!-- /.form-wrapper -->

	<div class="form-group hide">
		<input type="hidden" name="id_scope" id="id_scope" value="{$ddw_scope->id|escape:'htmlall':'UTF-8'}">
		<input type="hidden" name="id" id="id" value="{$ddw_scope->id_associated|escape:'htmlall':'UTF-8'}">
		<input type="hidden" name="scope" id="scope" value="{$ddw_scope->scope|escape:'htmlall':'UTF-8'}">
		<input type="hidden" name="id_carrier" id="id_carrier" value="{$smarty.get.id_carrier|escape:'htmlall':'UTF-8'}">
	</div>

	<div class="form-group row">
		<div class="panel-footer">
			<button type="button" value="1" id="ddw-products-save" class="btn btn-default pull-right">
				<i class="process-icon-save"></i> Save
			</button>
		</div>
	</div>
</div>


<script>
	$(document).ready(function() {
		//prestaShopUiKit.init();
	});
</script>

