{*
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author Musaffar Patel <musaffar.patel@gmail.com>
*  @copyright  2007-2016 Musaffar Patel
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Property of Musaffar Patel
*}

<ul class="nav nav-tabs" id="myTab" role="tablist">
	<li class="nav-item">
		<a class="nav-link" data-toggle="tab" href="#ddw-general-tab" role="tab">General</a>
	</li>
	<li class="nav-item">
		<a class="nav-link" data-toggle="tab" href="#ddw-weekdays-tab" role="tab">Weekdays</a>
	</li>
	<li class="nav-item">
		<a class="nav-link" data-toggle="tab" href="#ddw-specificdates-tab" role="tab">{l s='Specific Dates' mod='deliverydateswizardpro'}</a>
	</li>
	<li class="nav-item">
		<a class="nav-link" data-toggle="tab" href="#ddw-blockeddates-tab" role="tab">Blocked Dates</a>
	</li>
	<li class="nav-item">
		<a class="nav-link" data-toggle="tab" href="#ddw-products-tab" role="tab">Product Rules</a>
	</li>
</ul>

<div class="tab-content">
	<div class="tab-pane active" id="ddw-general-tab" role="tabpanel"></div>
	<div class="tab-pane" id="ddw-weekdays-tab" role="tabpanel"></div>
	<div class="tab-pane" id="ddw-specificdates-tab" role="tabpanel"></div>
	<div class="tab-pane" id="ddw-blockeddates-tab" role="tabpanel"></div>
	<div class="tab-pane" id="ddw-products-tab" role="tabpanel"></div>
</div>

<script>
	$(document).ready(function () {
		module_config_url = '{$module_config_url|escape:'quotes':'UTF-8'}';
		ddw = {$ddw|@json_encode};
		smarty = [];
		smarty.id_carrier = "{$smarty.get.id_carrier|escape:'htmlall':'UTF-8'}";

		ddw_general = new DDWGeneral('#ddw-general-tab');
		ddw_weekdays = new DDWWeekdays("#ddw-weekdays-tab");
		ddw_blockeddates = new DDWBlockedDates("#ddw-blockeddates-tab");
		ddw_products = new DDWProducts("#ddw-products-tab");
		ddw_specificdates = new DDWSpecificDates("#ddw-specificdates-tab");

	});
</script>