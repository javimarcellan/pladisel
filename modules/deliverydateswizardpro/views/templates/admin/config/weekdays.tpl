{*
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author Musaffar Patel <musaffar.patel@gmail.com>
*  @copyright  2007-2016 Musaffar Patel
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Property of Musaffar Patel
*}

<div id="ddw-weekdays" class="ddw-tab-panel">
	<h3>{l s='Setup your week' mod='deliverydateswizardpro'}</h3>
	<div class="form-wrapper">
		<div class="form-group row">
			{foreach from=$weekdays item=weekday}
				<div class="weekday col-xs-4 col-sm-3 col-md-1">
					<span class="title">{$weekday.label|escape:'htmlall':'UTF-8'}</span>
					<span class="status list-action-enable action-enabled">
						<a href="#edit" class="ddw-edit-weekday" data-id_carrier="{$id_carrier|escape:'quotes':'UTF-8'}" data-id_weekday="{$weekday.id_weekday|escape:'quotes':'UTF-8'}"><i class="icon icon-pencil"></i></a>
					</span>
				</div>
			{/foreach}
		</div>
	</div>
	<!-- /.form-wrapper -->
</div>