{*
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author Musaffar Patel <musaffar.patel@gmail.com>
*  @copyright  2007-2016 Musaffar Patel
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Property of Musaffar Patel
*}

<div id="ddw-blockeddate" class="panel">
	<div class="panel-heading">
		<i class="icon-cogs"></i> Add Blocked Date
	</div>

	<div class="form-wrapper">

		<div class="form-group row">
			<label class="control-label col-lg-3">
				Recurring?
			</label>

			<div class="col-lg-9">
				{*<input data-toggle="switch" class="" id="recurring" data-inverse="true" type="checkbox" name="recurring" value="1" {if $ddw->enabled eq "1"}checked{/if}>*}
				<span class="switch prestashop-switch fixed-width-lg">
					<input type="radio" name="recurring" id="recurring_on" value="1" {if $ddw->enabled eq "1"}checked{/if}>
					<label for="recurring_on">Yes</label>
					<input type="radio" name="recurring" id="recurring_off" value="0" {if $ddw->enabled neq "1"}checked{/if}>
					<label for="recurring_off">No</label>
					<a class="slide-button btn"></a>
				</span>
			</div>
		</div>

		<div class="form-group row">
			<label class="control-label col-lg-3">
				Start Date?
			</label>

			<div class="col-lg-9 ">
				<div class="input-group col-lg-4">
					<input id="start_date" type="text" class="datepicker" name="start_date" value="" style="position: relative; z-index: 9999">
					<span class="input-group-addon">
						<i class="icon-calendar-empty"></i>
					</span>
				</div>
			</div>
		</div>

		<div class="form-group row">
			<label class="control-label col-lg-3">
				End Date?
			</label>

			<div class="col-lg-9 ">
				<div class="input-group col-lg-4">
					<input id="end_date" type="text" class="datepicker" name="end_date" value="" style="position: relative; z-index: 9999">
					<span class="input-group-addon">
						<i class="icon-calendar-empty"></i>
					</span>
				</div>
			</div>
		</div>


		<div class="form-group hide">
			<input type="hidden" name="id_blockeddate" id="id_blockeddate" value="{$smarty.get.id_blockeddate|escape:'htmlall':'UTF-8'}">
			<input type="hidden" name="id_carrier" id="id_carrier" value="{$smarty.get.id_carrier|escape:'htmlall':'UTF-8'}">
		</div>
	</div>
	<!-- /.form-wrapper -->

	<div class="panel-footer">
		<button type="button" value="1" id="ddw-blockeddate-save" class="btn btn-default pull-right">
			<i class="process-icon-save"></i> Save
		</button>
	</div>
</div>


<script>
	$(document).ready(function () {
		//prestaShopUiKit.init();
		$(".datepicker").datepicker({
			prevText: '',
			nextText: '',
			dateFormat: 'yy-mm-dd'
		});
	});
</script>