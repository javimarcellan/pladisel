{*
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author Musaffar Patel <musaffar.patel@gmail.com>
*  @copyright  2007-2016 Musaffar Patel
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Property of Musaffar Patel
*}

<input type="hidden" id="ddw_timeslots" name="ddw_timeslots" value="">
<input type="hidden" id="id" name="id" value="{$smarty.get.id|escape:'htmlall':'UTF-8'}">
<input type="hidden" id="id_carrier" name="id_carrier" value="{$id_carrier|escape:'htmlall':'UTF-8'}">
<input type="hidden" id="model" name="moedl" value="{$model|escape:'htmlall':'UTF-8'}">

<div id="panel1" class="ddw-tab-panel">
	{if $model eq 'specificdate'}
		<h3>{l s='Edit Specific Date' mod='deliverydateswizardpro'}</h3>
	{else}
		<h3>{l s='Edit Weekday' mod='deliverydateswizardpro'}</h3>
	{/if}

	<div class="form-wrapper">

		{if $model eq 'specificdate'}
            <div class="form-group row">
                <label class="control-label col-lg-4 label-tooltip" for="" title="{l s='Date' mod='deliverydateswizardpro'}">
                    <span class="label-tooltip">{l s='Date' mod='deliverydateswizardpro'}</span>
                </label>
                <div class="col-lg-8">
                    <div class="input-group col-lg-4">
                        <input id="start_date" type="text" class="datepicker" name="start_date" value="{$ddw_weekday->start_date|escape:'htmlall':'UTF-8'}" style="position: relative; z-index: 9999">
                        <span class="input-group-addon">
                            <i class="icon-calendar-empty"></i>
                        </span>
                    </div>
                </div>
            </div>
		{/if}

		<div class="form-group row">
			<label class="control-label col-lg-4 label-tooltip" for="id_ppbs_dimension" title="Is weekday selectable in Calendar?">
				<span class="label-tooltip">Enabled?</span>
			</label>
			<div class="col-lg-8">
				{*<input data-toggle="switch" class="" id="weekday_enabled" data-inverse="true" type="checkbox" name="weekday_enabled" value="1" {if $ddw_weekday->enabled eq "1"}checked{/if}>*}
				<span class="switch prestashop-switch fixed-width-lg">
					<input type="radio" name="weekday_enabled" id="weekday_enabled_on" value="1" {if $ddw_weekday->enabled eq "1"}checked{/if}>
					<label for="weekday_enabled_on">Yes</label>
					<input type="radio" name="weekday_enabled" id="weekday_enabled_off" value="0" {if $ddw_weekday->enabled neq "1"}checked{/if}>
					<label for="weekday_enabled_off">No</label>
					<a class="slide-button btn"></a>
				</span>
			</div>
		</div>

		<div class="form-group row">
			<label class="control-label col-lg-4" for="id_ppbs_dimension">
				<span class="label-tooltip" title="Weekday should override main Min / Max days setting">Override Min / Max Days?</span>
			</label>

			<div class="col-lg-8">
				{*<input data-toggle="switch" class="" id="minmax_enabled" data-inverse="true" type="checkbox" name="minmax_enabled" value="1" {if $ddw_weekday->minmax_enabled eq "1"}checked{/if}>*}
				<span class="switch prestashop-switch fixed-width-lg">
					<input type="radio" name="minmax_enabled" id="minmax_enabled_on" value="1" {if $ddw_weekday->minmax_enabled eq "1"}checked{/if}>
					<label for="minmax_enabled_on">Yes</label>
					<input type="radio" name="minmax_enabled" id="minmax_enabled_off" value="0" {if $ddw_weekday->minmax_enabled neq "1"}checked{/if}>
					<label for="minmax_enabled_off">No</label>
					<a class="slide-button btn"></a>
				</span>
			</div>
		</div>

	</div>

	<div class="form-group row">
		<label class="control-label col-lg-4" for="spm_currency_0">
			<span class="label-tooltip" title="Min Days">Min Days</span>
		</label>
		<div class="col-lg-6">
			<input id="min_days" name="min_days" type="text" value="{$ddw_weekday->min_days|escape:'htmlall':'UTF-8'}" maxlength="8">
		</div>
	</div>

	<div class="form-group row">
		<label class="control-label col-lg-4" for="spm_currency_0">
			<span class="label-tooltip" title="Max">Max Days</span>
		</label>
		<div class="col-lg-6">
			<input id="max_days" name="max_days" type="text" value="{$ddw_weekday->max_days|escape:'htmlall':'UTF-8'}" maxlength="8">
		</div>
	</div>

	<div class="form-group row">
		<label class="control-label col-lg-4 label-tooltip" title="Override cut off time">
			<span class="label-tooltip">Override cut off time?</span>
		</label>
		<div class="col-lg-8">
			{*<input data-toggle="switch" class="" id="weekday_cutofftime_enabled" data-inverse="true" type="checkbox" name="weekday_cutofftime_enabled" value="1" {if $ddw_weekday->cutofftime_enabled eq "1"}checked{/if}>*}
			<span class="switch prestashop-switch fixed-width-lg">
				<input type="radio" name="weekday_cutofftime_enabled" id="weekday_cutofftime_enabled_on" value="1" {if $ddw_weekday->cutofftime_enabled eq "1"}checked{/if}>
				<label for="weekday_cutofftime_enabled_on">Yes</label>
				<input type="radio" name="weekday_cutofftime_enabled" id="weekday_cutofftime_enabled_off" value="0" {if $ddw_weekday->cutofftime_enabled neq "1"}checked{/if}>
				<label for="weekday_cutofftime_enabled_off">No</label>
				<a class="slide-button btn"></a>
			</span>
		</div>
	</div>

	<div class="form-group row">
		<label class="control-label col-lg-4">
		</label>
		<div class="cutofftime-panel col-lg-8">
			<div class="input-group">
				<span class="input-group-addon" style="width:100px;">Cut off Hours</span>
				<select name="weekday_cutofftime_hours" id="weekday_cutofftime_hours" style="width:80px">
					{foreach from=$hour_values item=hour}
						<option value="{"%02d"|sprintf:$hour|escape:'htmlall':'UTF-8'}" {if $hour eq $ddw_weekday->cutofftime_hours}selected="selected"{/if}>{"%02d"|sprintf:$hour|escape:'htmlall':'UTF-8'}</option>
					{/foreach}
				</select>
			</div>
			<div class="input-group">
				<span class="input-group-addon" style="width:100px">Cut off Min</span>
				<select name="weekday_cutofftime_minutes" id="weekday_cutofftime_minutes" style="width:80px">
					{foreach from=$min_values item=min}
						<option value="{"%02d"|sprintf:$min|escape:'htmlall':'UTF-8'}" {if $min eq $ddw_weekday->cutofftime_minutes}selected="selected"{/if}>{"%02d"|sprintf:$min|escape:'htmlall':'UTF-8'}</option>
					{/foreach}
				</select>
			</div>
		</div>
	</div>	
	
	<div class="form-group row">
		<label class="control-label col-lg-4" for="spm_currency_0">
			<span class="label-tooltip" title="Max">Min days (after cut off time)</span>
		</label>
		<div class="col-lg-6">
			<input id="min_days_postcutoff" name="min_days_postcutoff" type="text" value="{$ddw_weekday->min_days_postcutoff|escape:'htmlall':'UTF-8'}" maxlength="8">
		</div>
	</div>

	<div class="form-group row">
		<label class="control-label col-lg-4" for="spm_currency_0">
			<span class="label-tooltip" title="Max">Time slots</span>
		</label>

		<div class="col-lg-6">
			<a id="edit-timeslots" href="#edit-timeslots" class="btn btn-default">
				<i class="icon icon-list"></i>
				<span>Edit timeslots</span>
			</a>
		</div>
	</div>


	<div class="panel-footer">
		<a href="#close" id="ddw-weekday-edit-cancel" class="btn btn-default"><i class="process-icon-cancel"></i> Cancel</a>
		<button type="submit" id="ddw-popup-save" class="btn btn-default pull-right">
			<i class="process-icon-save"></i> Save
		</button>
	</div>
</div>

{* Start : Panel2 Dropdown Values *}
<div id="panel2" class="panel subpanel" style="z-index:9999">
	<h3>Timeslots</h3>

	<div class="row">
		<div class="col-sm-12"><h4>Add Timeslot</h4></div>
	</div>

	<div class="row">
		<div class="col-sm-12">
			<div class="form-group">
				<label class="control-label col-lg-2" for="spm_currency_0">
					<span class="" title="">{l s='Start Time' mod='deliverydateswizardpro'}</span>
				</label>

				<div class="col-lg-2">
					<div class="input-group">
						<span class="input-group-addon">{l s='HH:MM' mod='deliverydateswizardpro'}</span>
						<input type="text" name="time_start" id="time_start" value="" class="" required="required">
					</div>
				</div>

				<label class="control-label col-lg-2" for="spm_currency_0">
					<span class="" title="">{l s='End Time' mod='deliverydateswizardpro'}</span>
				</label>

				<div class="col-lg-2">
					<div class="input-group">
						<span class="input-group-addon">{l s='HH:MM' mod='deliverydateswizardpro'}</span>
						<input type="text" name="time_end" id="time_end" value="" class="" required="required">
					</div>
				</div>

				<label class="control-label col-lg-2" for="spm_currency_0">
					<span class="" title="">{l s='Order Limit' mod='deliverydateswizardpro'}</span>
				</label>

				<div class="col-lg-2">
					<div class="input-group">
						<span class="input-group-addon">{l s='0 for no limit' mod='deliverydateswizardpro'}</span>
						<input type="text" name="order_limit" id="order_limit" value="" class="" required="required">
					</div>
				</div>


			</div>
		</div>

		<div class="col-lg-2">
			<div class="form-group">
				<a href="#add-timeslot" id="add-timeslot" class="btn btn-default"><i class="icon-plus"></i> Add</a>
			</div>
		</div>

	</div>

	<div class="row timeslots-wrapper">
		<table id="timeslotsTable" class="table tableDnD">
			<thead>
			<tr class="nodrag nodrop">
				<th><span class="title_box">Start Time</span></th>
				<th><span class="title_box">End Time</span></th>
				<th><span class="title_box">Order Limit</span></th>
				<th><span class="title_box">{l s='Enabled' mod='deliverydateswizardpro'}</span></th>
				<th><span class="title_box">Position</span></th>
				<th><span class="title_box">Delete</span></th>
			</tr>
			</thead>

			<tbody>

			{foreach from=$ddw_timeslots item=ddw_timeslot}
				<tr>
					<td class="time_start">{$ddw_timeslot->time_start|escape:'htmlall':'UTF-8'}</td>
					<td class="time_end">{$ddw_timeslot->time_end|escape:'htmlall':'UTF-8'}</td>
					<td class="order_limit">{$ddw_timeslot->order_limit|escape:'htmlall':'UTF-8'}</td>
					<td class="state">
						{if $ddw_timeslot->enabled eq 1}
							<i class="icon icon-check toggle-state" data-id="{$ddw_timeslot->id|escape:'htmlall':'UTF-8'}" data-state="1"></i>
						{else}
							<i class="icon icon-times toggle-state" data-id="{$ddw_timeslot->id|escape:'htmlall':'UTF-8'}" data-state="0"></i>
						{/if}
					</td>
					<td class="dragHandle pointer">
						<div class="dragGroup">
							&nbsp;
						</div>
					</td>
					<td>
						<a href="#delete" class="ddw-timeslot-delete" data-id="{$ddw_timeslot->id|escape:'htmlall':'UTF-8'}"><i class="icon icon-trash"></i></a>
					</td>
				</tr>
			{/foreach}

			<tr class="cloneable hidden">
				<td class="time_start"></td>
				<td class="time_end"></td>
				<td class="order_limit"></td>
				<td class="state">
					<i class="icon icon-check toggle-state" data-id="" data-state="1"></i>
				</td
				<td class="dragHandle pointer">
					<div class="dragGroup">
						&nbsp;
					</div>
				</td>
				<td>
					<a href="#delete" class="ddw-timeslot-delete"><i class="icon icon-trash"></i></a>
				</td>
			</tr>
			</tbody>
		</table>
	</div>

	<div class="panel-footer">
		<a href="#close" id="ddw-timeslots-cancel" class="btn btn-default"><i class="process-icon-cancel"></i> Cancel</a>
		<button type="submit" id="ddw-timeslots-done" class="btn btn-default pull-right">
			<i class="process-icon-check icon-check"></i> Done
		</button>
	</div>

</div>
{* End: Panel2 Dropdown Values *}

<script>
	$(document).ready(function() {
		//prestaShopUiKit.init();
        {if $model eq 'specificdate'}
            $(".datepicker").datepicker({
                prevText: '',
                nextText: '',
                dateFormat: 'yy-mm-dd'
            });
        {/if}
	});
</script>