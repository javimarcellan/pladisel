{*
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author Musaffar Patel <musaffar.patel@gmail.com>
*  @copyright  2007-2016 Musaffar Patel
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Property of Musaffar Patel
*}

<div id="ddw-products" class="ddw-tab-panel">
	<h3>{l s='Product Rules' mod='deliverydateswizardpro'}</h3>

	<div class="row">
		<div class="col-xs-12">
			<table id="productsTable" class="table table-striped">
				<thead>
				<tr class="nodrag nodrop">
					<th><span class="title_box">{l s='Scope' mod='deliverydateswizardpro'}</span></th>
					<th><span class="title_box">{l s='Name' mod='deliverydateswizardpro'}</span></th>
					<th><span class="title_box">{l s='Min Days' mod='deliverydateswizardpro'}</span></th>
					<th><span class="title_box">{l s='Cut off time' mod='deliverydateswizardpro'}</span></th>
					<th><span class="title_box">{l s='Delete' mod='deliverydateswizardpro'}</span></th>
				</tr>
				</thead>
				<tbody>
				{foreach from=$ddw_scopes item=ddw_scope}
					<tr>
						<td class="scope">{$ddw_scope->scope|escape:'htmlall':'UTF-8'}</td>
						<td class="name">{$ddw_scope->name|escape:'htmlall':'UTF-8'}</td>
						<td class="min_days">{$ddw_scope->min_days|escape:'htmlall':'UTF-8'}</td>
						<td class="cutofftime">
							{"%02d"|sprintf:$ddw_scope->cutofftime_hours|escape:'htmlall':'UTF-8'}:
							{"%02d"|sprintf:$ddw_scope->cutofftime_minutes|escape:'htmlall':'UTF-8'}
						</td>
						<td>
							{if $ddw_scope->scope != 'product'}
								<a href="#edit" class="ddw-scope-edit" data-id_scope="{$ddw_scope->id|escape:'htmlall':'UTF-8'}"><i class="icon icon-pencil"></i></a>
							{/if}
							<a href="#delete" class="ddw-scope-delete" data-id_scope="{$ddw_scope->id|escape:'htmlall':'UTF-8'}"><i class="icon icon-trash"></i></a>
						</td>
					</tr>
				{/foreach}
				</tbody>
			</table>
		</div>
	</div>

	<div class="panel-footer row">
		<button type="button" value="1" id="ddw-product-add" class="btn btn-default pull-right">
			<i class="process-icon-save"></i> Add
		</button>
	</div>
</div>
