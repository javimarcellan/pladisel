{*
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author Musaffar Patel <musaffar.patel@gmail.com>
*  @copyright  2007-2016 Musaffar Patel
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Property of Musaffar Patel
*}

<div id="ddw-general" class="ddw-tab-panel">

	<h3>{l s='General Settings' mod='deliverydateswizardpro'}</h3>

	<div class="form-wrapper">

		<div class="form-group row">
			<label class="control-label col-lg-2">
				Enabled?
			</label>

			<div class="col-lg-10">
				{*
				<input data-toggle="switch" class="" id="enabled" data-inverse="true" type="checkbox" name="enabled" value="1" {if $ddw->enabled eq "1"}checked{/if}>
				*}
				<span class="switch prestashop-switch fixed-width-lg">
					<input type="radio" name="enabled" id="enabled_on" value="1" {if $ddw->enabled eq "1"}checked{/if}>
					<label for="enabled_on">Yes</label>
					<input type="radio" name="enabled" id="enabled_off" value="0"
						   {if $ddw->enabled neq "1"}checked{/if}>
					<label for="enabled_off">No</label>
					<a class="slide-button btn"></a>
				</span>
			</div>
		</div>

		<div class="form-group row">
			<label class="control-label col-lg-2">
				Required?
			</label>

			<div class="col-lg-10">
				{*
				<input data-toggle="switch" class="" id="required" data-inverse="true" type="checkbox" name="required" value="1" {if $ddw->required eq "1"}checked{/if}>
				*}
				<span class="switch prestashop-switch fixed-width-lg">
					<input type="radio" name="required" id="required_on" value="1"
						   {if $ddw->required eq "1"}checked{/if}>
					<label for="required_on">Yes</label>
					<input type="radio" name="required" id="required_off" value="0"
						   {if $ddw->required neq "1"}checked{/if}>
					<label for="required_off">No</label>
					<a class="slide-button btn"></a>
				</span>
			</div>
		</div>

		<div class="form-group row">
			<label class="control-label col-lg-2">
				Min Days?
			</label>
			<div class="col-lg-10">
				<input type="text" name="min_days" id="min_days" value="{$ddw->min_days|escape:'htmlall':'UTF-8'}" style="width:80px;">
			</div>
		</div>

		<div class="form-group row">
			<label class="control-label col-lg-2">
				Max Days?
			</label>

			<div class="col-lg-10">
				<input type="text" name="max_days" id="max_days" value="{$ddw->max_days|escape:'htmlall':'UTF-8'}" style="width:80px;">
			</div>
		</div>

		<div class="form-group row">
			<label class="control-label label-tooltip col-lg-2" data-toggle="tooltip" title="{l s='Timeslot is only visible if this amount of time is available for preparing the order leading up to the time slot' mod='deliverydateswizardpro'}">
				<span class="label-tooltip">Order Preparation time</span>
			</label>

			<div class="col-lg-10">
				<div class="input-group">
					<input type="text" name="timeslots_prep_minutes" id="timeslots_prep_minutes" value="{$ddw->timeslots_prep_minutes|escape:'htmlall':'UTF-8'}" style="width:80px;">
					<span class="input-group-addon" style="width: 7px;0px">minutes</span>
				</div>

			</div>
		</div>


		<div class="form-group row">
			<label class="control-label col-lg-2">
				Cut off time Enabled?
			</label>

			<div class="col-lg-10">
				{*
				<input data-toggle="switch" class="" id="cutofftime_enabled" data-inverse="true" type="checkbox" name="cutofftime_enabled" value="1" {if $ddw->cutofftime_enabled eq "1"}checked{/if}>
				*}
				<span class="switch prestashop-switch fixed-width-lg">
					<input type="radio" name="cutofftime_enabled" id="cutofftime_enabled_on" value="1"
						   {if $ddw->cutofftime_enabled eq "1"}checked{/if}>
					<label for="cutofftime_enabled_on">Yes</label>
					<input type="radio" name="cutofftime_enabled" id="cutofftime_enabled_off" value="0"
						   {if $ddw->cutofftime_enabled neq "1"}checked{/if}>
					<label for="cutofftime_enabled_off">No</label>
					<a class="slide-button btn"></a>
				</span>
			</div>
		</div>

		<div class="form-group row">
			<label class="control-label col-lg-2">
			</label>
			<div class="cutofftime-panel col-lg-10">
				<div class="input-group">
					<span class="input-group-addon" style="width:100px;">Cut off Hours</span>
					<select name="cutofftime_hours" id="cutofftime_hours" style="width:80px">
						{foreach from=$hour_values item=hour}
							<option value="{"%02d"|sprintf:$hour|escape:'htmlall':'UTF-8'}" {if $hour eq $ddw->cutofftime_hours}selected="selected"{/if}>{"%02d"|sprintf:$hour|escape:'htmlall':'UTF-8'}</option>
						{/foreach}
					</select>
				</div>
				<div class="input-group">
					<span class="input-group-addon" style="width:100px">Cut off Min</span>
					<select name="cutofftime_minutes" id="cutofftime_minutes" style="width:80px">
						{foreach from=$min_values item=min}
							<option value="{"%02d"|sprintf:$min|escape:'htmlall':'UTF-8'}" {if $min eq $ddw->cutofftime_minutes}selected="selected"{/if}>{"%02d"|sprintf:$min|escape:'htmlall':'UTF-8'}</option>
						{/foreach}
					</select>
				</div>
			</div>
		</div>

		<div class="form-group row">
			<label class="control-label col-lg-3">
				Display ETA on product page (when product exceptions apply)?
			</label>
			<div class="col-lg-9 ">
				{*
				<input data-toggle="switch" class="" id="product_eta_display_enabled" data-inverse="true" type="checkbox" name="product_eta_display_enabled" value="1" {if $ddw->product_eta_display_enabled eq "1"}checked{/if}>
				*}
				<span class="switch prestashop-switch fixed-width-lg">
					<input type="radio" name="product_eta_display_enabled" id="product_eta_display_enabled_on" value="1"
						   {if $ddw->product_eta_display_enabled eq "1"}checked{/if}>
					<label for="product_eta_display_enabled_on">Yes</label>
					<input type="radio" name="product_eta_display_enabled" id="product_eta_display_enabled_off"
						   value="0" {if $ddw->product_eta_display_enabled neq "1"}checked{/if}>
					<label for="product_eta_display_enabled_off">No</label>
					<a class="slide-button btn"></a>
				</span>
			</div>
		</div>

		<div class="form-group hide">
			<input type="hidden" name="id_ddw" id="id_ddw" value="{$ddw->id|escape:'htmlall':'UTF-8'}">
			<input type="hidden" name="id_carrier" id="id_carrier" value="{$id_carrier|escape:'htmlall':'UTF-8'}">
		</div>
	</div>
	<!-- /.form-wrapper -->

	<div class="row">
		<div class="col-xs-12">
			<button type="button" id="ddw-general-save" class="btn btn-primary btn-lg">Save</button>
		</div>
	</div>
</div>