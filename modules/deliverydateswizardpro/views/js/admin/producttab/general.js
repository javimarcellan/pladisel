/*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author Musaffar Patel <musaffar.patel@gmail.com>
*  @copyright  2015-2017 Musaffar
*  @version  Release: $Revision$
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Property of Musaffar Patel
*/

DDWProductGeneral = function(wrapper) {
	var self = this;
	self.wrapper = wrapper;
	self.$wrapper = $(wrapper);
	self.id_product = $("#id_product").val();

	/* Methods */

	self.resetForm = function() {
		self.$wrapper.find("input[name='id_scope']").val('');
		self.$wrapper.find("input[name='min_days']").val('');
		self.$wrapper.find("input[name='max_days']").val('');
		$("#product_cutofftime_enabled_off").trigger('click');
		self.$wrapper.find("select#cutofftime_hours").val('');
		self.$wrapper.find("select#cutofftime_minutes").val('');
	};

	self.populateForm = function(scope) {
		self.$wrapper.find("input[name='id_scope']").val(scope.id_scope);
		self.$wrapper.find("input[name='min_days']").val(scope.min_days);
		self.$wrapper.find("input[name='max_days']").val(scope.max_days);

		if (scope.cutofftime_enabled == 1)
			$("#product_cutofftime_enabled_on").trigger('click');
		else
			$("#product_cutofftime_enabled_off").trigger('click');

		self.$wrapper.find("select#cutofftime_hours").val(scope.cutofftime_hours);
		self.$wrapper.find("select#cutofftime_minutes").val(scope.cutofftime_minutes);
	};

	self.loadCarrierProductOptions = function(id_product, id_carrier) {
		MPTools.waitStart();

		$.ajax({
			type: 'POST',
			url: module_ajax_url_ddw + '?route=ddwadmintabgeneral&action=loadoptions',
			async: true,
			cache: false,
			dataType: "json",
			data: {
				'id_product' : id_product,
				'id_carrier' : id_carrier
			},
			success: function (jsonData) {
				if (jsonData == '' || jsonData == null)
					self.resetForm();
				else {
					self.populateForm(jsonData);
				}
				MPTools.waitEnd();
			}
		});
	};

	self.processForm = function() {

		MPTools.waitStart();
		$.ajax({
			type: 'POST',
			url: module_ajax_url_ddw + '?route=ddwadmintabgeneral&action=processform',
			async: true,
			cache: false,
			//dataType: "json",
			data: self.$wrapper.find(" :input").serialize(),
			success: function (jsonData) {
				console.log('done');
				console.log(jsonData);
				MPTools.waitEnd();
			}
		});

	};

	self.init = function() {
		self.loadCarrierProductOptions(self.id_product, 0);
	};
	self.init();

	/* Events */

	$("body").on("change", self.wrapper + " select.carrier-list", function () {
		self.loadCarrierProductOptions(self.id_product, $(this).val());
	});

	$("body").on("click", self.wrapper + " #ddw-general-save", function () {
		self.processForm();
	});

};