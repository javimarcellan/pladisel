/*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author Musaffar Patel <musaffar.patel@gmail.com>
*  @copyright  2015-2017 Musaffar
*  @version  Release: $Revision$
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Property of Musaffar Patel
*/

DDWWeekdays = function(wrapper) {
	var self = this;
	self.$wrapper = $(wrapper);
	self.wrapper = wrapper;

    self.ddw_datepopup = [];

	/* Initial render for tab  content */
	self.render = function() {
		self.$wrapper.load(
			module_config_url + '&route=ddwadminconfigweekdays&action=renderlist',
			{
				'id_carrier': smarty.id_carrier,
				'is_ajax': 1
			},
			function () {
				//prestaShopUiKit.init();
			}
		);
	};

	self.init = function() {
		self.ddw_datepopup = new DDWDatePopup(wrapper, smarty.id_carrier, 'mp-popup-editweekday', 'ddwadminconfigweekdays');
		self.ddw_datepopup.controller = self;
		self.render();
	};
	self.init();

	/* Events */
	$("body").on("click", self.wrapper + " a.ddw-edit-weekday", function () {
		self.ddw_datepopup.openAddForm($(this).attr("data-id_weekday"), smarty.id_carrier);
		return false;
	});

};