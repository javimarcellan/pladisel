/*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author Musaffar Patel <musaffar.patel@gmail.com>
*  @copyright  2015-2017 Musaffar
*  @version  Release: $Revision$
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Property of Musaffar Patel
*/

DDWSpecificDates = function(wrapper) {
    var self = this;
   	self.$wrapper = $(wrapper);
	self.wrapper = wrapper;

    self.ddw_datepopup = [];

    self.renderList = function () {
        self.$wrapper.load(
            module_config_url + '&route=ddwadminconfigspecificdates&action=renderlist #' + 'ddw-specificdates' + ' > *',
            {
                'id_carrier': smarty.id_carrier
            },
            function () {
            }
        );
    };

    /**
     * Delete the specific date completely
     */
    self.processDelete = function(id_ddw_specificdate) {
		MPTools.waitStart();
		$.ajax({
			type: 'POST',
			url: module_config_url + '&route=ddwadminconfigspecificdates&action=processdelete',
			async: true,
			cache: false,
			//dataType: "json",
			data: {
				'id_ddw_specificdate' : id_ddw_specificdate
			},
			success: function (jsonData) {
				self.renderList();
				MPTools.waitEnd();
			}
		});
    };

    self.init = function() {
        self.ddw_datepopup = new DDWDatePopup(wrapper, smarty.id_carrier, 'mp-popup-editspecificdate', 'ddwadminconfigspecificdates');
        self.ddw_datepopup.controller = self;
        self.ddw_datepopup.onClose = function(controller) {
            controller.renderList();
        };
        self.renderList();
	};
	self.init();

    /**
     * Events
     */

    /**
     * Add new specific date button on click
     */
    $("body").on("click", self.wrapper + " #ddw-specificdate-add", function () {
        self.ddw_datepopup.openAddForm(-1, smarty.id_carrier);
        return false;
    });

    /**
     * Edit Specific Date Icon Click
     */
	$("body").on("click", self.wrapper + " .ddw-specificdate-edit", function () {
        self.ddw_datepopup.openAddForm($(this).attr("data-id"), smarty.id_carrier);
		return false;
	});

    /**
     * Delete Specific Date Icon Click
     */
	$("body").on("click", self.wrapper + " .ddw-specificdate-delete", function () {
        self.processDelete($(this).attr("data-id"));
		return false;
	});

};