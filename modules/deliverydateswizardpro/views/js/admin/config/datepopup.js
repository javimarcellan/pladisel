/*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author Musaffar Patel <musaffar.patel@gmail.com>
*  @copyright  2015-2017 Musaffar
*  @version  Release: $Revision$
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Property of Musaffar Patel
*/

DDWDatePopup = function(wrapper, id_carrier, popupEditFormId, route) {
	var self = this;
	self.$wrapper = $(wrapper);
	self.wrapper = wrapper;

	self.popupEditFormId = popupEditFormId;
	self.timeslotsPanelID = 'panel2';
	self.popup; //instance of modal popup
    self.controller = []; //parent controller
    self.onClose = []; //custom function for parent controller

    /**
     * Open the popup form and load the seeings
     * @param id (id_weekday or id_specificdate)
     * @param id_carrier
     * @returns {boolean}
     */
	self.openAddForm = function (id, id_carrier) {
		MPTools.waitStart();
		var url = module_config_url + '&route='+route+'&action=rendereditform&id='+id+'&id_carrier=' + id_carrier;
		self.popup = new MPPopup(self.popupEditFormId, self.wrapper);
		self.popup.showContent(url, null, self.initTimeslotsPanel);
		return false;
	};

	/**
	 * Ensure a time HH:MM contains a leading zero if required (eg 9:00)
	 * @param time
	 */
	self.addLeadingZeroToTime = function (time) {
		var parts = time.split(':');

		if (parts.length == 2) {
			if (parts[0].length == 1) {
				return '0' + time;
			}
		}
		return time;
	};

	/*
	 *  Add new timeslot to the table
	 */
	self.addNewTimeslot = function () {
		var $timeslotsPanel = $("#" + self.timeslotsPanelID);
		var time_start = $("#" + self.timeslotsPanelID).find("input#time_start").val();
		var time_end = $("#" + self.timeslotsPanelID).find("input#time_end").val();
		var order_limit = $("#" + self.timeslotsPanelID).find("input#order_limit").val();

		time_start = self.addLeadingZeroToTime(time_start);
		time_end = self.addLeadingZeroToTime(time_end);

		var $cloned = $timeslotsPanel.find("#timeslotsTable tr.cloneable").clone();
		$cloned.removeClass("cloneable");
		$cloned.removeClass("hidden");
		$cloned.find("td.time_start").html(time_start);
		$cloned.find("td.time_end").html(time_end);
		$cloned.find("td.order_limit").html(order_limit);
		$cloned.appendTo($timeslotsPanel.find("#timeslotsTable tbody"));
		$timeslotsPanel.find("#timeslotsTable").tableDnDUpdate();
	};

	/**
	 * Make the timeslot table draggable
 	 */
	self.initTimeslotsPanel = function () {
		MPTools.waitEnd();
		$("#timeslotsTable").tableDnD({
			dragHandle: 'dragHandle',
			onDragClass: 'myDragClass',
			onDragStart: function (table, row) {
			},
			onDrop: function (table, row) {
			}
		});
	};

	/**
	 * Process the change of time slot enabled or disabled state
	 * @param sender
	 * @param new_state
	 */
	self.toggleTimesloteStatus = function ($sender) {
		if ($sender.attr("data-state") == '1') {
			$sender.attr("data-state", '0');
			$sender.removeClass("icon-check");
			$sender.addClass("icon-times");
		} else {
			$sender.attr("data-state", '1');
			$sender.removeClass("icon-times");
			$sender.addClass("icon-check");
		}
	};


	/*
	 *  Serialise timeslots displayed in the timeslots table
	 */
	self.serialiseTimeslots = function () {
		var timeslots = new Array();
		$("#" + self.timeslotsPanelID + ' #timeslotsTable tbody tr').each(function () {
			if (!$(this).hasClass('cloneable')) {
				var timeslot = {
					'time_start': $(this).find("td.time_start").html(),
					'time_end': $(this).find("td.time_end").html(),
					'order_limit': $(this).find("td.order_limit").html(),
					'enabled': $(this).find("td.state i").attr("data-state")
				};
				timeslots.push(timeslot);
			}
		});
		$("#" + self.popupEditFormId + " input#ddw_timeslots").val(JSON.stringify(timeslots));
		return JSON.stringify(timeslots);
	};


	/**#
     * Save all the settings
     */
    self.processEditForm = function() {
		self.serialiseTimeslots();
		MPTools.waitStart();
		$.ajax({
			type: 'POST',
			url: module_config_url + '&route='+route+'&action=processeditform',
			async: true,
			cache: false,
			//dataType: "json",
			data: $("#" + self.popupEditFormId + " :input").serialize(),
			success: function (jsonData) {
				self.popup.close();
				MPTools.waitEnd();
                if (typeof self.onClose == 'function') {
                    self.onClose(self.controller);
                }
			}
		});
    };

	self.init = function() {
	};
	self.init();

	/** Events **/

    /**
     * Save all settings
     */
    $("body").on("click", "#" + self.popupEditFormId + " #ddw-popup-save", function () {
        self.processEditForm();
        return false;
    });

	/**
	 * Popup Cancel button on click
	 */
	$("body").on("click", "#" + self.popupEditFormId + " #ddw-weekday-edit-cancel", function () {
		self.popup.close();
		return false;
	});


	/**
     * Edit Timeslots button click
      */
    $("body").on("click", "#" + self.popupEditFormId + " #edit-timeslots", function () {
		self.popup.showSubPanel('panel2');
		return false;
	});

	/**
	 * Events for Panel 2
	 */

	/**
	 * Add new timeslot
 	 */
	$("body").on("click", '#' + self.popupEditFormId + " #" + self.timeslotsPanelID + " #add-timeslot", function () {
		self.addNewTimeslot();
		return false;
	});

	/**
	 * Toggle Enabled / Disabled Icon Click
	 */
	$("body").on("click", '#' + self.popupEditFormId + " #" + self.timeslotsPanelID + " .toggle-state", function () {
		self.toggleTimesloteStatus($(this));
		return false;
	});

	/**
	 * Delete a timeslot
	 */
	$("body").on("click", '#' + self.popupEditFormId + " #" + self.timeslotsPanelID + " a.ddw-timeslot-delete", function () {
		$(this).parents("tr").remove();
	});

	/**
	 * Save timeslot button on click
	 */
	$("body").on("click", '#' + self.popupEditFormId + " #" + self.timeslotsPanelID + " #ddw-timeslots-done", function () {
		self.serialiseTimeslots();
		self.popup.hideSubPanel('panel2');
		return false;
	});

	/**
	 * Cancel timeslot panel on click
	 */
	$("body").on("click", '#' + self.popupEditFormId + " #" + self.timeslotsPanelID + " #ddw-timeslots-cancel", function () {
		self.popup.hideSubPanel('panel2');
		return false;
	});

};