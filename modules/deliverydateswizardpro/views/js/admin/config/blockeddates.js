/*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author Musaffar Patel <musaffar.patel@gmail.com>
*  @copyright  2015-2017 Musaffar
*  @version  Release: $Revision$
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Property of Musaffar Patel
*/

DDWBlockedDates = function (wrapper) {
	var self = this;
	self.popup; //instance of modal popup
	self.$wrapper = $(wrapper);
	self.wrapper = wrapper;
	self.popupFormId = 'mp-popup-addblockeddate';

	self.openAddForm = function (id_carrier) {
		MPTools.waitStart();
		var url = module_config_url + '&route=ddwadminconfigblockeddates&action=renderaddform&id_carrier=' + id_carrier;

		self.popup = new MPPopup(self.popupFormId, self.wrapper);
		self.popup.showContent(url, null, null);
		return false;
	};

	self.processEditForm = function() {
		MPTools.waitStart();
		$.ajax({
			type: 'POST',
			url: module_config_url + '&route=ddwadminconfigblockeddates&action=processeditform',
			async: true,
			cache: false,
			dataType: "json",
			data: $("#" + self.popupFormId + " :input").serialize(),
			success: function (jsonData) {
				self.popup.close();
				self.renderList();
				MPTools.waitEnd();
			}
		});
	};

	self.delete = function(id) {
		MPTools.waitStart();
		$.ajax({
			type: 'POST',
			url: module_config_url + '&route=ddwadminconfigblockeddates&action=processdelete',
			async: true,
			cache: false,
			dataType: "json",
			data: {
				'id' : id
			},
			success: function (jsonData) {
				self.renderList();
				MPTools.waitEnd();
			}
		});
	};

	self.renderList = function () {
		self.$wrapper.load(
				module_config_url + '&route=ddwadminconfigblockeddates&action=renderlist #' + 'ddw-blockeddates' + ' > *',
			{
				'id_carrier': smarty.id_carrier,
				'is_ajax': 1
			},
			function () {
			}
		);
	};

	self.init = function () {
		self.renderList();
	};
	self.init();

	$("body").on("click", self.wrapper + " #ddw-blockeddate-add", function () {
		self.openAddForm(smarty.id_carrier);
		return false;
	});

	$("body").on("click", self.wrapper + " .ddw-blockeddate-delete", function () {
		self.delete($(this).attr("data-id"));
		return false;
	});

	$("body").on("click", "#" + self.popupFormId + " #ddw-blockeddate-save", function () {
		self.processEditForm();
		return false;
	});
};

