/*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author Musaffar Patel <musaffar.patel@gmail.com>
*  @copyright  2015-2017 Musaffar
*  @version  Release: $Revision$
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Property of Musaffar Patel
*/

DDWProducts = function (wrapper) {
	var self = this;

	self.popup; //instance of modal popup
	self.$wrapper = $(wrapper);
	self.wrapper = wrapper;
	self.popupFormId = 'mp-popup-addproduct';

	/* Main methods */

	self.renderList = function () {
		self.$wrapper.load(
				module_config_url + '&route=ddwadminconfigproducts&action=renderlist #' + 'ddw-products' + ' > *',
				{
					'id_carrier': smarty.id_carrier,
					'is_ajax': 1
				},
				function () {
				}
		);
	};

	self.openAddForm = function (id_carrier, id_scope) {
		MPTools.waitStart();
		var url = module_config_url + '&route=ddwadminconfigproducts&action=renderaddform&id_carrier=' + id_carrier + '&id_scope=' + id_scope;
		self.popup = new MPPopup(self.popupFormId, self.wrapper);
		self.popup.showContent(url, null, null);
		return false;
	};

	self.processDelete = function(id_carrier, id_scope) {
		MPTools.waitStart();
		$.ajax({
			type: 'POST',
			url: module_config_url + '&route=ddwadminconfigproducts&action=processdelete',
			async: true,
			cache: false,
			dataType: "json",
			data: {
				'id_carrier': id_carrier,
				'id_scope': id_scope,
			},
			success: function (jsonData) {
				MPTools.waitEnd();
				self.renderList();
				MPTools.waitEnd();
			}
		});
	};

	self.init = function () {
		self.renderList();
	};
	self.init();


	/* Main Events */

	$("body").on("click", self.wrapper + " #ddw-product-add", function () {
		self.openAddForm(smarty.id_carrier);
		return false;
	});

	$("body").on("click", self.wrapper + " .ddw-scope-edit", function () {
		self.openAddForm(smarty.id_carrier, $(this).attr('data-id_scope'));
		return false;
	});

	$("body").on("click", self.wrapper + " .ddw-scope-delete", function () {
		self.processDelete(smarty.id_carrier, $(this).attr('data-id_scope'));
		return false;
	});

	/* Popup form methods */

	self.showResultsList = function() {
		$("#" + self.popupFormId).find("#product-search-results").slideDown();
	};

	self.hideResultsList = function() {
		$("#" + self.popupFormId).find("#product-search-results").slideUp();
	};

	self.clearResultsList = function() {
		$("#" + self.popupFormId).find("#search-results-table tbody tr.result-item").remove();
	};

	self.addToResultList = function(id, name, scope) {
		$form = $("#" + self.popupFormId);
		$results_table = $form.find("#search-results-table");

		var $cloned = $results_table.find("tr.cloneable").clone();
		$cloned.removeClass("cloneable");
		$cloned.removeClass("hidden");
		$cloned.addClass("result-item");
		$cloned.find("td.scope").html(scope);
		$cloned.find("td.name").html(name);
		$cloned.attr("data-id", id);
		$cloned.attr("data-name", name);
		$cloned.attr("data-scope", scope);
		$cloned.appendTo($results_table.find("tbody"));
	};

	self.onResultSelect = function(id, scope, name) {
		$form = $("#" + self.popupFormId);
		$form.find("input#id").val(id);
		$form.find("input#scope").val(scope);
		$('#' + self.popupFormId + " input#product_search").val(name);
	};

	self.popupProcessSearch = function(search_string, scope) {
		$.ajax({
			type: 'POST',
			url: module_config_url + '&route=ddwadminconfigproducts&action=processsearch',
			async: true,
			cache: false,
			dataType: "json",
			data: {
				'search_string': search_string,
				'scope' : scope
			},
			success: function (jsonData) {
				self.clearResultsList();
				self.showResultsList();
				for (var x=0; x<= jsonData.length-1; x++) {
					self.addToResultList(jsonData[x].id, jsonData[x].name, scope)
				}
				return false;
			}
		});
	};

	self.processForm = function () {
		MPTools.waitStart();
		$.ajax({
			type: 'POST',
			url: module_config_url + '&route=ddwadminconfigproducts&action=processeditform',
			async: true,
			cache: false,
			//dataType: "json",
			data: $("#" + self.popupFormId + " :input").serialize(),
			success: function (jsonData) {
				MPTools.waitEnd();
				self.popup.close();
				self.renderList();
			}
		});
	};

	/* Popup form events */
	$("body").on("keyup", '#' + self.popupFormId + " #product_search", function () {
		self.popupProcessSearch($(this).val(), $('#' + self.popupFormId + " #scope").val())
	});

	$("body").on("click", '#' + self.popupFormId + " tr.result-item", function () {
		self.onResultSelect($(this).attr("data-id"), $(this).attr("data-scope"), $(this).attr("data-name"));
		self.hideResultsList();
	});

	$("body").on("click", "#" + self.popupFormId + " #ddw-products-save", function () {
		self.processForm();
		return false;
	});


};

