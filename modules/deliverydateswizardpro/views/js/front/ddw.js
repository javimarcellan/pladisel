/*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author Musaffar Patel <musaffar.patel@gmail.com>
*  @copyright  2015-2017 Musaffar
*  @version  Release: $Revision$
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Property of Musaffar Patel
*/

DDWFrontEnd = function(controller) {
    var self = this;
    self.controller = controller;
    self.supercheckout = false;
	self.one_page_checkout = true;
    self.amazoncheckout = false;

	if (self.supercheckout) {
		self.carrierForm = "form#velsof_supercheckout_form";
		self.$carrierForm = $("form#velsof_supercheckout_form");
	}
	else {
		self.carrierForm = "form#js-delivery";
		self.$carrierForm = $("form#js-delivery");
	}

    self.module_folder = 'deliverydateswizardpro';
    self.loadedForBackButton = false; //flag to only run the related function once
	self.$widget = $("#delivery-date-panel");
    self.$calendar = $("#ddw_calendar");
    self.$timeslots = $("#ddw_timeslots");
    self.$summary = $("#ddw-summary");
    self.$input_ddw_order_date = $("input#ddw_order_date");
    self.$input_ddw_order_time = $("input#ddw_order_time");
    self.required = 0;
    self.id_lang = id_lang;
    self.timeslots_realcount = 0;  //number of timeslots present in current weekday (unfiltered)
	self.default_select = true;    // flag to indicate if first day and timeslot should be selected by default
	self.arrBlockedDates = []; //of DDWCalendarBlockedDate ;

    self.updateDDWCart = function() {
        $.ajax({
            type: 'POST',
            url: baseDir + 'modules/'+self.module_folder+'/ajax.php?action=update_ddw_cart&rand=' + new Date().getTime(),
            async: true,
            cache: false,
            data : {
                ddw_date : self.$input_ddw_order_date.val(),
                ddw_time : self.$input_ddw_order_time.val()
            },
            success: function(jsonData) {
                window.ddw_order_date_cache = $("input#ddw_order_date").val();
                window.ddw_order_time_cache = $("input#ddw_order_time").val();
            }
        });
    };

    self.refreshTimeSlots = function(timeslots_collection) {
        self.$timeslots.html('');
        $.each(timeslots_collection, function (i, timeslot) {
            if (typeof timeslot.time_slots[self.id_lang] !== 'undefined' && timeslot.time_slots[self.id_lang] != '')
                self.$timeslots.append('<div class=""><input type="radio" name="chk_timeslot" class="chk_timeslot" value="' + timeslot.time_slots[self.id_lang] + '"><label>' + timeslot.time_slots[self.id_lang] + '</label></div>')
        });
    };

    self.isDateBlocked = function(date_yyyymmdd, arr_blocked_dates) {
        var x = 0;
        var blocked = false;
        for (x=0; x <= arr_blocked_dates.length-1; x++) {
            if (arr_blocked_dates[x] == date_yyyymmdd) {
                blocked = true;
                break;
            }
        }
        return blocked;
    };

    self.getBlockedDates = function(id_carrier) {        
        $.ajax({
            type: 'POST',
            url: baseDir + 'modules/'+self.module_folder+'/ajax.php?action=get_blocked_dates&rand=' + new Date().getTime(),
            async: true,
            cache: false,
            data : {
                id_carrier : id_carrier
            },
            dataType : "json",
            complete: function(d) {
            },
            success: function(jsonData) {
                if (self.$calendar.hasClass("hasDatepicker"))
                    self.$calendar.datepicker("destroy");

                if (typeof jsonData.required !== "undefined")
                    self.required = jsonData.required;
                else
                    self.required = 0;

                if (typeof jsonData.enabled !== "undefined")
                    if (jsonData.enabled == false) {
                        $("input[name='ddw_order_date']").val('');
                        $("input[name='ddw_order_time']").val('');
                        $("#delivery-date-panel").fadeOut(100);
                        return false;
                    }
                    else {
                        $("#delivery-date-panel").fadeIn(100);
                    }

                self.arrBlockedDates = [];
                $.each(jsonData.calendar_blocked_dates, function (i, blocked_date) {
                    self.arrBlockedDates.push(blocked_date.date);
                });

                if (!self.isDateBlocked(jsonData.min_date, self.arrBlockedDates)) {
                    self.reload_time_slots(id_carrier, jsonData.defaults.calendar_default_year + '-' + jsonData.defaults.calendar_default_month + '-' + jsonData.defaults.calendar_default_day);
                }

                self.loadForBackButton();

                self.$calendar.datepicker({
                    dateFormat: 'yy-mm-dd',
                    minDate: jsonData.min_date,
                    maxDate: jsonData.max_date,
                    defaultDate: jsonData.defaults.calendar_default_year+'-'+jsonData.defaults.calendar_default_month+'-01',
                    beforeShowDay: function(date) {
                        var dateString = jQuery.datepicker.formatDate('yy-mm-dd', date);
                        return [self.arrBlockedDates.indexOf(dateString) == -1];
                    },
                    onSelect: function(date) {
                        var dateFormatted = date.split("-").reverse().join("-");
                        $("#ddw_text").html(dateFormatted);
                        self.$input_ddw_order_date.val(date);
                        self.$input_ddw_order_date.trigger("change");
                        self.reload_time_slots(self.getIDCarrier(), date);
                        self.updateSummary();
                        if (self.one_page_checkout || self.amazoncheckout == true) self.updateDDWCart();
                    },
                    // Prevent checkout step from being hidden due core.js conflict
                    onChangeMonthYear: function(a,b,c) {
                        if (typeof event !== 'undefined') {
                            // Prevent current step being closed in Chrome / IE / Safari
                            event.stopPropagation();
                        } else {
                            // Prevent current step being closed in Firefox
                            setTimeout(function() {
                                $("#checkout-delivery-step a.ui-datepicker-next").click(function (event) {
                                    event.stopPropagation();
                                });
                                $("#checkout-delivery-step a.ui-datepicker-prev").click(function (event) {
                                    event.stopPropagation();
                                });
                            }, 50);
                        }
                    }
                });

                // Prevent current step being closed in Firefox
                $("#checkout-delivery-step a.ui-datepicker-next").click(function (event) {
                    event.stopPropagation();
                });

                $("#checkout-delivery-step a.ui-datepicker-prev").click(function (event) {
                    event.stopPropagation();
                });
            }
        });

    };

    self.reloadCalender = function(id_carrier) {
        self.getBlockedDates(id_carrier);
    };

    self.setTimeslotsUnfilteredCount = function (count) {
        self.timeslots_realcount = count;
    };

    self.getTimeslotsCount = function() {
        return (self.$timeslots.find("input[type='radio']").length);
    };

    self.getTimeslotsUnfilteredCount = function () {
        return self.timeslots_realcount;
    };

    /**
     * Determine if the current step is the shipping step in One Page Checkout
     */
    self.isShippingStep = function() {
        if (($('#checkout-delivery-step')).hasClass('js-current-step')) {
            return true;
        } else {
            return false;
        }
    };

    self.reload_time_slots = function(id_carrier, date_selected) {
        self.loadState(self.$timeslots, 'on');
        self.$input_ddw_order_time.val('');

        $.ajax({
            type: 'POST',
            url: baseDir + 'modules/' + self.module_folder + '/ajax.php?action=get_timeslots&rand=' + new Date().getTime(),
            async: true,
            cache: false,
            data: {
                id_carrier: id_carrier,
                date: date_selected
            },
            dataType: "json",
            success: function (jsonData) {

				if (typeof date_selected !== 'undefined') {
                    var date_enabled = [self.arrBlockedDates.indexOf(date_selected) == -1];
                    if (!date_enabled) {
                        self.$timeslots.html('');
                        return false;
                    }
                } else {
                    self.$timeslots.html('');
                    return false;
                }

                self.loadState(self.$timeslots, 'off');
                self.$timeslots.html(jsonData.html);
                self.setTimeslotsUnfilteredCount(jsonData.timeslot_realcount);

                if (self.isShippingStep()) {
                    if (self.getTimeslotsCount() == 1 || self.default_select) {
                        $sender = self.$timeslots.find('.timeslot').first();
                        self.selectTimeslot($(this));
                    } else {
                        $("input[name='DDW_time_slot']").val("");
                        self.$input_ddw_order_time.val("");
                        $('input.ddw_time_slot').prop('checked', false);
                        self.updateSummary();
                    }
                }
            }
        });
    };

    self.reload_translations = function(id_carrier) {
		//todo: uncomment and debug js error
        //$(".ddw_texts").hide();
        //$(".ddw_texts-"+id_carrier).show();
    };

    self.blockDates = function(jsonData) {
        for (var d = new Date(startDate); d <= new Date(endDate); d.setDate(d.getDate() + 1)) {
            dateRange.push($.datepicker.formatDate('yy-mm-dd', d));
        }
    };

    self.getIDCarrier = function() {
		if (self.supercheckout)
        	var id_carrier = $("#shipping-method input:radio:checked").val();
		else
			var id_carrier = $(".delivery-options-list .custom-radio input:radio:checked").val();
        return id_carrier;
    };

    self.updateSummary = function() {
		//todo: update me
        var parts = self.$input_ddw_order_date.val().split(' ');
        var parts = parts[0].split('-');
        var year =+ parts[0];
        var month =+ parts[1];
        var day =+parts[2];
		
		if (!isNaN(day) && day > 0)
	        self.$summary.find(".delivery-date").html(day+' ' + ddw_months[month] + ' ' + year);
		else
            self.$summary.find(".delivery-date").html(ddw_texts.summary.select_date);

        var selected_time = self.$timeslots.find('.timeslot.selected .time').html();
        if (selected_time != '' && typeof(selected_time) !== 'undefined')
            self.$summary.find(".delivery-time").html(self.$timeslots.find('.timeslot.selected .time').html());
        else {
            if (self.getTimeslotsCount() > 0)
                self.$summary.find(".delivery-time").html(ddw_texts.summary.select_time);
            else
                self.$summary.find(".delivery-time").html('');
        }
    };

    self.loadState = function($element, state) {
        if (state == 'on')
            $element.append('<i class="icon icon-refresh icon-spin icon-loading"></i>')
        else
            $element.find('.icon-loading').remove();
    };

    /**
     * Select the timeslot
     * @param $sender
     * @returns {boolean}
     */
    self.selectTimeslot = function($sender) {
        if ($sender.hasClass('unavailable')) return false;
        self.$timeslots.find(".timeslot").removeClass("selected");
        $sender.addClass('selected');

        /* if timeslot selected without a date being seected, automatically select active calendar date */
        if (self.$input_ddw_order_date.val() == '') {
            var current_datepicker_date = self.$calendar.datepicker({dateFormat: 'yy-mm-dd'}).val();
            var dateFormatted = current_datepicker_date.split("-").reverse().join("-");
            $("#ddw_text").html(current_datepicker_date);
            self.$input_ddw_order_date.val(current_datepicker_date);
        }

        $("input[name='ddw_order_time']").val($sender.find('input[type="radio"]').val());
        self.updateSummary();
        if (self.one_page_checkout || self.amazoncheckout == true) self.updateDDWCart();
    };

    self.$timeslots.on('click', '.timeslot', function() {
        self.selectTimeslot($(this));
    });

    self.checkNextStep = function() {
        var perror = false;

        /* check timeslot selected if applicable*/
        if (self.getTimeslotsUnfilteredCount() > 0)
            if (self.$input_ddw_order_time.val() == '' || self.$input_ddw_order_time.val() == ' - ') perror = true;

        if (self.$input_ddw_order_date.val() == '' || self.$input_ddw_order_date.val() == '0000-00-00 00:00:00') perror = true;		

        if ($("#ddw_calendar").html() == "") perror = false; //no calendar displayed for selected carrier, proceed checkout
        if (self.required == 0) perror = false;

        return !perror;
    };

    self.showRequiredError = function() {
		self.$widget.find(".ddw-error").slideDown();
    };

    self.nextStep = function(e) {
        if (!self.checkNextStep()) {
            e.preventDefault();
            self.showRequiredError();
            return false;
        } else
            return true;
    };

    self.displayDate = function() {
		//@todo: update me
		return false;
        $("span#ddw_text").html(self.$input_ddw_order_date.val().replace(' 00:00:00', ''));
    };

    self.displayTime = function() {
        $("input[name='chk_timeslot'][value='"+self.$input_ddw_order_time.val()+"']").prop('checked', true);
    };
	
    self.selectDate = function(date) {
        self.$calendar.datepicker('setDate', date);
    };

    self.selectTime = function(time) {
        $("#ddw-timeslots .time:contains('"+time+"')").parents(".timeslot").trigger("click");
    };	

    // if the back button was pressed, reapply the date and time
    self.loadForBackButton = function() {
        if (self.one_page_checkout) {
            self.loadedForBackButton = true;
            return false;
        }
        if (self.loadedForBackButton) return false;

		$.ajax({
			type: 'POST',
			url: baseDir + 'modules/deliverydateswizardpro/ajax.php?action=get_last_ddw_cart&rand=' + new Date().getTime(),
			async: true,
			cache: false,
			dataType : "json",
			success: function(jsonData) {
				if (typeof jsonData.ddw_order_date !== 'undefined') {
                    self.$input_ddw_order_date.val(jsonData.ddw_order_date);
                    self.selectDate(jsonData.ddw_order_date);
                }

				if (typeof jsonData.ddw_order_time !== 'undefined') {
                    self.$input_ddw_order_time.val(jsonData.ddw_order_time);
                    self.selectTime(jsonData.ddw_order_time);
                }

				if (self.$input_ddw_order_date.val() != '')
					self.displayDate();

				if (self.$input_ddw_order_time.val() != '')
					self.displayTime();
				self.loadedForBackButton = true;
			}
		});
    };

	/**
	 * Adds the input fields to carrier form so the DDW date and time are available to actionCarrierProcess hook
	 */
	self.addFormFieldsToCarrierForm = function() {
		self.$carrierForm.append('<input id="ddw_order_date" name="ddw_order_date" type="hidden" value="">');
		self.$carrierForm.append('<input id="ddw_order_time" name="ddw_order_time" type="hidden" value="">');
		self.$input_ddw_order_date = $("input#ddw_order_date");
		self.$input_ddw_order_time = $("input#ddw_order_time");
	};

    self.init = function() {
		self.addFormFieldsToCarrierForm();

        if (self.controller == 'amzpayments') {
            self.amazoncheckout = true;
        }

        if (controller_name == 'order-opc') self.one_page_checkout = true;

		if (self.supercheckout) {
			self.one_page_checkout = true;
			id_carrier = $("input.delivery_option_radio:checked").val();
		}
		else {
			id_carrier = $(".delivery-options-list .custom-radio input:radio:checked").val();
		}		

        $('body').off('submit.ddwNextStep');
        $('body').on('submit.ddwNextStep', '#paypal_payment_form', self.nextStep);

        /* Payment form on submit */
        $('body').on('submit', '#opc_payment_methods-content form', function (e) {
            if (!self.checkNextStep()) {
                e.preventDefault();
                self.showRequiredError();
                return false;
            } else
                return true;
        });

        self.reloadCalender(id_carrier);
		//self.reload_time_slots(id_carrier);

        if (typeof default_time != "undefined") {
            $("input[name='DDW_time_slot']").val(default_time);
            $('input.ddw_time_slot[value="'+default_time+'"]').prop('checked', true);
        }
    };
    self.init();

	/**
     * Carrier option changed, reload calendar
     */
    $("input[name='shipping_method'], .delivery-options-list .custom-radio input:radio, input.supercheckout_shipping_option").change(function() {
		var is_deliveryoption_radio = false;
        window.ddw_order_date = '';
        window.ddw_order_time = '';
        $("input[name='ddw_order_date']").val('');
        $("input[name='ddw_order_time']").val('');

		if ($(this).attr("name").indexOf("delivery_option") > -1)
			is_deliveryoption_radio = true;

        if (is_deliveryoption_radio)
            var id_carrier = $(this).val().split(".")[0];   //for one page by Zelarg
        else
            var id_carrier = $(this).attr("id").split(".")[0];

        self.reloadCalender(id_carrier);
		self.reload_time_slots(id_carrier);
		self.reload_translations(id_carrier);
	});

	/**
     * continue button clicked (in shipping method panel), check if customer can proceed to next step
     */
    $("form#js-delivery").on('submit', function() {
        if (!self.checkNextStep()) {
            self.showRequiredError();
            return false;
        } else
            return true;
    })

	/**
	 * Super checkout payment submit
	 */
	$('#supercheckout_confirm_order').unbind('click');
    $("#supercheckout_confirm_order").click(function() {
        if (!self.checkNextStep()) {
            self.showRequiredError();
            return false;
        } else
			placeOrder();
    });

};