<?php
/**
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * You must not modify, adapt or create derivative works of this source code
 *
 * @author    Musaffar Patel
 * @copyright 2016-2017 Musaffar Patel
 * @license   LICENSE.txt
 */

class AdminOrdersController extends AdminOrdersControllerCore
{
	public function __construct()
	{
		parent::__construct();
		$add_select = '(SELECT o.ddw_order_date FROM `'._DB_PREFIX_.'orders` o WHERE o.id_order = a.id_order) as ddw_order_date,
					   (SELECT o.ddw_order_time FROM `'._DB_PREFIX_.'orders` o WHERE o.id_order = a.id_order) as ddw_order_time,';
		$this->_select = str_replace('country_lang.name as cname,', 'country_lang.name as cname,'.$add_select, $this->_select);
		if (_PS_VERSION_ < 1.6)
			$this->_select = str_replace('os.`color`,', 'os.`color`,'.$add_select, $this->_select);
		else
			$this->_select = str_replace('country_lang.name as cname,', 'country_lang.name as cname,'.$add_select, $this->_select);

		$this->fields_list['ddw_order_date'] = array(
			'title' => $this->l('Delivery Date'),
			'align' => 'text-right',
			'type' => 'datetime',
			'filter_key' => 'ddw_order_date',
			'callback' => 'printDDWDate',
			'class' => 'fixed-width-xl',
			'width' => '100'
		);
	}

	public function printDDWDate($id_order, $tr)
	{
		//$return = date('Y-m-d', strtotime($tr['ddw_date'])).' - ';
		$return = Tools::displayDate($tr['ddw_order_date']).' - ';
		if ($tr['ddw_order_time'] != '') $return .= ' ('.$tr['ddw_order_time'].')';
		return $return;
	}

}