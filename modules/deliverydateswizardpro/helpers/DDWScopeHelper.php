<?php
/**
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * You must not modify, adapt or create derivative works of this source code
 *
 * @author    Musaffar Patel
 * @copyright 2016-2017 Musaffar Patel
 * @license   LICENSE.txt
 */

abstract class DDWScopeHelper
{
	public $id_lang;
	protected $id_shop;
	protected $scope = '';

	abstract public function getScopeMatches($search_string);

	abstract public function getName($id);

	public static function getInstance($scope, $id_lang, $id_shop)
	{
		switch ($scope)
		{
			case 'supplier' :
				$scope_instance = new DDWScopeSupplier();
				break;
			case 'category' :
				$scope_instance = new DDWScopeCategory();
				break;

			case 'product' :
				$scope_instance = new DDWScopeProduct();
				break;
		}

		$scope_instance->id_lang = $id_lang;
		$scope_instance->id_shop = $id_shop;
		$scope_instance->scope = $scope;
		return $scope_instance;
	}

	public function getOptions($id_associated, $id_carrier)
	{
		$sql = new DbQuery();
		$sql->select('*');
		$sql->from('ddw_scopes');
		$sql->where('scope LIKE "'.$this->scope.'"');
		$sql->where('id_associated = '.(int)$id_associated);
		$sql->where('id_carrier = '.(int)$id_carrier);

		$row = Db::getInstance()->getRow($sql);
		return $row;
	}

}


class DDWScopeSupplier extends DDWScopeHelper
{
	public function getScopeMatches($search_string)
	{
		$sql = new DbQuery();
		$sql->select('s.name, s.id_supplier AS id');
		$sql->from('supplier', 's');
		$sql->innerJoin('supplier_shop', 'ss', 's.id_supplier = ss.id_supplier AND ss.id_shop = '.(int)$this->id_shop);
		$sql->where('s.name LIKE "%'.pSQL($search_string).'%"');
		$sql->orderBy('s.name');
		return Db::getInstance()->executeS($sql);
	}

	public function getName($id)
	{
		$sql = new DbQuery();
		$sql->select('s.name');
		$sql->from('supplier', 's');
		$sql->where('s.id_supplier = '.(int)$id);
		$row = Db::getInstance()->getRow($sql);
		if (!empty($row['name'])) return $row['name'];
		else return '';
	}

	public function getCartCollection($id_cart, $id_carrier = -1)
	{
		$sql = new DbQuery();
		$sql->select('*');
		$sql->from('ddw_scopes', 's');
		$sql->innerJoin('product_supplier', 'ps', 'ps.id_supplier = s.id_associated');
		$sql->innerJoin('cart_product', 'cp', 'cp.id_product = ps.id_product');
		$sql->where('s.scope LIKE "supplier"');
		$sql->where('cp.id_cart = '.(int)$id_cart);

		if ($id_carrier > -1)
			$sql->where('s.id_carrier = '.(int)$id_carrier);

		$sql->groupBy('id_supplier');
		$sql->orderBy('min_days DESC');
		$result = Db::getInstance()->executeS($sql);
		return $result;
	}

}

class DDWScopeCategory extends DDWScopeHelper
{
	public function getScopeMatches($search_string)
	{
		$sql = new DbQuery();
		$sql->select('c.name, c.id_category AS id');
		$sql->from('category_lang', 'c');
		$sql->where('c.name LIKE "%'.pSQL($search_string).'%"');
		$sql->where('c.id_shop = '.(int)$this->id_shop);
		$sql->where('c.id_lang = '.(int)$this->id_lang);
		$sql->orderBy('c.name');
		return Db::getInstance()->executeS($sql);
	}

	public function getName($id)
	{
		$sql = new DbQuery();
		$sql->select('cl.name');
		$sql->from('category_lang', 'cl');
		$sql->where('cl.id_category = '.(int)$id);
		$row = Db::getInstance()->getRow($sql);
		if (!empty($row['name'])) return $row['name'];
		else return '';
	}

	public function getCartCollection($id_cart, $id_carrier = -1)
	{
		$sql = new DbQuery();
		$sql->select('*');
		$sql->from('ddw_scopes', 's');
		$sql->innerJoin('category_product', 'c', 'c.id_category = s.id_associated');
		$sql->innerJoin('cart_product', 'cp', 'cp.id_product = c.id_product');
		$sql->where('s.scope LIKE "category"');
		$sql->where('cp.id_cart = '.(int)$id_cart);

		if ($id_carrier > -1)
			$sql->where('s.id_carrier = '.(int)$id_carrier);

		$sql->groupBy('id_category');
		$sql->orderBy('min_days DESC');
		$result = Db::getInstance()->executeS($sql);
		return $result;
	}

	public function getOptions($id_associated, $id_carrier)
	{
		$sql = new DbQuery();
		$sql->select('*');
		$sql->from('ddw_scopes');
		$sql->where('scope LIKE "category"');
		$sql->where('id_associated = '.(int)$id_associated);
		$sql->where('id_carrier = '.(int)$id_carrier);

		$row = Db::getInstance()->getRow($sql);
		return $row;
	}


}


class DDWScopeProduct extends DDWScopeHelper
{

	public function getScopeMatches($search_string)
	{
	}

	public function getName($id)
	{
		$sql = new DbQuery();
		$sql->select('pl.name');
		$sql->from('product_lang', 'pl');
		$sql->where('pl.id_product = '.(int)$id);
		$row = Db::getInstance()->getRow($sql);
		if (!empty($row['name'])) return $row['name'];
		else return '';
	}

	public function getCartCollection($id_cart, $id_carrier = -1)
	{
		$sql = new DbQuery();
		$sql->select('*');
		$sql->from('ddw_scopes', 's');
		$sql->innerJoin('product', 'p', 'p.id_product = s.id_associated');
		$sql->innerJoin('cart_product', 'cp', 'cp.id_product = p.id_product');
		$sql->where('s.scope LIKE "product"');
		$sql->where('cp.id_cart = '.(int)$id_cart);

		if ($id_carrier > -1)
			$sql->where('s.id_carrier = '.(int)$id_carrier);

		$sql->groupBy('cp.id_product');
		$sql->orderBy('min_days DESC');

		$result = Db::getInstance()->executeS($sql);
		return $result;
	}

}