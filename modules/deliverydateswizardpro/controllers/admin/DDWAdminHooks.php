<?php
/**
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * You must not modify, adapt or create derivative works of this source code
 *
 * @author    Musaffar Patel
 * @copyright 2016-2017 Musaffar Patel
 * @license   LICENSE.txt
 */

class DDWAdminHooks extends DDWControllerCore
{

	public function renderOrderDetailBlock($id_order)
	{
		$order_ddw = DDW::getOrderDDWDateTime($id_order);
		if ((int)$order_ddw['ddw_order_date'] > 0)
			$order_ddw['ddw_order_date'] = date('Y-m-d', strtotime($order_ddw['ddw_order_date']));
		else
			$order_ddw['ddw_order_date'] = '';

		$base_url = _PS_BASE_URL_.__PS_BASE_URI__;

		if ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off') || $_SERVER['SERVER_PORT'] == 443)
			$is_https = true;
		else
			$is_https = false;

		if ($is_https) $base_url = str_ireplace('http://', 'https://', $base_url);

		$this->sibling->smarty->assign(array(
			'order_ddw' => $order_ddw,
			'base_url' => $base_url
		));
		return $this->sibling->display($this->sibling->file, 'views/templates/admin/order_detail_block.tpl');		
	}


	public function renderHookDisplayPDFInvoice($params)
	{
		$return = '';
		$order_invoice = $params['object'];
		if (!($order_invoice instanceof OrderInvoice))
			return;
		$ddw_order = DDW::getOrderDDWDateTime($order_invoice->id_order);

		if ((int)$ddw_order['ddw_order_date'] > 0)
			//$return = DDWTranslationsModel::getTranslationByName('date_invoice_label').':'.date('j F Y', strtotime($ddw_order['ddw_order_date'])).'<br>';
			$return = DDWTranslationsModel::getTranslationByName('date_invoice_label').':'.Tools::displayDate($ddw_order['ddw_order_date']).'<br>';
		else
			$return = '';

		if ($ddw_order['ddw_order_time'] != '' && $ddw_order['ddw_order_time'] != 0)
			$return .= DDWTranslationsModel::getTranslationByName('time_invoice_label').':'.$ddw_order['ddw_order_time'];
		return $return;
	}

	public function renderHookDisplayPDFDeliverySlip($params)
	{
		return $this->renderHookDisplayPDFInvoice($params);
	}

	public function processOrderDDWUpdate()
	{
		if (Tools::getIsset('id_order'))
			DDW::saveToOrderDirect(Tools::getValue('id_order'), Tools::getValue('ddw_order_date'), Tools::getValue('ddw_order_time'));
		$redirect_url = '?controller=AdminOrders&id_order='.(int)Tools::getValue('id_order').'&vieworder&token='.Tools::getAdminTokenLite('AdminOrders');
		Tools::redirectAdmin($redirect_url);
	}

}