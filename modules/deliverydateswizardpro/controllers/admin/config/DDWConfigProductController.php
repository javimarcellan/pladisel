<?php
/**
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * You must not modify, adapt or create derivative works of this source code
 *
 * @author    Musaffar Patel
 * @copyright 2016-2017 Musaffar Patel
 * @license   LICENSE.txt
 */

class DDWConfigProductController extends DDWControllerCore
{

	public function renderList()
	{
		$ddw = new DDW();
		$ddw->loadByCarrier(Tools::getValue('id_carrier'));

		$ddw_scope = new DDWScope();
		$ddw_scopes = $ddw_scope->getCollection(Tools::getValue('id_carrier'), Context::getContext()->language->id, Context::getContext()->shop->id);

		Context::getContext()->smarty->assign(array(
			'module_config_url' => $this->module_config_url,
			'ddw_scopes' => $ddw_scopes
		));
		return $this->sibling->display($this->sibling->module_file, 'views/templates/admin/config/product.tpl');
	}

	public function renderAddForm()
	{
		if (Tools::getValue('id_scope') != '')
			$ddw_scope = new DDWScope(Tools::getValue('id_scope'));
		else
			$ddw_scope = new DDWScope();

		if (!empty($ddw_scope->id))
			$ddw_scope->name = DDWScopeHelper::getInstance($ddw_scope->scope, Context::getContext()->language->id, Context::getContext()->shop->id)->getName($ddw_scope->id_associated);

		Context::getContext()->smarty->assign(array(
			'module_config_url' => $this->module_config_url,
			'ddw_scope' => $ddw_scope,
			'hour_values' => range(0, 23),
			'min_values' => range(0, 59),
		));
		return $this->sibling->display($this->sibling->module_file, 'views/templates/admin/config/products_add.tpl');
	}

	public function processSearch()
	{
		$scope = DDWScopeHelper::getInstance(Tools::getValue('scope'), Context::getContext()->language->id, Context::getContext()->shop->id);
		$results = $scope->getScopeMatches(Tools::getValue('search_string'));
		return Tools::jsonEncode($results);
	}

	public function processForm()
	{
		if (Tools::getValue('id_scope') != '')
			$ddw_scope = new DDWScope(Tools::getValue('id_scope'));
		else
			$ddw_scope = new DDWScope();

		$ddw_scope->id_associated = Tools::getValue('id');
		$ddw_scope->scope = Tools::getValue('scope');
		$ddw_scope->cutofftime_enabled = (int)Tools::getValue('product_cutofftime_enabled');
		$ddw_scope->cutofftime_hours = (int)Tools::getValue('cutofftime_hours');
		$ddw_scope->cutofftime_minutes = (int)Tools::getValue('cutofftime_minutes');
		$ddw_scope->min_days = (int)Tools::getValue('min_days');
		$ddw_scope->max_days = (int)Tools::getValue('max_days');
		$ddw_scope->id_carrier = (int)Tools::getValue('id_carrier');
		$ddw_scope->save();
		return Tools::jsonEncode(array());
	}

	public function processDelete()
	{
		$ddw_scope = new DDWScope(Tools::getValue('id_scope'));
		if (!empty($ddw_scope->id))
			$ddw_scope->delete();
		return Tools::jsonEncode(array());
	}

	public function route()
	{
		switch (Tools::getValue('action'))
		{
			case 'renderlist' :
				die($this->renderList());

			case 'renderaddform' :
				die($this->renderAddForm());

			case 'processsearch':
				die($this->processSearch());

			case 'processeditform':
				die($this->processForm());

			case 'processdelete':
				die($this->processDelete());
		}
	}

}