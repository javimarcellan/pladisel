<?php
/**
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * You must not modify, adapt or create derivative works of this source code
 *
 * @author    Musaffar Patel
 * @copyright 2016-2017 Musaffar Patel
 * @license   LICENSE.txt
 */

class DDWConfigWeekdaysController extends DDWControllerCore
{
	protected $weekday_map = array('sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday');

	public function renderList()
	{
		$ddw = new DDW();
		$ddw->loadByCarrier(Tools::getValue('id_carrier'));

        /* render the list of weekdays */
		$i = 0;
		$weekday_item = array();
		$weekdays = array();
		foreach ($this->weekday_map as $weekday)
		{
			$ddw_weekday = new DDWWeekday();
			$ddw_weekday->loadWeekday(Tools::getValue('id_carrier'), $i, Context::getContext()->shop->id);

			$weekday_item['label'] = $this->l($weekday);
			$weekday_item['id_weekday'] = $i;
			$weekday_item['enabled'] = $ddw_weekday->enabled;
			$weekdays[] = $weekday_item;
			$i++;
		}

		Context::getContext()->smarty->assign(array(
            'id_carrier' => Tools::getValue('id_carrier'),
			'module_config_url' => $this->module_config_url,
			'ddw' => $ddw,
			'weekdays' => $weekdays
		));
		return $this->sibling->display($this->sibling->module_file, 'views/templates/admin/config/weekdays.tpl');
	}

	public function renderEditForm()
	{
		$ddw_weekday = new DDWWeekday();
		$ddw_weekday->loadWeekday(Tools::getValue('id_carrier'), Tools::getValue('id'), Context::getContext()->shop->id);

		$ddw_timeslots = new DDWTimeslot();
		$timeslots = $ddw_timeslots->getTimeSlotsForWeekday(Tools::getValue('id_carrier'), Tools::getValue('id'), Context::getContext()->shop->id);

		Context::getContext()->smarty->assign(array(
			'id_carrier' => Tools::getValue('id_carrier'),
			'ddw_weekday' => $ddw_weekday,
			'ddw_timeslots' => $timeslots,
			'hour_values' => range(0, 23),
			'min_values' => range(0, 59),
            'model' => 'weekdays'
		));
		return $this->sibling->display($this->sibling->module_file, 'views/templates/admin/config/datepopup_edit.tpl');
	}

	public function processForm()
	{
		$ddw_weekday = new DDWWeekday();
		$ddw_weekday->loadWeekday(Tools::getValue('id_carrier'), Tools::getValue('id'), Context::getContext()->shop->id);

		if (empty($ddw_weekday->id_carrier)) $ddw_weekday->id_carrier = Tools::getValue('id_carrier');
		if (empty($ddw_weekday->id_weekday)) $ddw_weekday->id_weekday = Tools::getValue('id');
		if (empty($ddw_weekday->id_shop)) $ddw_weekday->id_shop = Context::getContext()->shop->id;

		$ddw_weekday->enabled = (int)Tools::getValue('weekday_enabled');
		$ddw_weekday->minmax_enabled = (int)Tools::getValue('minmax_enabled');
		$ddw_weekday->min_days = Tools::getValue('min_days');
		$ddw_weekday->max_days = Tools::getValue('max_days');
		$ddw_weekday->cutofftime_enabled = (int)Tools::getValue('weekday_cutofftime_enabled');
		$ddw_weekday->cutofftime_hours = (int)Tools::getValue('weekday_cutofftime_hours');
		$ddw_weekday->cutofftime_minutes = (int)Tools::getValue('weekday_cutofftime_minutes');
		$ddw_weekday->min_days_postcutoff = (int)Tools::getValue('min_days_postcutoff');

		if ($ddw_weekday->save())
		{
			DDWTimeslot::deleteAllByCarrier(Tools::getValue('id_carrier'), Context::getContext()->shop->id, Tools::getValue('id'));
			$timeslots = Tools::jsonDecode(Tools::getValue('ddw_timeslots'));
			$i = 0;
			if (is_array($timeslots))
			{
				foreach ($timeslots as $timeslot)
				{
					$ddw_timeslot = new DDWTimeslot();
					$ddw_timeslot->id_weekday = Tools::getValue('id');
					$ddw_timeslot->id_ddw_specificdate = 0;
					$ddw_timeslot->id_carrier = Tools::getValue('id_carrier');
					$ddw_timeslot->id_shop = Context::getContext()->shop->id;
					$ddw_timeslot->time_start = DDWTimeslot::formatHumanTime($timeslot->time_start);
					$ddw_timeslot->time_end = DDWTimeslot::formatHumanTime($timeslot->time_end);
					$ddw_timeslot->order_limit = $timeslot->order_limit;
					$ddw_timeslot->position = $i;
					$ddw_timeslot->enabled = (int)$timeslot->enabled;
					$ddw_timeslot->add(false);
					$i++;
				}
			}
		}
	}

	public function route()
	{
		switch (Tools::getValue('action'))
		{
			case 'renderlist' :
				die($this->renderList());

			case 'processeditform' :
				die($this->processForm());
			default:
				return $this->renderEditForm();
		}
	}

}