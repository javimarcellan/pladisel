<?php
/**
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * You must not modify, adapt or create derivative works of this source code
 *
 * @author    Musaffar Patel
 * @copyright 2016-2017 Musaffar Patel
 * @license   LICENSE.txt
 */

class DDWConfigBlockedDatesController extends DDWControllerCore
{

	public function renderList()
	{
		$ddw_blockeddate = new DDWBlockedDate();
		$ddw_blockeddate_collection = $ddw_blockeddate->getCollection(Tools::getValue('id_carrier'), Context::getContext()->shop->id);
		Context::getContext()->smarty->assign(array(
			'module_config_url' => $this->module_config_url,
			'ddw_blockeddates' => $ddw_blockeddate_collection
		));
		return $this->sibling->display($this->sibling->module_file, 'views/templates/admin/config/blockeddates.tpl');
	}

	public function renderAddForm()
	{
		Context::getContext()->smarty->assign(array(
				'module_config_url' => $this->module_config_url,
				'ddw_blockeddates' => array()
		));
		return $this->sibling->display($this->sibling->module_file, 'views/templates/admin/config/blockeddates_add.tpl');
	}

	public function processForm()
	{
		if (Tools::getValue('id_blockeddate') == '')
			$ddw_blockeddate = new DDWBlockedDate();
		else
			$ddw_blockeddate = new DDWBlockedDate(Tools::getValue('id_blockeddate'));

		$ddw_blockeddate->id_carrier = Tools::getValue('id_carrier');
		$ddw_blockeddate->id_shop = Context::getContext()->shop->id;
		$ddw_blockeddate->recurring = Tools::getValue('recurring');
		$ddw_blockeddate->start_date = Tools::getValue('start_date');
		$ddw_blockeddate->end_date = Tools::getValue('end_date');
		$ddw_blockeddate->save();
		return Tools::jsonEncode(array());
	}

	public function processDelete()
	{
		$ddw_blockeddate = new DDWBlockedDate(Tools::getValue('id'));
		$ddw_blockeddate->delete();
		return Tools::jsonEncode(array());
	}

	public function route()
	{
		switch (Tools::getValue('action'))
		{
			case 'renderaddform' :
				die($this->renderAddForm());
				break;

			case 'processeditform' :
				die($this->processForm());
				break;

			case 'renderlist' :
				die($this->renderList());
				break;

			case 'processdelete' :
				die($this->processDelete());
				break;

		}
	}

}