<?php
/**
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * You must not modify, adapt or create derivative works of this source code
 *
 * @author    Musaffar Patel
 * @copyright 2016-2017 Musaffar Patel
 * @license   LICENSE.txt
 */

class DDWConfigGeneralController extends DDWControllerCore
{
	protected $sibling;

	public function __construct(&$sibling = null)
	{
		parent::__construct($sibling);
		if ($sibling !== null)
			$this->sibling = &$sibling;
	}
	public function setMedia()
	{
		if (Tools::getValue('controller') == 'AdminModules' && Tools::getValue('configure') == 'deliverydateswizardpro')
		{
			Context::getContext()->controller->addCSS($this->sibling->_path.'views/css/admin/lib/tools.css');
			Context::getContext()->controller->addCSS($this->sibling->_path.'views/css/admin/lib/popup.css');
			Context::getContext()->controller->addCSS($this->sibling->_path.'views/css/admin/config/config.css');
			Context::getContext()->controller->addCSS($this->sibling->_path.'views/css/admin/config/weekdays.css');
			Context::getContext()->controller->addCSS($this->sibling->_path.'views/css/admin/config/products.css');
			Context::getContext()->controller->addCSS($this->getAdminWebPath().'/themes/new-theme/public/theme.css');

			Context::getContext()->controller->addJquery();
			Context::getContext()->controller->addJS($this->sibling->_path.'views/js/admin/lib/popup.js');
			Context::getContext()->controller->addJS($this->sibling->_path.'views/js/admin/lib/tools.js');
			Context::getContext()->controller->addJS($this->sibling->_path.'views/js/admin/config/general.js');
			Context::getContext()->controller->addJS($this->sibling->_path.'views/js/admin/config/datepopup.js');
			Context::getContext()->controller->addJS($this->sibling->_path.'views/js/admin/config/weekdays.js');
			Context::getContext()->controller->addJS($this->sibling->_path.'views/js/admin/config/blockeddates.js');
			Context::getContext()->controller->addJS($this->sibling->_path.'views/js/admin/config/products.js');
			Context::getContext()->controller->addJS($this->sibling->_path.'views/js/admin/config/specificdates.js');
			//Context::getContext()->controller->addJS($this->getAdminWebPath().'/themes/new-theme/public/bundle.js');
			Context::getContext()->controller->addJqueryPlugin('tablednd');
		}
	}

	public function renderForm()
	{
		$ddw = new DDW();
		$ddw->loadByCarrier(Tools::getValue('id_carrier'));

		Context::getContext()->smarty->assign(array(
			'module_config_url' => $this->module_config_url,
			'id_carrier' => Tools::getValue('id_carrier'),
			'ddw' => $ddw,
			'hour_values' => range(0, 23),
			'min_values' => range(0, 59),
		));
		return $this->sibling->display($this->sibling->module_file, 'views/templates/admin/config/general.tpl');
	}

	public function processForm()
	{
		$ddw = new DDW(Tools::getValue('id_ddw'));
		$ddw->enabled = Tools::getValue('enabled');
		$ddw->required = Tools::getValue('required');
		$ddw->id_carrier = Tools::getValue('id_carrier');
		$ddw->min_days = (int)Tools::getValue('min_days');
		$ddw->max_days = (int)Tools::getValue('max_days');
		$ddw->cutofftime_enabled = (int)Tools::getValue('cutofftime_enabled');
		$ddw->cutofftime_hours = (int)Tools::getValue('cutofftime_hours');
		$ddw->cutofftime_minutes = (int)Tools::getValue('cutofftime_minutes');
		$ddw->timeslots_prep_minutes = (int)Tools::getValue('timeslots_prep_minutes');
		$ddw->product_eta_display_enabled = (int)Tools::getValue('product_eta_display_enabled');
		$ddw->save();
		return Tools::jsonEncode(array());
	}

	public function route()
	{
		switch (Tools::getValue('action'))
		{
			case 'renderform' :
				die($this->renderForm());

			case 'processform' :
				die($this->processForm());
		}
	}

}