<?php
/**
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * You must not modify, adapt or create derivative works of this source code
 *
 * @author    Musaffar Patel
 * @copyright 2016-2017 Musaffar Patel
 * @license   LICENSE.txt
 */

class DDWConfigTranslationsController extends DDWControllerCore
{

	public function renderList()
	{
		$translations = DDWTranslationsModel::getTranslations(Context::getContext()->shop->id, true);

		$fields_list = array(
			'name' => array(
				'title' => $this->l('Name'),
				'type' => 'text',
			),
			'type' => array(
				'title' => $this->l('Type'),
				'type' => 'text',
			)
		);
		$this->setupHelperList('Translations');
		$this->helper_list->currentIndex .= '&route=ddwconfigtranslations&action=renderform';

		$this->helper_list->identifier = 'id_translation';
		$this->helper_list->table = 'ddw_translations';
		$this->helper_list->show_toolbar = true;
		$this->helper_list->actions = array('edit');

		$return = '<br>';
		$return .= $this->helper_list->generateList($translations, $fields_list);
		return $return;
	}

	public function renderEditForm()
	{
		$ddw_translation = DDWTranslationsModel::getTranslation((int)Tools::getValue('id_translation'));

		if ($ddw_translation->type == 'text') $type = 'text';
		else $type = 'textarea';

		$this->setupHelperForm();
		$fields = array(
			'form' => array(
				'legend' => array(
					'title' => $this->l('Edit Translation'),
					'icon' => 'icon-list'
				),
				'input' => array(
					array(
						'name' => 'id_translation',
						'type' => 'hidden'
					),
					array(
						'label' => $ddw_translation->name,
						'type' => $type,
						'autoload_rte' => true,
						'name' => 'text',
						'lang' => true,
						'required' => true
					),
				),
				'submit' => array(
					'title' => $this->l('Save'),
					'class' => 'btn btn-default pull-left',
					'icon' => 'icon-list',
					'name' => 'submit_',
				)
			),
		);
		$this->helper_form->currentIndex .= '&route=ddwconfigtranslations&action=processform&id_translation='.(int)Tools::getValue('id_translation');

		if ($ddw_translation && $ddw_translation->id_translation != -1)
		{
			$this->helper_form->fields_value['id_translation'] = $ddw_translation->id_translation;
			$languages = $this->sibling->context->controller->getLanguages();

			foreach ($languages as $language)
				$this->helper_form->fields_value['text'][$language['id_lang']] = $ddw_translation->text_collection[$language['id_lang']];
		}
		return $this->helper_form->generateForm(array($fields));
	}

	public function processForm()
	{
		$ddw_translation = new TDDWTranslation();
		$ddw_translation->id_translation = Tools::getValue('id_translation');

		$languages = $this->sibling->context->controller->getLanguages();

		foreach ($languages as $language)
		{
			if (Tools::getIsset('text_'.$language['id_lang']))
				$ddw_translation->text_collection[$language['id_lang']] = Tools::getValue('text_'.$language['id_lang']);
			else
				$ddw_translation->text_collection[$language['id_lang']] = '';
		}
		DDWTranslationsModel::saveTranslationLang($ddw_translation);
		$this->redirect();
	}

	public function route()
	{
		switch (Tools::getValue('action'))
		{
			case 'renderform' :
				return $this->renderEditForm();

			case 'processform' :
				die($this->processForm());
		}
	}

}
