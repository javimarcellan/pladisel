<?php
/**
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * You must not modify, adapt or create derivative works of this source code
 *
 * @author    Musaffar Patel
 * @copyright 2016-2017 Musaffar Patel
 * @license   LICENSE.txt
 */

class DDWConfigSpecificDatesController extends DDWControllerCore
{

    /**
     * Render list of existing specific dates
     */
    public function renderList()
    {
        $ddw_specificdates_model = new DDWSpecificDateModel();
        $specificdates = $ddw_specificdates_model->getAll(Tools::getValue('id_carrier'), Context::getContext()->shop->id);

		Context::getContext()->smarty->assign(array(
			'module_config_url' => $this->module_config_url,
			'specificdates' => $specificdates
		));
        return $this->sibling->display($this->sibling->module_file, 'views/templates/admin/config/specificdates.tpl');
    }

	/**
	 * Render the settings popup form
	 * @return mixed
	 */
	public function renderEditForm()
	{
		$id_ddw_speificdate = (int)Tools::getValue('id');

		if ($id_ddw_speificdate > 0) {
            $ddw_specificdate_model = new DDWSpecificDateModel($id_ddw_speificdate);
			$ddw_timeslots = new DDWTimeslot();
			$timeslots = $ddw_timeslots->getTimeSlotsForSpecificDate($id_ddw_speificdate, Tools::getValue('id_carrier'), Context::getContext()->shop->id);
		}
		Context::getContext()->smarty->assign(array(
            'model' => 'specificdate',
			'id_carrier' => Tools::getValue('id_carrier'),
			'ddw_weekday' => $ddw_specificdate_model,
			'ddw_timeslots' => $timeslots,
			'hour_values' => range(0, 23),
			'min_values' => range(0, 59)
		));
		return $this->sibling->display($this->sibling->module_file, 'views/templates/admin/config/datepopup_edit.tpl');
	}

	/**
	 * Save the settings and the timeslots for the specific date
	 */
	public function processEditForm()
	{
        if ((int)Tools::getValue('id_ddw_specificdate') == 0) {
            $ddw_specificdate = new DDWSpecificDateModel();

            //delete any sopecific dates with the same date
            $ddw_specificdate->loadByDate(Tools::getValue('start_date'), Tools::getValue('id_carrier'), Context::getContext()->shop->id);
            if (!empty($ddw_specificdate->id_ddw_specificdate)) {
                DDWTimeslot::deleteAllBySpecificDate($ddw_specificdate->id_ddw_specificdate);
                $ddw_specificdate->delete();
                $ddw_specificdate = new DDWSpecificDateModel();
            }
        } else {
            $ddw_specificdate = new DDWSpecificDateModel(Tools::getValue('id_ddw_specificdate'));
        }

		if (empty($ddw_specificdate->id_carrier)) {
            $ddw_specificdate->id_carrier = Tools::getValue('id_carrier');
        }
		if (empty($ddw_specificdate->id_shop)) {
            $ddw_specificdate->id_shop = Context::getContext()->shop->id;
        }

        if (empty(Tools::getValue('start_date'))) {
            //@todo: display error
            return false;
        }

        $ddw_specificdate->enabled = (int)Tools::getValue('weekday_enabled');
        $ddw_specificdate->start_date = pSQL(Tools::getValue('start_date'));
        $ddw_specificdate->minmax_enabled = (int)Tools::getValue('minmax_enabled');
        $ddw_specificdate->min_days = Tools::getValue('min_days');
        $ddw_specificdate->max_days = Tools::getValue('max_days');
        $ddw_specificdate->cutofftime_enabled = (int)Tools::getValue('weekday_cutofftime_enabled');
        $ddw_specificdate->cutofftime_hours = (int)Tools::getValue('weekday_cutofftime_hours');
        $ddw_specificdate->cutofftime_minutes = (int)Tools::getValue('weekday_cutofftime_minutes');
        $ddw_specificdate->min_days_postcutoff = (int)Tools::getValue('min_days_postcutoff');

		if ($ddw_specificdate->save())
		{
			DDWTimeslot::deleteAllByCarrier(Tools::getValue('id_carrier'), Context::getContext()->shop->id, '', $ddw_specificdate->id);
			$timeslots = Tools::jsonDecode(Tools::getValue('ddw_timeslots'));
			$i = 0;
			if (is_array($timeslots)) {
				foreach ($timeslots as $timeslot) {
					$ddw_timeslot = new DDWTimeslot();
                    $ddw_timeslot->id_weekday = -1;
					$ddw_timeslot->id_ddw_specificdate = $ddw_specificdate->id;
					$ddw_timeslot->id_carrier = Tools::getValue('id_carrier');
					$ddw_timeslot->id_shop = Context::getContext()->shop->id;
					$ddw_timeslot->time_start = DDWTimeslot::formatHumanTime($timeslot->time_start);
					$ddw_timeslot->time_end = DDWTimeslot::formatHumanTime($timeslot->time_end);
					$ddw_timeslot->order_limit = $timeslot->order_limit;
					$ddw_timeslot->position = $i;
					$ddw_timeslot->enabled = (int)$timeslot->enabled;
					$ddw_timeslot->add(false);
					$i++;
				}
			}
		}
	}

    /**
     * Delete a specific date completely including related timeslots
     */
    public function processDelete()
    {
        if ((int)Tools::getValue('id_ddw_specificdate') <= 0) {
            return false;
        }
        DDWTimeslot::deleteAllBySpecificDate(Tools::getValue('id_ddw_specificdate'));
        $ddw_specificdate_model = new DDWSpecificDateModel((int)Tools::getValue('id_ddw_specificdate'));
        $ddw_specificdate_model->delete();
    }

	public function route()
	{
		switch (Tools::getValue('action'))
		{
            case 'renderlist':
                die ($this->renderList());

			case 'rendereditform':
				die ($this->renderEditForm());

			case 'processeditform':
				die ($this->processEditForm());

            case 'processdelete':
                die ($this->processDelete());
		}
	}
}