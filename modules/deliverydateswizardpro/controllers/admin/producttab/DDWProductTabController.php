<?php
/**
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * You must not modify, adapt or create derivative works of this source code
 *
 * @author    Musaffar Patel
 * @copyright 2016-2017 Musaffar Patel
 * @license   LICENSE.txt
 */

class DDWProductTabController extends DDWControllerCore
{

	public function __construct($sibling, $params = array())
	{
		parent::__construct($sibling, $params);
		$this->sibling = $sibling;
		$this->set_module_base_admin_url();
		$this->base_url = Tools::getShopProtocol().Tools::getShopDomain().__PS_BASE_URI__;

		//$this->_addJS();
		//$this->_addCSS();
	}

	private function _addJS()
	{
		if (!empty(Context::getContext()->controller))
		{
			Context::getContext()->controller->addJS($this->sibling->base_url.'/modules/'.$this->sibling->module_folder.'/views/js/admin/lib/popup.js');
			Context::getContext()->controller->addJS($this->sibling->base_url.'/modules/'.$this->sibling->module_folder.'/views/js/admin/lib/tools.js');
			Context::getContext()->controller->addJS($this->sibling->base_url.'/modules/'.$this->sibling->module_folder.'/views/js/admin/producttab/general.js');
		}
	}

	private function _addCSS()
	{
		if (!empty(Context::getContext()->controller))
		{
			Context::getContext()->controller->addCSS($this->sibling->base_url.'/modules/'.$this->sibling->module_folder.'/views/css/admin/lib/tools.css');
			Context::getContext()->controller->addCSS($this->sibling->base_url.'/modules/'.$this->sibling->module_folder.'/views/css/admin/lib/popup.css');
			Context::getContext()->controller->addCSS($this->sibling->base_url.'/modules/'.$this->sibling->module_folder.'/views/css/admin/producttab/general.css');
		}
	}


	protected function set_module_base_admin_url()
	{
		if (!defined('_PS_ADMIN_DIR_'))
		{
			$this->admin_mode = false;
			return false;
		}
		else
		{
			$this->admin_mode = true;
			$arr_temp = explode('/', _PS_ADMIN_DIR_);
			if (count($arr_temp) <= 1) $arr_temp = explode('\\', _PS_ADMIN_DIR_);
			$dir_name = end($arr_temp);
			$this->module_base_url = $dir_name.'/index.php?controller=AdminProducts&id_product='.Tools::getValue('id_product').'&updateproduct&token='.Tools::getAdminTokenLite('AdminProducts');
		}
	}

	public function route()
	{
		switch (Tools::getValue('route'))
		{
			case 'ddwadmintabgeneral' :
				$ddw_admin_tab_general_controller = new DDWAdminTabGeneralController($this->sibling, $this->params);
				die($ddw_admin_tab_general_controller->route());
		}

		$general_controller = new DDWAdminTabGeneralController($this->sibling, $this->params);
		$ret = '';
		$ret .= $general_controller->renderForm();

		return $ret;
	}

}