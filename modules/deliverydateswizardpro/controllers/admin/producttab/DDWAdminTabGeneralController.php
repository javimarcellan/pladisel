<?php
/**
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * You must not modify, adapt or create derivative works of this source code
 *
 * @author    Musaffar Patel
 * @copyright 2016-2017 Musaffar Patel
 * @license   LICENSE.txt
 */

class DDWAdminTabGeneralController extends DDWControllerCore
{
	public function setMedia()
	{
		if (Tools::getValue('controller') == 'AdminProducts')
		{
			Context::getContext()->controller->addCSS($this->sibling->_path.'views/css/admin/lib/tools.css');
			Context::getContext()->controller->addCSS($this->sibling->_path.'views/css/admin/lib/popup.css');
			Context::getContext()->controller->addCSS($this->sibling->_path.'views/css/admin/producttab/general.css');

			Context::getContext()->controller->addJS($this->sibling->_path.'views/js/admin/lib/popup.js');
			Context::getContext()->controller->addJS($this->sibling->_path.'views/js/admin/lib/tools.js');
			Context::getContext()->controller->addJS($this->sibling->_path.'views/js/admin/producttab/general.js');
		}
	}

	public function renderForm()
	{
		$carrier = new Carrier();
		$carriers = $carrier->getCarriers(Context::getContext()->language->id);

		Context::getContext()->smarty->assign(array(
			'module_ajax_url' => $this->module_ajax_url,
			'id_product' => $this->params['id_product'],
			'carriers' => $carriers,
			'hour_values' => range(0, 23),
			'min_values' => range(0, 59)
		));
		return $this->sibling->display($this->sibling->module_file, 'views/templates/admin/producttab/general.tpl');
	}

	public function loadOptions($id_product, $id_carrier)
	{
		$ddw_scope = DDWScopeHelper::getInstance('product', Context::getContext()->language->id, Context::getContext()->shop->id);
		$scope = $ddw_scope->getOptions($id_product, $id_carrier);

		if (!empty($scope))
			return Tools::jsonEncode($scope);
		else
			return Tools::jsonEncode(array());
	}

	public function processForm()
	{
		if (Tools::getValue('id_scope') == '') {
			$ddw_scope = new DDWScope();
			$ddw_scope->load((int)Tools::getValue('id_product'), (int)Tools::getValue('id_carrier'), 'product');
		}
		else {
			$ddw_scope = new DDWScope(Tools::getValue('id_scope'));
		}

		$ddw_scope->id_carrier = (int)Tools::getValue('id_carrier');
		$ddw_scope->id_associated = (int)Tools::getValue('id_product');
		$ddw_scope->min_days = (int)Tools::getValue('min_days');
		$ddw_scope->max_days = (int)Tools::getValue('max_days');
		$ddw_scope->cutofftime_enabled = (int)Tools::getValue('product_cutofftime_enabled');
		$ddw_scope->cutofftime_hours = (int)Tools::getValue('cutofftime_hours');
		$ddw_scope->cutofftime_minutes = (int)Tools::getValue('cutofftime_minutes');
		$ddw_scope->scope = 'product';
		$ddw_scope->save();
	}

	public function route()
	{
		switch (Tools::getValue('action'))
		{
			case 'loadoptions' :
				die($this->loadOptions(Tools::getValue('id_product'), Tools::getValue('id_carrier')));

			case 'processform' :
				die($this->processForm());
		}
	}


}