<?php
/**
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * You must not modify, adapt or create derivative works of this source code
 *
 * @author    Musaffar Patel
 * @copyright 2016-2017 Musaffar Patel
 * @license   LICENSE.txt
 */

class DDWConfigCarrierController extends DDWControllerCore
{

	public function route()
	{
		$general_controller = new DDWConfigGeneralController($this->sibling);
		$weekdays_controller = new DDWConfigWeekdaysController($this->sibling);
		$blockeddates_controller = new DDWConfigBlockedDatesController($this->sibling);
		$product_controller = new DDWConfigProductController($this->sibling);
		$specificdays_controller = new DDWConfigSpecificDatesController($this->sibling);

		Context::getContext()->smarty->assign(array(
			//'content_general' => $general_controller->renderForm(),
			'content_weekdays' => $weekdays_controller->renderList(),
			//'content_blockeddates' => $blockeddates_controller->renderList(),
			'content_products' => $product_controller->renderList()
		));
		return $this->sibling->display($this->sibling->module_file, 'views/templates/admin/config/main.tpl');
	}

}
