<?php
/**
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * You must not modify, adapt or create derivative works of this source code
 *
 * @author    Musaffar Patel
 * @copyright 2016-2017 Musaffar Patel
 * @license   LICENSE.txt
 */

class DDWConfigController extends DDWControllerCore
{
	private function renderCarrierList()
	{
		$fields = array(
			'id_carrier' => array(
				'title' => $this->l('ID'),
				'width' => 140,
				'type' => 'text',
			),
			'name' => array(
				'title' => $this->l('Name'),
				'width' => 140,
				'type' => 'text',
			)
		);

		$helper = new HelperList();
		$helper->shopLinkType = '';
		$helper->simple_header = false;
		$helper->token = Tools::getAdminTokenLite('AdminModules');

		$helper->actions = array('edit');

		$helper->identifier = 'id_carrier';
		$helper->show_toolbar = true;
		$helper->title = 'Carriers';
		$helper->table = 'carriers';

		$helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->sibling->name;
		$carrier_list = CarrierCore::getCarriers(Context::getContext()->language->id, false, false, false, null, CarrierCore::ALL_CARRIERS);

		$return = $helper->generateList($carrier_list, $fields);
		return $return;
	}

	public function renderMain()
	{
		$ddw_translations_controller = new DDWConfigTranslationsController($this->sibling);
		$rendered = $this->renderCarrierList();
		$rendered .= $ddw_translations_controller->renderList();
		return $rendered;
	}

}