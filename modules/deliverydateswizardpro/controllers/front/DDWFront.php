<?php
/**
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * You must not modify, adapt or create derivative works of this source code
 *
 * @author    Musaffar Patel
 * @copyright 2016-2017 Musaffar Patel
 * @license   LICENSE.txt
 */

class DDWControllerFront extends DDWControllerCore
{
	/** @var DDW */
	public $ddw_model;

	public function __construct(&$sibling = null)
	{
		parent::__construct($sibling);
		if ($sibling !== null)
			$this->sibling = &$sibling;
	}

	public function setMedia()
	{
		if (Tools::getValue('controller') == 'order' || Tools::getValue('controller') == 'supercheckout' || Tools::getValue('controller') == 'amzpayments')
		{
			$this->context->controller->addJquery();
			$this->context->controller->addJqueryUI('ui.datepicker');
			//$this->context->controller->registerJavascript('typewatch', Media::getJqueryPluginPath('ui.datepicker', null)['js']);
			$this->sibling->context->controller->registerJavascript('jquery_ui_localizations', 'js/jquery/ui/i18n/jquery.ui.datepicker-'.Context::getContext()->language->iso_code.'.js', null);

			$this->sibling->context->controller->addJS($this->sibling->_path.'views/js/front/ddw.js');
			$this->sibling->context->controller->addCSS($this->sibling->_path.'views/css/front/deliverydateswizard.css');
			$this->sibling->context->controller->addCSS($this->sibling->_path.'views/css/front/datepicker.css');
		}

		if (Tools::getValue('controller') == 'product')
			$this->sibling->context->controller->addCSS($this->sibling->_path.'views/css/front/deliverydateswizard.css');
	}

	public function getWeekdayTranslated($id_weekday)
	{
		$weekdays = array('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday');
		return $this->l($weekdays[$id_weekday]);
	}

	public function getMonthTranslated($id_month)
	{
		$months = array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
		return $this->l($months[$id_month - 1]);
	}


	/**
	 * @var $blocked_dates Array Of DDWBlockedDate
	 * @var $date timestamp
	 * @return boolean
	 */
	private function isDateBlocked($blocked_dates, $date)
	{
		$blocked = false;

		/* Weekdays Blocked check */
		$current_week_day = date('w', $date);

		if (!in_array($current_week_day, $this->weekdays)) return true;
		if (!is_array($blocked_dates)) return false;

		foreach ($blocked_dates as $blocked_date)
		{
			if ($blocked_date->recurring == 0)
			{
				$y = date('Y', strtotime($blocked_date->start_date));
				$m = date('m', strtotime($blocked_date->start_date));
				$d = date('d', strtotime($blocked_date->start_date));
				if ($blocked_date->start_date == $blocked_date->end_date)
					$blocked_date->end_date = date('Y-m-d H:i:s', strtotime("$y-$m-$d 23:59:59"));				
			}
			$timestamp_start = strtotime($blocked_date->start_date);
			$timestamp_end = strtotime($blocked_date->end_date);

			if ($date >= $timestamp_start && $date <= $timestamp_end)
				$blocked = true;

			/* Recurring block check */
			if ($blocked_date->recurring == 1)
			{
				$recurring_timestamp_start = date('Y-m-d H:i:s', strtotime(
					date('Y').'-'.date('m', $timestamp_start).'-'.date('d', $timestamp_start).' 00:00:00'
				));
				$recurring_timestamp_end = date('Y-m-d H:i:s', strtotime(
					date('Y').'-'.date('m', $timestamp_end).'-'.date('d', $timestamp_end).' 23:59:00'
				));
				if ($date >= $recurring_timestamp_start && $date <= $recurring_timestamp_end) $blocked = true;
			}
		}
		return $blocked;
	}

	public function getScopeExceptions($id_carrier, $id_shop)
	{
		$scopes = array();

		/* supplier overrides */
		$ddw_scope_helper = DDWScopeHelper::getInstance('supplier', Context::getContext()->language->id, $id_shop);
		$supplier_scopes = $ddw_scope_helper->getCartCollection(Context::getContext()->cart->id, $id_carrier);
		if (is_array($supplier_scopes) && count($supplier_scopes) > 0)
			$scopes[] = $supplier_scopes[0];

		/* category overrides */
		$ddw_scope_helper = DDWScopeHelper::getInstance('category', Context::getContext()->language->id, $id_shop);
		$category_scopes = $ddw_scope_helper->getCartCollection(Context::getContext()->cart->id, $id_carrier);
		if (is_array($category_scopes) && count($category_scopes) > 0)
			$scopes[] = $category_scopes[0];

		/* product overrides */
		$ddw_scope_helper = DDWScopeHelper::getInstance('product', Context::getContext()->language->id, $id_shop);
		$product_scopes = $ddw_scope_helper->getCartCollection(Context::getContext()->cart->id, $id_carrier);
		if (is_array($product_scopes) && count($product_scopes) > 0)
			$scopes[] = $product_scopes[0];

		if (!empty($scopes))
		{
			usort($scopes, function($a, $b) {
				return $b['min_days'] - $a['min_days'];
			});
			return $scopes[0];
		}

		else return false;
	}

	public function getBlockedDates($override_scope = null)
	{
		$return = array();

		if (empty($override_scope['id_carrier']))
			$id_carrier = (int)Tools::getValue('id_carrier');
		else
			$id_carrier = (int)$override_scope['id_carrier'];

		$id_shop = Context::getContext()->shop->id;
		$start_date = date('Y-m-d');
		$calendar_blocked_dates = array(); //of TDDWCalendarBlockedDates

		$this->ddw_model = new DDW();
		$this->ddw_model->loadByCarrier($id_carrier);

		$cutofftime_enabled = $this->ddw_model->cutofftime_enabled;
		$cutofftime_hours = $this->ddw_model->cutofftime_hours;
		$cutofftime_minutes = $this->ddw_model->cutofftime_minutes;
		$min_days = $this->ddw_model->min_days;

		/* Load available weekdays */
		$ddw_weekdays = new DDWWeekday();
		$weekdays = $ddw_weekdays->getAvailableWeekdaysCollection($id_carrier, $id_shop);
		if (!empty($weekdays))
			foreach ($weekdays as $weekday)
				$this->weekdays[] = $weekday->id_weekday;
		else
			$this->weekdays = array();

		$blocked_dates = new DDWBlockedDate();
		$blocked_dates_collection = $blocked_dates->getCollection($id_carrier, $id_shop);

		$today_is_blocked = $this->isDateBlocked($blocked_dates_collection, time());

		/* week day settings */
		$current_week_day = date('w');
		$ddw_weekday = new DDWWeekday();
		$ddw_weekday->loadWeekday($id_carrier, $current_week_day, $id_shop);

		if ($ddw_weekday->cutofftime_enabled) {
			$cutofftime_enabled = $ddw_weekday->cutofftime_enabled;
			$cutofftime_hours = $ddw_weekday->cutofftime_hours;
			$cutofftime_minutes = $ddw_weekday->cutofftime_minutes;
		}

		/* ovverride the min / max day for the current weekday if setting enabled */
		if ($ddw_weekday->minmax_enabled) {
			$min_days = $ddw_weekday->min_days;
			$max_days = $ddw_weekday->max_days;
		}

        /* Specific Dates Settings */
        $ddw_specificdate_model = new DDWSpecificDateModel();
        $ddw_specificdate_model->loadByDate(date('Y-m-d'), $id_carrier, $id_shop);
        if ($ddw_specificdate_model->id_ddw_specificdate > 0) {
            if ($ddw_specificdate_model->cutofftime_enabled) {
                $cutofftime_enabled = $ddw_specificdate_model->cutofftime_enabled;
                $cutofftime_hours = $ddw_specificdate_model->cutofftime_hours;
                $cutofftime_minutes = $ddw_specificdate_model->cutofftime_minutes;
            }

            if ($ddw_specificdate_model->minmax_enabled) {
                $min_days = $ddw_specificdate_model->min_days;
                $max_days = $ddw_specificdate_model->max_days;
            }
        }

        /* override min / max based on scope exceptions (supplier, category and product) */
		if ($override_scope == null) {
			$scope = $this->getScopeExceptions($id_carrier, $id_shop);
			if ($scope != false) {
				if ($scope['min_days'] >$min_days ) {
					$min_days = $scope['min_days'];
				}
				
				$max_days = $scope['max_days'];

				if ($scope['cutofftime_enabled'])
				{
					$cutofftime_hours = $scope['cutofftime_hours'];
					$cutofftime_minutes = $scope['cutofftime_minutes'];
				}
			}
		}
		else {
			if ($override_scope['min_days'] >$min_days ) {
				$min_days = $override_scope['min_days'];
			}
		
			$max_days = $override_scope['max_days'];

			if ($override_scope['cutofftime_enabled'])
			{
				$cutofftime_hours = $override_scope['cutofftime_hours'];
				$cutofftime_minutes = $override_scope['cutofftime_minutes'];
			}
		}
		$max_days = ($this->ddw_model->max_days == 0 ? 365 : $this->ddw_model->max_days);

		/* Determine if cut off time requires Min Days to be blocked from today onwards  */
		$cutofftime_passed = false;
		if (($cutofftime_enabled == 1 || $ddw_weekday->cutofftime_enabled == 1) && !$today_is_blocked) {
			$hours = date('H');
			$minutes = date('i');

			if ($hours > $cutofftime_hours)
				$cutofftime_passed = true;
			elseif ($hours == $cutofftime_hours &&  $minutes >= $cutofftime_minutes)
				$cutofftime_passed = true;

			if ($cutofftime_passed)
				$min_days++;
		}

		if ($today_is_blocked)
			$min_days++;

		/* Adjust min days to post cut off time for weekday if necessary */
		if ($ddw_weekday->minmax_enabled && $cutofftime_passed && $ddw_weekday->cutofftime_enabled && $ddw_weekday->min_days_postcutoff > 0)
			$min_days = $ddw_weekday->min_days_postcutoff + 1;

		/* If any of the days within min days falls on a block date, increment min days accordingly */
		$j = $min_days;

		for ($i = 0; $i < $j; $i++)
		{
			$loop_date = strtotime("+$i day", strtotime($start_date));
			$today = date('Y-m-d');
			$loop_date_compare = date('Y-m-d', $loop_date);

			/* if today is blocked and cut off time, do not add today to min days offset */
			if ($today == $loop_date_compare && $today_is_blocked)
				continue;
			else
				if ($this->isDateBlocked($blocked_dates_collection, $loop_date))
				{
					$min_days++;
					break;
				}
		}

		/* Block all days up to min_days from order date (today) */
		for ($i = 0; $i < $min_days; $i++)
		{
			$loop_date = strtotime("+$i day", strtotime($start_date));
			$calendarBlockedDate = new TDDWCalendarBlockedDate();
			$calendarBlockedDate->date = date('Y-m-d', $loop_date);
			$calendarBlockedDate->blocked = true;
			$calendar_blocked_dates[] = $calendarBlockedDate;
		}

		/* Loop through dates */
		for ($i = 0; $i < $max_days; $i++)
		{
			$loop_date = strtotime("+$i day", strtotime($start_date));
			if ($this->isDateBlocked($blocked_dates_collection, $loop_date))
			{
				$calendarBlockedDate = new TDDWCalendarBlockedDate();
				$calendarBlockedDate->date = date('Y-m-d', $loop_date);
				$calendarBlockedDate->blocked = true;
				$calendar_blocked_dates[] = $calendarBlockedDate;
			} 
		}

		/* Fix bug which caused last day of max date to become selcatble when it fell within a blocked date range */
		$calendarBlockedDate = new TDDWCalendarBlockedDate();
		$calendarBlockedDate->date = date('Y-m-d', strtotime('+'.$max_days.' days'));
		$calendarBlockedDate->blocked = true;
		$calendar_blocked_dates[] = $calendarBlockedDate;

		// remove calendar blocked dates which are allowed in specific dates
		$allowed_dates = $ddw_specificdate_model->getAll($id_carrier, $id_shop);
		$allowed_dates_arr = array();
		foreach ($allowed_dates as $key => $allowed_date) {
			if ($allowed_date->enabled) {
				$allowed_dates_arr[$allowed_date->start_date] = $allowed_date->start_date;
			}
		}

		$calendar_blocked_dates_tmp = array();
		foreach ($calendar_blocked_dates as $blocked_date) {
			if (empty($allowed_dates_arr[$blocked_date->date])) {
				$calendar_blocked_dates_tmp[] = $blocked_date;
			}
		}
		$calendar_blocked_dates = $calendar_blocked_dates_tmp;

		/* If enabled/disabled */
		if ($this->ddw_model->enabled != 1) {
			$calendar_blocked_dates = array();
			$calendar_blocked_dates['enabled'] = false;
			$return['enabled'] = false;
			return $return;
		}

		/* Create return data */
		$return['min_date'] = date('Y-m-d');
		$return['max_date'] = date('Y-m-d', strtotime('+'.$max_days.' days'));
		$return['calendar_blocked_dates'] = $calendar_blocked_dates;

		$return['enabled'] = $this->ddw_model->enabled;
		$return['required'] = $this->ddw_model->required;

		/* get first available day for delivery, allowing the calendar to change months if necessary */
		$start_date = date('Y-m-d');
		$haystack = array();
		foreach ($calendar_blocked_dates as $date_obj) {
			$haystack[] = $date_obj->date;
		}

		// now get first available day for delivery and check if that day has timeslots available
		$loop_date = $start_date;
		$ddw_timeslots = new DDWTimeslot();
		$timeslot_check_complete = false;
        $recheck_timeslots = false;

        for ($i = 0; $i < 9999; $i++) {
			$loop_date = date('Y-m-d', strtotime("+$i day", strtotime($start_date)));
			if (!in_array($loop_date, $haystack)) {

				if (!$timeslot_check_complete) {
					$day_of_week = date('w', strtotime($loop_date));

                    $ddw_specificdate_model = new DDWSpecificDateModel();
                    $ddw_specificdate_model->loadByDate(date('Y-m-d', strtotime($loop_date)), $id_carrier, $id_shop);

                    if ($ddw_specificdate_model->id_ddw_specificdate > 0) {
                        $timeslots_collection_unfiltered = $ddw_timeslots->getTimeSlotsForSpecificDate($ddw_specificdate_model->id_ddw_specificdate, $id_carrier, $id_shop);
                    } else {
                        $timeslots_collection_unfiltered = $ddw_timeslots->getTimeSlotsForWeekday($id_carrier, $day_of_week, $id_shop);
                    }

					$timeslots = $this->filterTimeslots($timeslots_collection_unfiltered, $loop_date, $this->ddw_model);

					if (count($timeslots_collection_unfiltered) > 0 && empty($timeslots)) {
						$timeslot_check_complete = true;
						$calendarBlockedDate = new TDDWCalendarBlockedDate();
						$calendarBlockedDate->date = date($loop_date);
						$calendarBlockedDate->blocked = true;
						array_unshift($calendar_blocked_dates, $calendarBlockedDate);
					} else {
						break;
					}
				} else {
					break;
				}
			}
		}

        // Get default timeslots for first available day
        if ($recheck_timeslots) {
            $haystack = array();
            foreach ($calendar_blocked_dates as $date_obj) {
                $haystack[] = $date_obj->date;
            }

            for ($i = 0; $i < 9999; $i++) {
                $loop_date = date('Y-m-d', strtotime("+$i day", strtotime($start_date)));

                if (!in_array($loop_date, $haystack)) {
                    $day_of_week = date('w', strtotime($loop_date));
                    $ddw_specificdate_model = new DDWSpecificDateModel();
                    $ddw_specificdate_model->loadByDate(date('Y-m-d', strtotime($loop_date)), $id_carrier, $id_shop);

                    if ($ddw_specificdate_model->id_ddw_specificdate > 0) {
                        $timeslots_collection_unfiltered = $ddw_timeslots->getTimeSlotsForSpecificDate($ddw_specificdate_model->id_ddw_specificdate, $id_carrier, $id_shop);
                    } else {
                        $timeslots_collection_unfiltered = $ddw_timeslots->getTimeSlotsForWeekday($id_carrier, $day_of_week, $id_shop);
                    }

                    $timeslots = $this->filterTimeslots($timeslots_collection_unfiltered, $loop_date, $this->ddw_model);
                    break;
                }
            }
        }

		$return['calendar_blocked_dates'] = $calendar_blocked_dates;
		$return['defaults']['calendar_default_day'] = date('d', strtotime($loop_date));
		$return['defaults']['calendar_default_month'] = date('m', strtotime($loop_date));
		$return['defaults']['calendar_default_year'] = date('Y', strtotime($loop_date));
		$return['defaults']['timeslots'] = $timeslots;
		return $return;
	}

	public function update_ddw_cart()
	{
		if (Context::getContext()->cart->id != '')
			DDW::saveToCart(Tools::getValue('ddw_date'), Tools::getValue('ddw_time'), Context::getContext()->cart->id);
	}

	public function getLastDDWCart()
	{
		if (Context::getContext()->cart->id != '')
			return DDW::getCartDDWDateTime(Context::getContext()->cart->id);
	}

	public function renderFrontWidget()
	{
		$ddw_translations = DDWTranslationsModel::getTranslations(Context::getContext()->shop->id, false);
		$this->assignTranslations($ddw_translations);

		$this->sibling->smarty->assign(array(
			'controller_name' => Context::getContext()->controller->php_self,
			'controller' => Tools::getValue('controller'),
			'id_lang' => Context::getContext()->language->id,
			'baseDir' => __PS_BASE_URI__
		));
		return $this->sibling->display($this->sibling->file, 'views/templates/front/widget.tpl');
	}

	/**
	 * Filter the timeslots by time of day and orders placed
	 * @param $timeslot_collection
	 * @param $date
	 * @param $ddw_model
	 * @return array
	 */
	private function filterTimeslots($timeslot_collection, $date, $ddw_model)
	{
		$timeslots = array();
		//only filter todays timeslots
		if (!empty($timeslot_collection) && $date == date('Y-m-d')) {
			foreach ($timeslot_collection as $timeslot) {
				$seconds_timeslot_start = strtotime(date('Ymd') . $timeslot->time_start . ':00');
				$seconds_timeslot_end = strtotime(date('Ymd') . $timeslot->time_end . ':00');
				$seconds_now = time();
				$min = ($seconds_timeslot_start - $seconds_now) / 60;
				if ($min > 0) {
					if ((int)$ddw_model->timeslots_prep_minutes > 0 && $min >= $ddw_model->timeslots_prep_minutes) {
						$timeslots[] = $timeslot;
					} elseif ($ddw_model->timeslots_prep_minutes == 0) {
						$timeslots[] = $timeslot;
					}
				}
			}
		} else {
			$timeslots = $timeslot_collection;
		}

		if (!empty($timeslots)) {
			foreach ($timeslots as &$timeslot) {
				$count = DDW::getOrderCountInTimeWindow($date,
					$timeslot->time_start . ' - ' . $timeslot->time_end, $this->context->shop->id);

				if ($timeslot->order_limit > 0) {
					if ($count < $timeslot->order_limit) {
						$timeslot->available = 1;
					} else {
						$timeslot->available = 0;
					}
				} else {
					$timeslot->available = 1;
				}

				if (!$timeslot->enabled) {
					$timeslot->available = 0;
				}
			}
		}
		return $timeslots;
	}

	public function renderTimeSlots()
	{
		$json_return = array();

		$ddw_model = new DDW();
		$ddw_model->loadByCarrier(Tools::getValue('id_carrier'));

		$day_of_week = date('w', strtotime(Tools::getValue('date')));

		$ddw_timeslots = new DDWTimeslot();
        $ddw_specificdate_model = new DDWSpecificDateModel();
        $ddw_specificdate_model->loadByDate(date('Y-m-d', strtotime(Tools::getValue('date'))), Tools::getValue('id_carrier'), Context::getContext()->shop->id);

        if ($ddw_specificdate_model->id_ddw_specificdate > 0 && $ddw_specificdate_model->enabled) {
            $timeslots_collection_unfiltered = $ddw_timeslots->getTimeSlotsForSpecificDate($ddw_specificdate_model->id_ddw_specificdate, Tools::getValue('id_carrier'), Context::getContext()->shop->id);
        } else {
            $timeslots_collection_unfiltered = $ddw_timeslots->getTimeSlotsForWeekday(Tools::getValue('id_carrier'), $day_of_week, Context::getContext()->shop->id);
        }

        $timeslots = $this->filterTimeslots($timeslots_collection_unfiltered, Tools::getValue('date'), $ddw_model);
		$json_return['timeslot_realcount'] = count($timeslots_collection_unfiltered);

		$this->sibling->smarty->assign(array(
			'timeslots' => $timeslots
		));

		$json_return['html'] = $this->sibling->display($this->sibling->file, 'views/templates/front/timeslots.tpl');
		return Tools::jsonEncode($json_return);
	}

	/* Hooks */

	/**
	 * Save the selected delivery date timeslot ID to the cart
	 * @param $params
	 * @return bool|void
	 */
	public function hookActionCarrierProcess($params)
	{
		//if (!Tools::getIsset('confirmDeliveryOption')) return false;

		$ddw_order_time = '';
		$ddw_timeslot_id = '';

		/* check if time slot is available */
		if (Tools::getIsset('ddw_order_time'))
		{
			$ddw_timeslot = new DDWTimeslot(Tools::getValue('ddw_order_time'));
			$ddw_order_time = $ddw_timeslot->time_start.' - '.$ddw_timeslot->time_end;
			$ddw_timeslot_id = $ddw_timeslot->id;
			if ($ddw_timeslot->order_limit > 0)
			{
				$count = DDW::getOrderCountInTimeWindow(Tools::getValue('ddw_order_date'), $ddw_order_time, $this->context->shop->id);
				if ($count >= $ddw_timeslot->order_limit) $ddw_timeslot->id = '';
			}

			$cart = $params['cart'];
			if (!($cart instanceof Cart))
				return;

			DDW::saveToCart(Tools::getValue('ddw_order_date'), $ddw_timeslot_id, $cart->id);
			$cart->ddw_order_date = Tools::getValue('ddw_order_date');
			$cart->ddw_order_time = $ddw_timeslot_id;
		}
	}

	/**
	 * Display date and time on order confirmation page
	 * @param $params
	 */
	public function hookDisplayOrderConfirmation($params)
	{
		error_log(print_r($params, true));

		if (empty($params['order'])) return '';

		$order = $params['order'];
		$ddw_datetime = DDW::getOrderDDWDateTime($order->id);

		$block_html = '';
		if ($ddw_datetime['ddw_order_date'] != '') $block_html .= '<br><strong>'.Translate::getModuleTranslation('deliverydateswizardpro', 'Delivery Date:', 'ORDER_DETAIL_BLOCK').'</strong> '.Tools::displayDate($ddw_datetime['ddw_order_date']);
		if ($ddw_datetime['ddw_order_time'] != '' && $ddw_datetime['ddw_order_time'] != 0) $block_html .= '<br><strong>'.Translate::getModuleTranslation('deliverydateswizardpro', 'Delivery Time:', 'ORDER_DETAIL_BLOCK').'</strong> '.$ddw_datetime['ddw_order_time'];
		return $block_html;
	}

}