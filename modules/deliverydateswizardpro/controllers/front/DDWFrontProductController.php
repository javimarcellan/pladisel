<?php
/**
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * You must not modify, adapt or create derivative works of this source code
 *
 * @author    Musaffar Patel
 * @copyright 2016-2017 Musaffar Patel
 * @license   LICENSE.txt
 */

class DDWFrontProductController extends DDWControllerCore
{

	public function __construct(&$sibling = null)
	{
		parent::__construct($sibling);
		if ($sibling !== null)
		{
			$this->sibling = &$sibling;
			if (!empty($this->sibling->context->controller))
				$this->sibling->context->controller->addCSS($this->sibling->_path.'views/css/front/deliverydateswizard.css');
		}
	}

	public function renderETA($id_product, $params)
	{
		$product = new Product($id_product);
		$id_shop = Context::getContext()->shop->id;
		$id_language = Context::getContext()->language->id;
		$id_carrier = $params['cart']->id_carrier;

		if ((int)$id_carrier == 0) $id_carrier = Configuration::get('PS_CARRIER_DEFAULT');

		$ddw = new DDW();
		$ddw->loadByCarrier($id_carrier);

		if (!$ddw->product_eta_display_enabled) return false;

		$scopes = array();

		/* supplier overrides */
		$ddw_scope_helper = DDWScopeHelper::getInstance('supplier', $id_language, $id_shop);
		$supplier_scope = $ddw_scope_helper->getOptions($product->id_supplier, $id_carrier);
		if (!empty($supplier_scope)) $scopes[] = $supplier_scope;

		/* category overrides */
		$ddw_scope_helper = DDWScopeHelper::getInstance('category', $id_language, $id_shop);
		$category_scope = $ddw_scope_helper->getOptions($product->id_category_default, $id_carrier);
		if (!empty($category_scope)) $scopes[] = $category_scope;

		/* product overrides */
		$ddw_scope_helper = DDWScopeHelper::getInstance('product', $id_language, $id_shop);
		$product_scope = $ddw_scope_helper->getOptions($product->id, $id_carrier);
		if (!empty($product_scope)) $scopes[] = $product_scope;

		if (!empty($scopes)) {
			$ddw_front_controller = new DDWControllerFront($this->sibling);

			if (empty(Context::getContext()->cart->id_carrer)) {
				$id_carrier = 0;
			} else {
				$id_carrier = Context::getContext()->cart->id_carrier;
			}
            $override_scope = array(
                'id_carrier' => $id_carrier
            );

            $ddw_front_controller = new DDWControllerFront($this->sibling);
			$blocked_dates = $ddw_front_controller->getBlockedDates($override_scope);

			$timestamp = strtotime($blocked_dates['defaults']['calendar_default_year'].'/'.$blocked_dates['defaults']['calendar_default_month'].'/'.$blocked_dates['defaults']['calendar_default_day']);
			$weekday = $ddw_front_controller->getWeekdayTranslated(date('w', $timestamp));
			$month = $ddw_front_controller->getMonthTranslated(date('m', $timestamp));

			$ddw_product_eta = $blocked_dates['defaults'];
			$ddw_product_eta['text'] = $weekday.' '.$blocked_dates['defaults']['calendar_default_day'].' '.$month;

			// today?
			$today = false; //is first available day the current date?
			if ($blocked_dates['defaults']['calendar_default_day'] == date('d') && $blocked_dates['defaults']['calendar_default_month'] == date('m') && $blocked_dates['defaults']['calendar_default_year'] == date('Y')) {
				$ddw_product_eta['text'] = $this->l('Today');
				$today = true;
			}

			// get first available timslots
			if (!empty($blocked_dates['defaults']['timeslots'])) {
				if ($today) {
				} else {
					foreach ($blocked_dates['defaults']['timeslots'] as $timeslot) {
						if ($timeslot->enabled) {
							$timeslot_text = $timeslot->time_start . ' - ' . $timeslot->time_end;
						}
					}
				}
			}

			$this->sibling->smarty->assign(array(
				'ddw_product_eta' => $ddw_product_eta,
				'timeslot_text' => $timeslot_text
			));
			return $this->sibling->display($this->sibling->file, 'views/templates/front/product_eta.tpl');
		}
	}

}