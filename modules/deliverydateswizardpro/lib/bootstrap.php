<?php
/**
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * You must not modify, adapt or create derivative works of this source code
 *
 * @author    Musaffar Patel
 * @copyright 2016-2017 Musaffar Patel
 * @license   LICENSE.txt
 */

/* types */
include_once(_PS_MODULE_DIR_.'/deliverydateswizardpro/lib/types/DDWCarrier.php');

/* Module Core Classes */
include_once(_PS_MODULE_DIR_.'/deliverydateswizardpro/lib/classes/DDWControllerCore.php');

/* models */
include_once(_PS_MODULE_DIR_.'/deliverydateswizardpro/models/DDWInstall.php');
include_once(_PS_MODULE_DIR_.'/deliverydateswizardpro/models/DDWModel.php');
include_once(_PS_MODULE_DIR_.'/deliverydateswizardpro/models/DDWTimeslot.php');
include_once(_PS_MODULE_DIR_.'/deliverydateswizardpro/models/DDWTranslations.php');
include_once(_PS_MODULE_DIR_.'/deliverydateswizardpro/models/DDWWeekday.php');
include_once(_PS_MODULE_DIR_.'/deliverydateswizardpro/models/DDWBlockedDate.php');
include_once(_PS_MODULE_DIR_.'/deliverydateswizardpro/models/DDWScope.php');
include_once(_PS_MODULE_DIR_.'/deliverydateswizardpro/models/DDWSpecificDateModel.php');

/* controllers */
include_once(_PS_MODULE_DIR_.'/deliverydateswizardpro/controllers/admin/DDWAdminHooks.php');
include_once(_PS_MODULE_DIR_.'/deliverydateswizardpro/controllers/admin/DDWConfig.php');
include_once(_PS_MODULE_DIR_.'/deliverydateswizardpro/controllers/admin/DDWConfigCarrier.php');

include_once(_PS_MODULE_DIR_.'/deliverydateswizardpro/controllers/front/DDWFront.php');
include_once(_PS_MODULE_DIR_.'/deliverydateswizardpro/controllers/front/DDWFrontProductController.php');

include_once(_PS_MODULE_DIR_.'/deliverydateswizardpro/controllers/admin/config/DDWConfigGeneralController.php');
include_once(_PS_MODULE_DIR_.'/deliverydateswizardpro/controllers/admin/config/DDWConfigWeekdaysController.php');
include_once(_PS_MODULE_DIR_.'/deliverydateswizardpro/controllers/admin/config/DDWConfigBlockedDatesController.php');
include_once(_PS_MODULE_DIR_.'/deliverydateswizardpro/controllers/admin/config/DDWConfigTranslationsController.php');
include_once(_PS_MODULE_DIR_.'/deliverydateswizardpro/controllers/admin/config/DDWConfigProductController.php');
include_once(_PS_MODULE_DIR_.'/deliverydateswizardpro/controllers/admin/config/DDWConfigSpecificDatesController.php');

include_once(_PS_MODULE_DIR_.'/deliverydateswizardpro/controllers/admin/producttab/DDWProductTabController.php');
include_once(_PS_MODULE_DIR_.'/deliverydateswizardpro/controllers/admin/producttab/DDWAdminTabGeneralController.php');

/* Helpers */
include_once(_PS_MODULE_DIR_.'/deliverydateswizardpro/helpers/DDWScopeHelper.php');
