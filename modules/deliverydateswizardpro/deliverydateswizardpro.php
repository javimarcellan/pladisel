<?php
/**
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * You must not modify, adapt or create derivative works of this source code
 *
 * @author    Musaffar Patel
 * @copyright 2016-2017 Musaffar Patel
 * @license   LICENSE.txt
 */

if (!defined('_PS_VERSION_'))
	exit;

include_once(_PS_MODULE_DIR_.'/deliverydateswizardpro/lib/bootstrap.php');

class DeliveryDatesWizardPro extends Module {

	const __MA_MAIL_DELIMITOR__ = ',';

	public $module_folder = 'deliverydateswizardpro';
	public $module_file = __FILE__;
	public $base_url = '';

	public $controller_translations;


	protected $controller_front;
	protected $controller_admin_hooks;
	protected $controller_config;
	protected $controller_config_carrier;
	protected $controller_daterange;
	protected $controller_timeslots;

	public function __construct()
	{
		$this->name = 'deliverydateswizardpro';
		$this->tab = 'checkout';
		$this->version = '2.0.13';
		$this->author = 'Musaffar Patel';
		parent::__construct();
		$this->displayName = $this->l('Delivery Dates Wizard Pro');
		$this->description = $this->l('Allow customers to conveniently select a delivery date during checkout');
		$this->module_key = '893c65fda54d76f42ec334089fdd1963';
		$this->confirmUninstall = $this->l('Are you sure you want to uninstall?');

		$this->file = __FILE__;
		$this->bootstrap = true;

		$this->base_url = Tools::getShopProtocol().Tools::getShopDomain().__PS_BASE_URI__;
		$this->ps_versions_compliancy = array('min' => '1.7.0.0', 'max' => _PS_VERSION_);

		/* Initialise controllers */
		$this->controller_front = new DDWControllerFront($this);
		$this->controller_admin_hooks = new DDWAdminHooks($this);
		$this->controller_config = new DDWConfigController($this);
		$this->controller_config_carrier = new DDWConfigCarrierController($this);
		$this->ddw_producttab_controller = new DDWProductTabController($this);
	}

	public function install()
	{
		if (!parent::install()
			|| !$this->registerHook('header')
			|| !$this->registerHook('actionCarrierProcess')
			|| !$this->registerHook('beforeCarrier')
			|| !$this->registerHook('displayAfterCarrier')
			|| !$this->registerHook('adminOrder')
			|| !$this->registerHook('backOfficeHeader')
			|| !$this->registerHook('paymentConfirm')
			|| !$this->registerHook('newOrder')
			|| !$this->registerHook('PDFInvoice')
			|| !$this->registerHook('DdwValidateOrder')
			|| !$this->registerHook('actionOrderDetail')
			|| !$this->registerHook('displayPDFInvoice')
			|| !$this->registerHook('displayCarrierList')
			|| !$this->registerHook('displayPDFDeliverySlip')
			|| !$this->registerHook('actionCarrierUpdate')
			|| !$this->registerHook('displayAdminProductsExtra')
			|| !$this->registerHook('displayProductButtons')
			|| !$this->registerHook('displayOrderConfirmation')
			|| !$this->installModule())
			return false;
		return true;
	}

	private function installModule()
	{
		DDWInstall::installDB();
		DDWInstall::installData();
		return true;
	}

	public function uninstall()
	{
		DDWInstall::uninstall();
		parent::uninstall();
		return true;
	}

	/* Call set media for all the various controllers in this module.  Each controller will decide if the time is appropriate for queuing it's css and js */
	public function setMedia()
	{
		(new DDWConfigGeneralController($this))->setMedia();
		(new DDWAdminTabGeneralController($this))->setMedia();
		(new DDWControllerFront($this))->setMedia();
	}

	public function route()
	{
		$render = '';
		switch (Tools::getValue('route'))
		{
			case 'ddwadminconfiggeneral':
				$ddw_general = new DDWConfigGeneralController($this);
				$render .= $ddw_general->route();
				break;

			case 'ddwadminconfigweekdays':
				$ddw_weekdays = new DDWConfigWeekdaysController($this);
				die($ddw_weekdays->route());

			case 'ddwadminconfigspecificdates':
				$ddw_specificdates = new DDWConfigSpecificDatesController($this);
				die($ddw_specificdates->route());

			case 'ddwadminconfigblockeddates':
				$ddw_blockedates = new DDWConfigBlockedDatesController($this);
				die($ddw_blockedates->route());

			case 'ddwconfigtranslations' :
				$ddw_translations = new DDWConfigTranslationsController($this);
				return $ddw_translations->route();
			case 'ddwadminconfigproducts' :
				$ddw_products = new DDWConfigProductController($this);
				return $ddw_products->route();
		}

		/* Route the date/time update from the order details page */
		if (Tools::getIsset('processOrderDDWUpdate'))
			return $this->controller_admin_hooks->processOrderDDWUpdate();

		if (Tools::getIsset('updatecarriers'))
			return $this->controller_config_carrier->route();
		else
			return $this->controller_config->renderMain();

		return $render;
	}

	public function getContent()
	{
		return $this->route();
	}

	/* Store Hooks */

	public function hookDisplayAdminProductsExtra($params)
	{
		$ddw_producttab_controller = new DDWProductTabController($this, $params);
		return $ddw_producttab_controller->route();
	}

	public function hookActionCarrierProcess($params)
	{
		$this->controller_front->hookActionCarrierProcess($params);
	}

	public function hookDisplayAfterCarrier($params)
	{
		$html = '';
		$html .= $this->controller_front->renderFrontWidget();
		return $html;
	}

	public function hookActionOrderDetail($params)
	{
	}

	public function hookAdminOrder($params)
	{
		$html = $this->controller_admin_hooks->renderOrderDetailBlock($params['id_order']);
		return $html;
	}

	public function hookPaymentConfirm($params)
	{
		//return("ok");
	}

	public function hookDisplayPDFInvoice($params)
	{
		return $this->controller_admin_hooks->renderHookDisplayPDFInvoice($params);
	}

	public function hookDdwValidateOrder($params)
	{
		$ddw_datetime = DDW::getCartDDWDateTime($params['id_cart']);
		return $ddw_datetime;
	}

	public function hookNewOrder($params)
	{
		$cart = $params['cart'];
		if (!($cart instanceof Cart))
			return;
		DDW::saveToOrder($cart);
	}

	public function hookHeader($params)
	{
		$this->setMedia();
	}

	public function hookBackOfficeHeader($params)
	{
		$this->setMedia();
	}

	public function hookDisplayPDFDeliverySlip($params)
	{
		return $this->controller_admin_hooks->renderHookDisplayPDFDeliverySlip($params);
	}

	public function hookActionCarrierUpdate($params)
	{
		DDW::updateCarrierID($params['id_carrier'], $params['carrier']->id);
	}

	public function hookDisplayProductButtons($params)
	{
		$ddw_front_product_controller = new DDWFrontProductController($this);
		return $ddw_front_product_controller->renderETA(Tools::getValue('id_product'), $params);
	}

	public function hookDisplayOrderConfirmation($params)
	{
		$ddw_front_controller = new DDWControllerFront($this);
		return $ddw_front_controller->hookDisplayOrderConfirmation($params);
	}

}