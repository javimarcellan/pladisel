<?php
require_once('../../config/config.inc.php');

$token = Tools::getValue('token');
$comparedToken = Tools::getAdminToken('gmexport');

if ($token == $comparedToken) {
    $ids = array();
    $idLang = (int)Tools::getValue('id_lang');
    $iso = Language::getIsoById($idLang);
    header('Content-Type: application/csv; charset=UTF-8');
    header('Content-Disposition: attachement; filename="Categories_'.$iso.'.csv";');
    $fp = fopen('php://output', 'w');
    $row = array('id', 'name', 'parent', 'description');
    fputcsv($fp, $row, ';');
    generateCategoryCsvList();
    fclose($fp);
} else {
    die('Invalid token, sorry...');
}

function generateCategoryCsvList($parent = 0)
{
    global $ids;
    global $fp;
    $idLang = (int)Tools::getValue('id_lang');
    $idShop = (int)Tools::getValue('id_shop');
    if ($parent == 0) {
        $results = Db::getInstance()->ExecuteS("SELECT c.id_parent, c.id_category, CONCAT ( REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(level_depth,1,''),2,''),3,''),4,''),5,''),6,''),cl.name) as name, cl.description, cl.link_rewrite,cs.position ,level_depth
					FROM "._DB_PREFIX_."category c
					LEFT JOIN "._DB_PREFIX_."category_lang cl ON (c.id_category = cl.id_category AND id_lang = '{$idLang}')
					LEFT JOIN "._DB_PREFIX_."category_group cg ON (cg.`id_category` = c.id_category)
					LEFT JOIN `"._DB_PREFIX_."category_shop` cs ON (c.`id_category` = cs.`id_category` )
					WHERE c.id_category <> '1'
                    AND cs.id_shop = {$idShop}
					GROUP BY c.id_category
					ORDER BY c.`id_parent` ASC, level_depth ASC");
        foreach ($results as $row) {
            if (!in_array($row['id_category'], $ids)) {
                $csvRow = array(
                    $row['id_category'],
                    $row['name'],
                    $row['id_parent'],
                    $row['description']
                );
                fputcsv($fp, $csvRow, ';');
                array_push($ids, $row['id_category']);
                generateCategoryCsvList($row['id_category']);
            }
        }
    } else {
        $results = Db::getInstance()->ExecuteS("SELECT c.id_parent, c.id_category, CONCAT ( REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(level_depth,1,''),2,''),3,''),4,''),5,''),6,''),cl.name) as name, cl.description, cl.link_rewrite,cs.position ,level_depth
					FROM "._DB_PREFIX_."category c
					LEFT JOIN "._DB_PREFIX_."category_lang cl ON (c.id_category = cl.id_category AND id_lang = '{$idLang}')
					LEFT JOIN "._DB_PREFIX_."category_group cg ON (cg.`id_category` = c.id_category)
					LEFT JOIN `"._DB_PREFIX_."category_shop` cs ON (c.`id_category` = cs.`id_category` )
					WHERE c.id_category <> '1'
					AND c.id_parent = '".$parent."'
                    AND cs.id_shop = {$idShop}
					GROUP BY c.id_category
					ORDER BY cs.position ASC, c.`id_parent` ASC, level_depth ASC");
        if (!empty($results) > 0) {
            foreach ($results as $row) {
                if (!in_array($row['id_category'], $ids)) {
                    $csvRow = array(
                        $row['id_category'],
                        $row['name'],
                        $row['id_parent'],
                        $row['description']
                    );
                    fputcsv($fp, $csvRow, ';');
                    array_push($ids, $row['id_category']);
                    generateCategoryCsvList($row['id_category']);
                }
            }
        }
    }
}
