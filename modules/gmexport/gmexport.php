<?php
/**
 * Exports data from the shop
 *
 * @package   gmexport
 * @author    Dariusz Tryba (contact@greenmousestudio.com)
 * @copyright Copyright (c) Green Mouse Studio (http://www.greenmousestudio.com)
 * @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */
if (!defined('_PS_VERSION_')) exit;

class GMExport extends Module
{
    protected $token;

    public function __construct()
    {
        $this->name = 'gmexport';
        $this->tab = 'administration';
        $this->version = '1.0.0';
        $this->author = 'GreenMouseStudio.com';
        $this->module_key = '';
        $this->need_instance = 0;
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('Export');
        $this->description = $this->l('Exports data from your shop');
        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
        $this->token = Tools::getAdminToken('gmexport');
    }

    public function getContent()
    {
        $content = '';
        $content .= $this->displayCategoryExportPanel();
        return $content
            .$this->context->smarty->fetch($this->local_path.'views/templates/admin/gms.tpl');
    }

    protected function displayCategoryExportPanel()
    {
        $action = Tools::getHttpHost(true).__PS_BASE_URI__.'modules/'.$this->name.'/categories.php';
        $caption = $this->l('Export categories');
        $content = '<div class="panel clearfix">';
        $content .= '<form method="post" action="'.$action.'">';
        $content .= $this->displayShopSelect();
        $content .= $this->displayLanguageSelect();
        $content .= '<input type="hidden" name="token" value="'.$this->token.'" />';
        $content .= '<div class="form-group"><button type="submit" class="btn btn-default">'
            .'<i class="icon-check"></i> '.$caption.'</button></div>';
        $content .= '</form>';
        $content .= '</div>';
        return $content;
    }

    protected function displayShopSelect() {
        $shops = Shop::getShops();
        $content = '<div class="form-group">
            <label class="control-label">'.$this->l('Shop').'</label>';
        $content .= '<select name="id_shop" class="fixed-width-xl">';
        foreach ($shops as $shop) {
            $content .= '<option value="'.$shop['id_shop'].'">'.$shop['name'].'</option>';
        }
        $content .= '</select></div>';
        return $content;
    }

    protected function displayLanguageSelect()
    {
        $languages = Language::getLanguages();
        $content = '<div class="form-group">
            <label class="control-label">'.$this->l('Language').'</label>';
        $content .= '<select name="id_lang" class="fixed-width-xl">';
        foreach ($languages as $language) {
            $content .= '<option value="'.$language['id_lang'].'">'.$language['name'].'</option>';
        }
        $content .= '</select></div>';
        return $content;
    }
}
