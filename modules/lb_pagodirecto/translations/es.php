<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{lb_pagodirecto}prestashop>payment_cf958c247735661fbdd775fcb6f05b3f'] = 'Pagar por pago directo';
$_MODULE['<{lb_pagodirecto}prestashop>payment_4e1fb9f4b46556d64db55d50629ee301'] = '(el procesamiento de pedido será más largo)';
$_MODULE['<{lb_pagodirecto}prestashop>payment_return_88526efe38fd18179a127024aba8c1d7'] = 'Tu pedido %s ha sido completado.';
$_MODULE['<{lb_pagodirecto}prestashop>payment_return_b3f68a0aad67dacdba186c744e690289'] = 'Nosotros tambien te mandaremos esta información por mail.';
$_MODULE['<{lb_pagodirecto}prestashop>payment_return_b9a1cae09e5754424e33764777cfcaa0'] = 'Su pedido será enviado tan pronto como recibamos el pago.';
$_MODULE['<{lb_pagodirecto}prestashop>payment_return_1b22721c0967c351b34d82496917654f'] = 'Si tienes preguntas, comentarios o inquietudes, contacte con nuestro [1] equipo experto de atención al cliente [/ 1].';
$_MODULE['<{lb_pagodirecto}prestashop>payment_return_c00373052588907ffdd28d1866a75aee'] = 'Notamos un problema con su pedido. Si cree que se trata de un error, contacte con nuestro [1] equipo experto de atención al cliente [/ 1].';
