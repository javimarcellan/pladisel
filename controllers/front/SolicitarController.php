<?php
class SolicitarController extends FrontController
{
	public $php_self = 'solicitar';
	public $ssl = true;
	
	/**
	 * Start forms process
	 * @see FrontController::postProcess()
	 */
	public function postProcess()
	{
		if (Tools::isSubmit('submitMessage')) {	
			$nombreApellidos = Tools::getValue('nombreApellidos');
			$empresa = Tools::getValue('empresa');
			$nif = Tools::getValue('nif');
			$direccion = Tools::getValue('direccion');
			$poblacion = Tools::getValue('poblacion');
			$cp = Tools::getValue('cp');
			$provincia= Tools::getValue('provincia');
			$pais= Tools::getValue('pais');
			$telefono= Tools::getValue('telefono');
			$mail = Tools::getValue('mail');
			$observaciones = trim(Tools::getValue('observaciones'));
			$politica = Tools::getValue('politica');
			
			if (!$nombreApellidos) {
				$this->errors[] = Tools::displayError('The nombre y apellidos cannot be blank.');
			} elseif (!Validate::isCleanHtml($nombreApellidos))  {
				$this->errors[] = Tools::displayError('Invalid nombre');
			} elseif (!$empresa) {
				$this->errors[] = Tools::displayError('The empresa cannot be blank.');
			} elseif (!Validate::isCleanHtml($empresa)) {
				$this->errors[] = Tools::displayError('Invalid empresa');
			 }elseif (!$nif) {
				$this->errors[] = Tools::displayError('The nif cannot be blank.');
			} elseif (!Validate::isCleanHtml($nif)) {
				$this->errors[] = Tools::displayError('Invalid nif');
			} elseif (!$direccion) {
				$this->errors[] = Tools::displayError('The direccion cannot be blank.');
			} elseif (!Validate::isCleanHtml($direccion)) {
				$this->errors[] = Tools::displayError('Invalid direccion');
			} elseif (!$poblacion) {
				$this->errors[] = Tools::displayError('The poblacion cannot be blank.');
			} elseif (!Validate::isCleanHtml($poblacion)) {
				$this->errors[] = Tools::displayError('Invalid poblacion');
			} elseif (!$cp) {
				$this->errors[] = Tools::displayError('The cp cannot be blank.');
			} elseif (!Validate::isCleanHtml($cp)) {
				$this->errors[] = Tools::displayError('Invalid cp');
			} elseif (!$provincia) {
				$this->errors[] = Tools::displayError('The provincia cannot be blank.');
			} elseif (!Validate::isCleanHtml($provincia)) {
				$this->errors[] = Tools::displayError('Invalid provincia');
			} elseif (!$pais) {
				$this->errors[] = Tools::displayError('The pais cannot be blank.');
			} elseif (!Validate::isCleanHtml($pais)) {
				$this->errors[] = Tools::displayError('Invalid pais');
			} if (!($mail = trim(Tools::getValue('mail'))) || !Validate::isEmail($mail)) {
				$this->errors[] = Tools::displayError('Invalid email address.');
			} elseif (!$telefono) {
				$this->errors[] = Tools::displayError('The telefono cannot be blank.');
			} elseif (!Validate::isCleanHtml($telefono)) {
				$this->errors[] = Tools::displayError('Invalid telefono');
			} elseif (!Validate::isCleanHtml($observaciones)) {
				$this->errors[] = Tools::displayError('Invalid observaciones');
			} elseif (!$politica) {
				$this->errors[] = Tools::displayError('The politica cannot be blank.');
			}
			else {					
				if (!count($this->errors)) {
					$var_list = array(
							'{nombreApellidos}' =>  $nombreApellidos,
							'{empresa}' =>  $empresa,
							'{nif}' =>  $nif,
							'{direccion}' =>  $direccion,
							'{poblacion}' =>  $poblacion,
							'{cp}' =>  $cp,
							'{provincia}' =>  $provincia,
							'{pais}' =>  $pais,
							'{mail}' =>  $mail,
							'{telefono}' =>  $telefono,
							'{observaciones}' =>  $observaciones,
							'{politica}' =>  $politica
					);	
					//Tipo de contacto 2, es el que se usa
					$contact = new Contact(2, $this->context->language->id);
			
					if (!Mail::Send($this->context->language->id, 'solicitud_alta', Mail::l('Nueva solicitud de alta'),
							$var_list, $contact->email, $contact->name, null, null,
							null, null, _PS_MAIL_DIR_, false, null, null, $from)) {
						$this->errors[] = Tools::displayError('An error occurred while sending the message.');
					}					
				}
				
				if (count($this->errors) > 1) {
					array_unique($this->errors);
				} elseif (!count($this->errors)) {
					$this->context->smarty->assign('confirmation', 1);
				}
			}
		}
	}

	public function setMedia()
	{
		parent::setMedia();
		$this->addCSS(_THEME_CSS_DIR_.'contact-form.css');
		$this->addJS(_THEME_JS_DIR_.'contact-form.js');
		$this->addJS(_PS_JS_DIR_.'validate.js');
	}

	
	/**
	 * Assign template vars related to page content
	 * @see FrontController::initContent()
	 */
	public function initContent()
	{
		parent::initContent();	
		
		$nombreApellidos = Tools::getValue('nombreApellidos');
		$empresa = Tools::getValue('empresa');
		$nif = Tools::getValue('nif');
		$direccion = Tools::getValue('direccion');
		$poblacion = Tools::getValue('poblacion');
		$cp = Tools::getValue('cp');
		$provincia= Tools::getValue('provincia');
		$pais= Tools::getValue('pais');
		$telefono= Tools::getValue('telefono');
		$mail = Tools::getValue('mail');
		$observaciones = trim(Tools::getValue('observaciones'));
		$politica = Tools::getValue('politica');

		$this->context->smarty->assign(array(
			'nombreApellidos' =>  $nombreApellidos,
			'empresa' =>  $empresa,
			'nif' =>  $nif,
			'direccion' =>  $direccion,
			'poblacion' =>  $poblacion,
			'cp' =>  $cp,
			'provincia' =>  $provincia,
			'pais' =>  $pais,
			'mail' =>  $mail,
			'telefono' =>  $telefono,
			'observaciones' =>  $observaciones,
			'politica' =>  $politica
		));

		$this->setTemplate('customer/solicitar-alta');
	}
}