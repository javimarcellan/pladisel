/*
 * Custom code goes here.
 * A template should always ship with an empty custom.js
 */

jQuery(function($) {
    jQuery("a[data-rel^='prettyPhoto']").prettyPhoto({
        social_tools: false,
        show_title:false,
        allow_resize: true, 
        allow_expand: false
    })
});