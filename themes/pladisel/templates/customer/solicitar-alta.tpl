{extends file='page.tpl'}

{block name='page_title'}
  {l s='Solicitar alta' d='Shop.Theme.Global'}
{/block}

{block name='page_content'}
<section class="contact-form shadow-box">
{if isset($confirmation)}
	<p class="alert alert-success">{l s='Your message has been successfully sent to our team.'}</p>
	<ul class="footer_links clearfix">
		<li>
			<a class="btn btn-default button button-small" href="{if isset($force_ssl) && $force_ssl}{$base_dir_ssl}{else}{$base_dir}{/if}">
				<span>
					<i class="icon-chevron-left"></i>{l s='Home'}
				</span>
			</a>
		</li>
	</ul>
{elseif isset($alreadySent)}
	<p class="alert alert-warning">{l s='Your message has already been sent.'}</p>
	<ul class="footer_links clearfix">
		<li>
			<a class="btn btn-default button button-small" href="{if isset($force_ssl) && $force_ssl}{$base_dir_ssl}{else}{$base_dir}{/if}">
				<span>
					<i class="icon-chevron-left"></i>{l s='Home'}
				</span>
			</a>
		</li>
	</ul>
{else}
	<form action="{$request_uri}" method="post" class="contact-form-box" enctype="multipart/form-data">
		<fieldset>
			<h3 class="page-subheading">{l s='Para poder darse de alta como cliente de Pladisel'}</h3>
			<div class="clearfix">
              <div class="row">
				<div class="column col-xs-12 col-md-6">
					<p class="form-group">
						<label for="nombreApellidos">{l s='Nombre y apellidos'} *</label>						
						<input class="form-control validate" type="text" id="nombreApellidos" name="nombreApellidos" data-validate="isCleanHtml" value="{$nombreApellidos|escape:'html':'UTF-8'}" />						
					</p>
    				<p class="form-group">
						<label for="empresa">{l s='Empresa'} *</label>						
						<input class="form-control validate" type="text" id="empresa" name="empresa" data-validate="isCleanHtml" value="{$empresa|escape:'html':'UTF-8'}" />						
					</p>
        			<p class="form-group">
						<label for="nif">{l s='NIF'} *</label>						
						<input class="form-control validate" type="text" id="nif" name="nif" data-validate="isCleanHtml" value="{$nif|escape:'html':'UTF-8'}" />						
					</p>
    				<p class="form-group">
						<label for="direccion">{l s='Direccion'} *</label>						
						<input class="form-control validate" type="text" id="direccion" name="direccion" data-validate="isCleanHtml" value="{$direccion|escape:'html':'UTF-8'}" />						
					</p>
    				<p class="form-group">
						<label for="poblacion">{l s='Poblacion'} *</label>						
						<input class="form-control validate" type="text" id="poblacion" name="poblacion" data-validate="isCleanHtml" value="{$poblacion|escape:'html':'UTF-8'}" />						
					</p>
				</div>
				<div class="column col-xs-12 col-md-6">
        			<p class="form-group">
						<label for="cp">{l s='Codigo postal'} *</label>						
						<input class="form-control validate" type="text" id="cp" name="cp" data-validate="isCleanHtml" value="{$cp|escape:'html':'UTF-8'}" />						
					</p>				
					<p class="form-group">
						<label for="provincia">{l s='Provincia'} *</label>						
						<input class="form-control validate" type="text" id="provincia" name="provincia" data-validate="isCleanHtml" value="{$provincia|escape:'html':'UTF-8'}" />						
					</p>
					<p class="form-group">
						<label for="pais">{l s='Pais'} *</label>						
						<input class="form-control validate" type="text" id="pais" name="pais" data-validate="isCleanHtml" value="{$pais|escape:'html':'UTF-8'}" />						
					</p>
					<p class="form-group">
						<label for="telefono">{l s='Telefono'} *</label>						
						<input class="form-control validate" type="text" id="telefono" name="telefono" data-validate="isCleanHtml" value="{$telefono|escape:'html':'UTF-8'}" />						
					</p>
					<p class="form-group">
						<label for="mail">{l s='Email address'} *</label>						
						<input class="form-control validate" type="text" id="mail" name="mail" data-validate="isEmail" value="{$mail|escape:'html':'UTF-8'}" />						
					</p>
				</div>
              </div>
              <div class="row">
                <div class="column col-md-12">
                  <p class="form-group">
                  <label for="observaciones">{l s='Observaciones'}</label>						
                  <textarea placeholder="¿Cómo podemos ayudarle?" class="form-control validate" id="observaciones" name="observaciones" rows="5" data-validate="isCleanHtml" value="{$observaciones|escape:'html':'UTF-8'}"></textarea						
                  </p>
                  <div class="form-check">
                    <input type="checkbox" class="form-check-input" id="politica" name="politica">
                    <label class="form-check-label" for="politica">{l s='Acepto la política de privacidad y protección de datos personales'} *</label>
                  </div>
                </div>
              </div>
			</div>
			<div class="submit text-right">				
				<button type="submit" name="submitMessage" id="submitMessage" class="button btn btn btn-primary button-medium"><span>{l s='Send'}<i class="icon-chevron-right right"></i></span></button>
			</div>
			<p>{l s="* Campos obligatorios"}</p>
		</fieldset>
	</form>
{/if}
</section>
{/block}
