<?php
/**
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * You must not modify, adapt or create derivative works of this source code
 *
 * @author    Musaffar Patel
 * @copyright 2016-2017 Musaffar Patel
 * @license   LICENSE.txt
 */
class Cart extends CartCore
{
	/*
    * module: freeproductgifts
    * date: 2018-05-23 13:47:56
    * version: 2.0.6
    */
    public function deleteProduct($id_product, $id_product_attribute = null, $id_customization = null, $id_address_delivery = 0)
	{
		if (!Module::isEnabled('FreeProductGifts'))
			parent::deleteProduct($id_product, $id_product_attribute = null, $id_customization = null, $id_address_delivery = 0);
		$result = Hook::exec('fpgDeleteCartProduct', array(
			'id_product' => $id_product,
			'id_product_attribute' => $id_product_attribute,
			'id_customization' => $id_customization,
			'id_address_delivery' => $id_address_delivery,
		),
			null, false);
		if ($result == false)
			parent::deleteProduct($id_product, $id_product_attribute, $id_customization, $id_address_delivery);
	}
}