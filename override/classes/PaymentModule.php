<?php
/**
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * You must not modify, adapt or create derivative works of this source code
 *
 * @author    Musaffar Patel
 * @copyright 2016-2017 Musaffar Patel
 * @license   LICENSE.txt
 */

abstract class PaymentModule extends PaymentModuleCore
{
	/*
    * module: deliverydateswizardpro
    * date: 2018-05-23 13:48:48
    * version: 2.0.13
    */
    private function _addDDWBlock($id_cart)
	{
		$block_html = '';
		$cart_ddw = Hook::exec('ddwValidateOrder', array('id_cart' => $id_cart), null, true);
		$cart_ddw = $cart_ddw['deliverydateswizardpro'];
		if ($cart_ddw['ddw_order_date'] != '') $block_html .= '<br><strong>'.Translate::getModuleTranslation('deliverydateswizardpro', 'Delivery Date:', 'ORDER_DETAIL_BLOCK').'</strong> '.Tools::displayDate($cart_ddw['ddw_order_date']);
		if ($cart_ddw['ddw_order_time'] != '') $block_html .= '<br><strong>'.Translate::getModuleTranslation('deliverydateswizardpro', 'Delivery Time:', 'ORDER_DETAIL_BLOCK').'</strong> '.$cart_ddw['ddw_order_time'];
		return $block_html;
	}
	/*
    * module: deliverydateswizardpro
    * date: 2018-05-23 13:48:48
    * version: 2.0.13
    */
    public function validateOrder($id_cart, $id_order_state, $amount_paid, $payment_method = 'Unknown', $message = null, $extra_vars = array(), $currency_special = null, $dont_touch_amount = false, $secure_key = false, Shop $shop = null)
	{
		$cart_ddw = Hook::exec('ddwValidateOrder', array('id_cart' => $id_cart), null, true);
		$cart_ddw = $cart_ddw['deliverydateswizardpro'];
		if ($cart_ddw['ddw_order_date'] != '') $extra_vars['{DDW_ORDER_DATE}'] = Tools::displayDate($cart_ddw['ddw_order_date']);
		if ($cart_ddw['ddw_order_time'] != '') $extra_vars['{DDW_ORDER_TIME}'] = $cart_ddw['ddw_order_time'];
		return parent::validateOrder(
				$id_cart, $id_order_state, $amount_paid, $payment_method,
				$message, $extra_vars, $currency_special, $dont_touch_amount,
				$secure_key, $shop
		);
	}
}